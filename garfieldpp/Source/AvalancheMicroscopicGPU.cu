#include "Garfield/GPUInterface.hh"
#include "Garfield/AvalancheMicroscopic.hh"
#include "Garfield/AvalancheMicroscopicGPU.h"

#include "Garfield/GPUFunctions.h"

#include <iostream>
#include <chrono>

#include "Garfield/GPUInterface.hh"
#define __GPUCOMPILE__
#include "Garfield/SensorGPU.h"
#undef __GPUCOMPILE__
#include "Garfield/Random.hh"
#include "Garfield/RandomEnginePreCalcGPU.h"
#include "Garfield/RandomEnginePreCalc.hh"
#include "Garfield/RandomEngineGPU.h"
#include "Garfield/GPUFunctions.h"
#include "Garfield/RandomGPU.h"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

using highres_clock_t = std::chrono::high_resolution_clock;
using second_t = std::chrono::duration<double, std::ratio<1> >;

namespace Garfield {

    // ----------------------------------------------------------------------------------------------------
    // Global/Device functions that can't be included in the class due to the definition being compiled with AvalancheMicroscopic
    __global__ void setStatusArray(int *all_status_array, int num_active_particles, int num_total_particles) 
    {
        int thread_idx = (threadIdx.x + blockIdx.x * blockDim.x);
        all_status_array[num_active_particles+thread_idx] = thread_idx + num_total_particles;
    }

    __device__ double Mag(const double x, const double y, const double z) {
        return sqrt(x * x + y * y + z * z);
    }

    // ----------------------------------------------------------------------------------------------------
    // Class methods
    AvalancheMicroscopicGPU::AvalancheMicroscopicGPU() {

        // MAXPARTICLES :- Maximum number of particles that can be generated in one shower
        // MAXSTACKSIZE :- Maximum number of *active* particles in one shower (i.e. same size as the reserve for stackOld)
        // MAXCREATEDPARTICLES :- Maximum possible number of created particles per particle per iteration (usually 1-2 because the loop is ended after particle creation)
        memUsageStack += InitialiseGPUParticleStack(stackOldGPU, MAXPARTICLES);
        memUsageStack += InitialiseGPUParticleStack(stackNewGPU, MAXCREATEDPARTICLES * MAXSTACKSIZE);
        InitialiseCPUParticleStack(stackTransfer, MAXPARTICLES); // on CPU stack so doesn't count to GPU memory usage
        checkCudaErrors( cudaMalloc(&activeIndexArray, MAXSTACKSIZE*sizeof(int)));
        checkCudaErrors( cudaMalloc(&newIndexArray, (MAXCREATEDPARTICLES * MAXSTACKSIZE)*sizeof(int)));
        memUsageStack += (MAXCREATEDPARTICLES * MAXSTACKSIZE + MAXSTACKSIZE)*sizeof(int);

        std::cout << "--------------- AvalancheMicroscopicGPU Initialised:" << std::endl;
        std::cout << "Memory assigned:  " << memUsageStack / (1024.*1024.) << " MB" << std::endl;
    }

    AvalancheMicroscopicGPU::~AvalancheMicroscopicGPU() {
        cudaFree(newIndexArray);
        cudaFree(activeIndexArray);
        FreeCPUParticleStack(stackTransfer);
        FreeGPUParticleStack(stackNewGPU);
        FreeGPUParticleStack(stackOldGPU);
        //DELETE INTERNAL STUFF
    }

    double AvalancheMicroscopicGPU::InitialiseGPUParticleStack(ParticleStack &stack, unsigned int num) {
        // initialise all the memory
        checkCudaErrors( cudaMalloc(&stack.x, (num)*sizeof(double)));
        checkCudaErrors( cudaMalloc(&stack.y, (num)*sizeof(double)));
        checkCudaErrors( cudaMalloc(&stack.z, (num)*sizeof(double)));
        checkCudaErrors( cudaMalloc(&stack.t, (num)*sizeof(double)));
        checkCudaErrors( cudaMalloc(&stack.x0, (num)*sizeof(double)));
        checkCudaErrors( cudaMalloc(&stack.y0, (num)*sizeof(double)));
        checkCudaErrors( cudaMalloc(&stack.z0, (num)*sizeof(double)));
        checkCudaErrors( cudaMalloc(&stack.t0, (num)*sizeof(double)));
        checkCudaErrors( cudaMalloc(&stack.e0, (num)*sizeof(double)));
        checkCudaErrors( cudaMalloc(&stack.status, (num)*sizeof(int)));
        checkCudaErrors( cudaMalloc(&stack.energy, (num)*sizeof(double)));
        checkCudaErrors( cudaMalloc(&stack.band, (num)*sizeof(int)));
        checkCudaErrors( cudaMalloc(&stack.kx, (num)*sizeof(double)));
        checkCudaErrors( cudaMalloc(&stack.ky, (num)*sizeof(double)));
        checkCudaErrors( cudaMalloc(&stack.kz, (num)*sizeof(double)));
        checkCudaErrors( cudaMalloc(&stack.hole, (num)*sizeof(bool)));
        stack.stack_size = 0;

        return num * sizeof(double) * 16;
    }

    void AvalancheMicroscopicGPU::FreeGPUParticleStack(ParticleStack &stack) {
        checkCudaErrors( cudaFree(stack.x));
        checkCudaErrors( cudaFree(stack.y));
        checkCudaErrors( cudaFree(stack.z));
        checkCudaErrors( cudaFree(stack.t));
        checkCudaErrors( cudaFree(stack.x0));
        checkCudaErrors( cudaFree(stack.y0));
        checkCudaErrors( cudaFree(stack.z0));
        checkCudaErrors( cudaFree(stack.t0));
        checkCudaErrors( cudaFree(stack.e0));
        checkCudaErrors( cudaFree(stack.status));
        checkCudaErrors( cudaFree(stack.energy));
        checkCudaErrors( cudaFree(stack.band));
        checkCudaErrors( cudaFree(stack.kx));
        checkCudaErrors( cudaFree(stack.ky));
        checkCudaErrors( cudaFree(stack.kz));
        checkCudaErrors( cudaFree(stack.hole));
    }

    void AvalancheMicroscopicGPU::InitialiseCPUParticleStack(ParticleStack &stack, unsigned int num) {
        // initialise all the memory
        stack.x = new double[num];
        stack.y = new double[num];
        stack.z = new double[num];
        stack.t = new double[num];
        stack.x0 = new double[num];
        stack.y0 = new double[num];
        stack.z0 = new double[num];
        stack.t0 = new double[num];
        stack.e0 = new double[num];
        stack.status = new int[num];
        stack.energy = new double[num];
        stack.band = new int[num];
        stack.kx = new double[num];
        stack.ky = new double[num];
        stack.kz = new double[num];
        stack.hole = new bool[num];
        stack.stack_size = 0;
    }

    void AvalancheMicroscopicGPU::FreeCPUParticleStack(ParticleStack &stack) {
        free(stack.x);
        free(stack.y);
        free(stack.z);
        free(stack.t);
        free(stack.x0);
        free(stack.y0);
        free(stack.z0);
        free(stack.t0);
        free(stack.e0);
        free(stack.status);
        free(stack.energy);
        free(stack.band);
        free(stack.kx);
        free(stack.ky);
        free(stack.kz);
        free(stack.hole);
    }

    // device function to transfer the whole current stack to the given transfer stack
    void AvalancheMicroscopicGPU::transferParticleStack(ParticleStack dest, unsigned int offset, ParticleStack source,
        unsigned int num, TransferType type, bool init_dest)
    {
        cudaMemcpyKind cuda_type;
        switch (type)
        {
            case TransferType::HostToDevice:
                cuda_type = cudaMemcpyHostToDevice;
                break;
            case TransferType::DeviceToDevice:
                cuda_type = cudaMemcpyDeviceToDevice;
                break;
            case TransferType::DeviceToHost:
                cuda_type = cudaMemcpyDeviceToHost;
                break;
        }
        if (init_dest)
        {
            checkCudaErrors( cudaMemcpy(dest.x + offset, source.x0, num*sizeof(GPUFLOAT), cuda_type ) );
            checkCudaErrors( cudaMemcpy(dest.y + offset, source.y0, num*sizeof(GPUFLOAT), cuda_type ) );
            checkCudaErrors( cudaMemcpy(dest.z + offset, source.z0, num*sizeof(GPUFLOAT), cuda_type ));
            checkCudaErrors( cudaMemcpy(dest.t + offset, source.t0, num*sizeof(GPUFLOAT), cuda_type ));
            checkCudaErrors( cudaMemcpy(dest.energy + offset, source.e0, num*sizeof(GPUFLOAT), cuda_type ));
            thrust::fill_n(thrust::device, dest.status + stackOldGPU.stack_size, num, 0);
        } else {
            checkCudaErrors( cudaMemcpy(dest.x + offset, source.x, num*sizeof(GPUFLOAT), cuda_type ) );
            checkCudaErrors( cudaMemcpy(dest.y + offset, source.y, num*sizeof(GPUFLOAT), cuda_type ) );
            checkCudaErrors( cudaMemcpy(dest.z + offset, source.z, num*sizeof(GPUFLOAT), cuda_type ));
            checkCudaErrors( cudaMemcpy(dest.t + offset, source.t, num*sizeof(GPUFLOAT), cuda_type ));
            checkCudaErrors( cudaMemcpy(dest.energy + offset, source.energy, num*sizeof(GPUFLOAT), cuda_type ));
            checkCudaErrors( cudaMemcpy(dest.status + offset, source.status, num*sizeof(int), cuda_type ));
        }
        checkCudaErrors( cudaMemcpy(dest.x0 + offset, source.x0, num*sizeof(GPUFLOAT), cuda_type ) );
        checkCudaErrors( cudaMemcpy(dest.y0 + offset, source.y0, num*sizeof(GPUFLOAT), cuda_type ) );
        checkCudaErrors( cudaMemcpy(dest.z0 + offset, source.z0, num*sizeof(GPUFLOAT), cuda_type ));
        checkCudaErrors( cudaMemcpy(dest.t0 + offset, source.t0, num*sizeof(GPUFLOAT), cuda_type ));
        checkCudaErrors( cudaMemcpy(dest.e0 + offset, source.e0, num*sizeof(GPUFLOAT), cuda_type ));        
        checkCudaErrors( cudaMemcpy(dest.band + offset, source.band, num*sizeof(int), cuda_type ));
        checkCudaErrors( cudaMemcpy(dest.kx + offset, source.kx, num*sizeof(GPUFLOAT), cuda_type ));
        checkCudaErrors( cudaMemcpy(dest.ky + offset, source.ky, num*sizeof(GPUFLOAT), cuda_type ));
        checkCudaErrors( cudaMemcpy(dest.kz + offset, source.kz, num*sizeof(GPUFLOAT), cuda_type ));
        checkCudaErrors( cudaMemcpy(dest.hole + offset, source.hole, num*sizeof(bool), cuda_type ));
    }

    void AvalancheMicroscopicGPU::TransferStackFromCPUToGPU(std::vector<AvalancheMicroscopic::Electron> &stackOld) {

        // assumes that stackOld contains only active particles and the current state of the 
        // GPU memory can be overwritten
        std::cout << "Transferring stack data to GPU..." << std::endl;
        stackOldGPU.stack_size = stackOld.size();

        // transfer the current particle list to the device
        // the stackTransfer object is used here to shift from array of structures to structure of arrays
        // a memcpy can then be done from stackTransfer to the device stackOldGPU
        unsigned int i{0};

        // temporary vector to hold the indices of the active particles. Allows a memcpy below.
        thrust::host_vector<int> index_transfer;
        for (auto elec : stackOld)
        {
            stackTransfer.x[i] = elec.x;
            stackTransfer.y[i] = elec.y;
            stackTransfer.z[i] = elec.z;
            stackTransfer.t[i] = elec.t;
            stackTransfer.x0[i] = elec.x0;
            stackTransfer.y0[i] = elec.y0;
            stackTransfer.z0[i] = elec.z0;
            stackTransfer.t0[i] = elec.t0;
            stackTransfer.e0[i] = elec.e0;
            stackTransfer.energy[i] = elec.energy;
            stackTransfer.band[i] = elec.band;
            stackTransfer.kx[i] = elec.kx;
            stackTransfer.ky[i] = elec.ky;
            stackTransfer.kz[i] = elec.kz;
            stackTransfer.hole[i] = elec.hole;
            stackTransfer.status[i] = elec.status;
            index_transfer.push_back(i);
            i++;
        }

        transferParticleStack(stackOldGPU, 0, stackTransfer, stackOld.size(), TransferType::HostToDevice);

        // initialise active indices to -1 (i.e. no active particle)
        thrust::device_ptr<int> activeIndexArray_thr_ptr(activeIndexArray);
        thrust::fill(activeIndexArray_thr_ptr, activeIndexArray_thr_ptr + MAXSTACKSIZE, -1);

        // initialise new indices to one past the max size
        thrust::device_ptr<int> newIndexArray_thr_ptr(newIndexArray);
        thrust::fill(newIndexArray_thr_ptr, newIndexArray_thr_ptr + MAXCREATEDPARTICLES * MAXSTACKSIZE, (MAXCREATEDPARTICLES * MAXSTACKSIZE) +1);

        // copy the initial indices of the active particles
        thrust::copy(index_transfer.begin(), index_transfer.end(), activeIndexArray_thr_ptr);
        numActiveParticles = index_transfer.size();
        m_nElectrons = numActiveParticles;

        std::cout << "Stack transfer complete" << std::endl;
    }

    void AvalancheMicroscopicGPU::TransferClassInternalInfo(AvalancheMicroscopic *src) {

        std::cout << "Transferring internal data to GPU..." << std::endl;
        memUsageSensor = src->m_sensor->CreateGPUTransferObject(m_sensor);

#ifdef USEPRECALCRNG
        memRNG = randomEngine.CreateGPUTransferObject(m_randomEngine);
#else
        checkCudaErrors( cudaMallocManaged( &m_randomEngine, sizeof(RandomEngineGPU) ) );
        memRNG = sizeof(RandomEngineGPU) + m_randomEngine->initCURandStates(randomEngine.GetSeed());
#endif
        m_randomEngine->setRandomEngineOnDevice();

        std::cout << "--------------- AvalancheMicroscopic Internals Transferred:" << std::endl;
        std::cout << "Stack Memory:  " << memUsageStack / (1024.*1024.) << " MB"  << std::endl;
        std::cout << "Sensor Memory:  " << memUsageSensor / (1024.*1024.) << " MB"  << std::endl;
        std::cout << "RNG Memory:  " << memRNG / (1024.*1024.*1024.) << " GB" << std::endl;
        std::cout << "TOTAL:   " << (memUsageStack + memUsageSensor + memRNG) / (1024.*1024.*1024.) << " GB" << std::endl;
        std::cout << "--------------- Transfer Complete." << std::endl;
    }

    // process the particle stack
    size_t AvalancheMicroscopicGPU::processParticleStack(unsigned int &num_active, unsigned int &num_new)
    {   
        // remove any active particles that now have status of -1 (should probably be < 0)
        // do this on the activeIndexArray as stackOld will contain *all* particles, even those that have terminated
        // sort the index/status array
        thrust::device_ptr<int> activeIndexArray_thr_ptr(activeIndexArray);
        auto num_active_particles_ptr = thrust::remove(activeIndexArray_thr_ptr, activeIndexArray_thr_ptr + numActiveParticles, -1);
        int num_active_particles = num_active_particles_ptr - activeIndexArray_thr_ptr;

        // Sort all newly created particles so they are at the start of the array
        // note: this is where the newIndexArray comes in as these values are less than the max if 
        // it points to a valid new particle
        // create a zip iterator and then sort based on the new particles key
        auto it = thrust::make_zip_iterator(thrust::make_tuple(stackNewGPU.x0, stackNewGPU.y0, stackNewGPU.z0, stackNewGPU.t0, stackNewGPU.e0,
                                                            stackNewGPU.band, stackNewGPU.kx, stackNewGPU.ky, stackNewGPU.kz, stackNewGPU.hole));

        thrust::sort_by_key(thrust::device, newIndexArray, newIndexArray+numActiveParticles*MAXCREATEDPARTICLES, it);
        int num_not_new_particles = thrust::count(thrust::device, newIndexArray, newIndexArray+numActiveParticles*MAXCREATEDPARTICLES, (MAXSTACKSIZE * 2) +1);
        int num_new_particles = numActiveParticles*MAXCREATEDPARTICLES - num_not_new_particles;
        
        // transfer the new particle info
        transferParticleStack(stackOldGPU, stackOldGPU.stack_size, stackNewGPU, num_new_particles*sizeof(double), TransferType::DeviceToDevice, true);
        
        // set the indices of the new particles in the index array
        setStatusArray<<<1 + num_new_particles / 512, 512>>>(activeIndexArray, num_active_particles, stackOldGPU.stack_size);
        cudaDeviceSynchronize();
        
        numActiveParticles = num_active_particles + num_new_particles;
        m_nElectrons += num_new_particles;
        stackOldGPU.stack_size += num_new_particles;

        num_active = numActiveParticles;
        num_new = num_new_particles;
        return numActiveParticles;
    }
    
    void AvalancheMicroscopicGPU::SetCUDADevice(int dev)
    {
        std::cout << "INFO:  Setting CUDA device to " << dev << std::endl;
        checkCudaErrors(cudaSetDevice(dev));
    }


    void AvalancheMicroscopicGPU::TransferStackFromGPUToCPU(std::vector<AvalancheMicroscopic::Electron> &stack, bool end_points) {
        AvalancheMicroscopic::Electron elec;
        stack.clear();
        transferParticleStack(stackTransfer, 0, stackOldGPU, stackOldGPU.stack_size, AvalancheMicroscopicGPU::TransferType::DeviceToHost);
        for (int i = 0; i < stackOldGPU.stack_size; i++)
        { 
            if (((!end_points) && (stackTransfer.status[i] == 0)) || 
                ((end_points) && (stackTransfer.status[i] != 0)))
            {
                elec.x = stackTransfer.x[i];
                elec.y = stackTransfer.y[i];
                elec.z = stackTransfer.z[i];
                elec.t = stackTransfer.t[i];
                elec.x0 = stackTransfer.x0[i];
                elec.y0 = stackTransfer.y0[i];
                elec.z0 = stackTransfer.z0[i];
                elec.t0 = stackTransfer.t0[i];
                elec.e0 = stackTransfer.e0[i];
                elec.energy = stackTransfer.energy[i];
                elec.band = stackTransfer.band[i];
                elec.kx = stackTransfer.kx[i];
                elec.ky = stackTransfer.ky[i];
                elec.kz = stackTransfer.kz[i];
                elec.hole = stackTransfer.hole[i];
                elec.status = stackTransfer.status[i];
                stack.push_back(elec);

                /*if (stackTransfer.status[i] == StatusAttached)
                {
                    --m_nElectronsGPU;
                }*/
            }
        }

        //GPUREMOVE: de-allocate everything
    }

    __device__ void Update(AvalancheMicroscopicGPU::ParticleStack &raw_ptr_stack, int thread_idx,
        const GPUFLOAT x, const GPUFLOAT y,
        const GPUFLOAT z, const GPUFLOAT t,
        const GPUFLOAT energy, const GPUFLOAT kx,
        const GPUFLOAT ky, const GPUFLOAT kz,
        const int band) 
    {

        raw_ptr_stack.x[thread_idx] = x;
        raw_ptr_stack.y[thread_idx] = y;
        raw_ptr_stack.z[thread_idx] = z;
        raw_ptr_stack.t[thread_idx] = t;
        raw_ptr_stack.energy[thread_idx] = energy;
        raw_ptr_stack.band[thread_idx] = band;
        raw_ptr_stack.kx[thread_idx] = kx;
        raw_ptr_stack.ky[thread_idx] = ky;
        raw_ptr_stack.kz[thread_idx] = kz;
    }

    __device__ void AddToStack(AvalancheMicroscopicGPU::ParticleStack &raw_ptr_stack, unsigned int &num_new_particles, int thread_idx,
        int *new_status_array,
        const GPUFLOAT x, const GPUFLOAT y,
        const GPUFLOAT z, const GPUFLOAT t,
        const GPUFLOAT energy, const GPUFLOAT kx,
        const GPUFLOAT ky, const GPUFLOAT kz,
        const int band, const bool hole) 
    {
        unsigned int step = MAXCREATEDPARTICLES;

        // only record the values that can be sorted
        raw_ptr_stack.x0[thread_idx*step+num_new_particles] = x;
        raw_ptr_stack.y0[thread_idx*step+num_new_particles] = y;
        raw_ptr_stack.z0[thread_idx*step+num_new_particles] = z;
        raw_ptr_stack.t0[thread_idx*step+num_new_particles] = t;
        raw_ptr_stack.e0[thread_idx*step+num_new_particles] = energy;
        raw_ptr_stack.band[thread_idx*step+num_new_particles] = band;
        raw_ptr_stack.kx[thread_idx*step+num_new_particles] = kx;
        raw_ptr_stack.ky[thread_idx*step+num_new_particles] = ky;
        raw_ptr_stack.kz[thread_idx*step+num_new_particles] = kz;
        raw_ptr_stack.hole[thread_idx*step+num_new_particles] = hole;

        new_status_array[thread_idx*step+num_new_particles] = thread_idx*step+num_new_particles;
        num_new_particles++;
    }

    __device__ void AddToStack(AvalancheMicroscopicGPU::ParticleStack &raw_ptr_stack, unsigned int &num_new_particles, int thread_idx,
        int *new_status_array,
        const GPUFLOAT x, const GPUFLOAT y,
        const GPUFLOAT z, const GPUFLOAT t,
        const GPUFLOAT energy, const bool hole) 
    {
        GPUFLOAT dx = 0., dy = 0., dz = 1.;
        RndmDirectionGPU(dx, dy, dz);
        AddToStack(raw_ptr_stack, num_new_particles, thread_idx, new_status_array, x, y, z, t, energy, dx, dy, dz, 0, hole);
    }

    __device__ void Terminate(GPUFLOAT x0, GPUFLOAT y0, GPUFLOAT z0, GPUFLOAT t0,
        GPUFLOAT& x1, GPUFLOAT& y1, GPUFLOAT& z1,
        GPUFLOAT& t1, SensorGPU* m_sensor) {

        const GPUFLOAT dx = x1 - x0;
        const GPUFLOAT dy = y1 - y0;
        const GPUFLOAT dz = z1 - z0;
        GPUFLOAT d = Mag(dx, dy, dz);
        while (d > BoundaryDistance) {
            d *= 0.5;
            const GPUFLOAT xm = 0.5 * (x0 + x1);
            const GPUFLOAT ym = 0.5 * (y0 + y1);
            const GPUFLOAT zm = 0.5 * (z0 + z1);
            const GPUFLOAT tm = 0.5 * (t0 + t1);
            // Check if the mid-point is inside the drift medium.
            GPUFLOAT ex = 0., ey = 0., ez = 0.;
            MediumGPU* medium = nullptr;
            int status = 0;
            m_sensor->ElectricField(xm, ym, zm, ex, ey, ez, medium, status);
            if ((status == 0) && (m_sensor->IsInArea(xm, ym, zm))) {
                x0 = xm;
                y0 = ym;
                z0 = zm;
                t0 = tm;
            } else {
                x1 = xm;
                y1 = ym;
                z1 = zm;
                t1 = tm;
            }
        }
    }

    __global__ void transportSingleParticleGPU(AvalancheMicroscopicGPU::ParticleStack raw_ptr_stack, 
                                                AvalancheMicroscopicGPU::ParticleStack raw_ptr_stack_new,
                                                int *all_status_array,
                                                int *new_status_array,
                                                SensorGPU *m_sensor,
                                                GPUFLOAT m_deltaCut,
                                                int id,
                                                bool useBandStructure,
                                                GPUFLOAT c1,
                                                GPUFLOAT c2,
                                                GPUFLOAT fLim,
                                                GPUFLOAT fInv,
                                                unsigned int max_thread_idx,
                                                int debug_electron = -1
                                                )
    {
        //GPU_REMOVE: std::vector<std::pair<double, double> > stackPhotons;
        //GPU_REMOVE: std::vector<std::pair<int, double> > secondaries;
        int num_secondaries;
        int secondaries_type[MAXCREATEDPARTICLES];
        GPUFLOAT secondaries_energy[MAXCREATEDPARTICLES];
    
        // find the thread id                                                                                                                                                            
        int thread_idx = (threadIdx.x + blockIdx.x * blockDim.x);
        if (thread_idx >= max_thread_idx)
        {
            return;
        }
        
        int particle_idx{all_status_array[thread_idx]};

        // reset new particle info
        for (int i=0; i < MAXCREATEDPARTICLES; i++)
        {
	  new_status_array[thread_idx*MAXCREATEDPARTICLES+i] = (MAXSTACKSIZE * 2) +1;
        }

        // sort out the RNG
        ResetSeed();
        
        // reset new particles
        unsigned int num_new_particles{0};

        // initialiase some things
        MediumGPU *medium{nullptr};
        
        // Get an electron/hole from the stack.
        //int status = raw_ptr_stack.status[particle_idx];
        GPUFLOAT x = raw_ptr_stack.x[particle_idx];
        GPUFLOAT y = raw_ptr_stack.y[particle_idx];
        GPUFLOAT z = raw_ptr_stack.z[particle_idx];
        GPUFLOAT t = raw_ptr_stack.t[particle_idx];
        GPUFLOAT en = raw_ptr_stack.energy[particle_idx];
        int band = raw_ptr_stack.band[particle_idx];
        GPUFLOAT kx = raw_ptr_stack.kx[particle_idx];
        GPUFLOAT ky = raw_ptr_stack.ky[particle_idx];
        GPUFLOAT kz = raw_ptr_stack.kz[particle_idx];
        bool hole = raw_ptr_stack.hole[particle_idx];

        bool ok = true;
        // bail out if we don't have a valid particle to transport
        //if (raw_ptr_stack.status[thread_idx] != 0) {
        //    return;
        //}

        // Count number of collisions between updates.
        unsigned int nCollTemp = 0;

        // Get the local electric field and medium.
        GPUFLOAT ex = 0., ey = 0., ez = 0.;
        int status = 0;
        m_sensor->ElectricField(x, y, z, ex, ey, ez, medium, status);

        // Sign change for electrons.
        if (!hole) {
            ex = -ex;
            ey = -ey;
            ez = -ez;
        }

#ifndef GPUOPTIMISE
        if (thread_idx == debug_electron) printf("GPU %d (A, line %d):   %.8f %.8f %.8f %.8f %.8f %.8f %.8f\n", thread_idx, __LINE__, x, y, z, en, ex, ey, ez);
#endif

        if (status != 0) {
            // Electron is not inside a drift medium.
            Update(raw_ptr_stack, particle_idx, x, y, z, t, en, kx, ky, kz, band);
            raw_ptr_stack.status[particle_idx] = StatusLeftDriftMedium;
            all_status_array[thread_idx] = -1;
            //GPUREMOVE: AddToEndPoints(*it, hole);
            return;
        }
        /* GPUREMOVE:  
        // If switched on, get the local magnetic field.
        GPUFLOAT bx = 0., by = 0., bz = 0.;
        // Cyclotron frequency.
        GPUFLOAT omega = 0.;
        // Ratio of transverse electric field component and magnetic field.
        GPUFLOAT ezovb = 0.;
        GPUFLOAT rot[3][3];
        if (m_useBfield) {
            m_sensor->MagneticField(x, y, z, bx, by, bz, status);
            const double scale = hole ? Tesla2Internal : -Tesla2Internal;
            bx *= scale;
            by *= scale;
            bz *= scale;
            const double bmag = Mag(bx, by, bz);
            // Calculate the rotation matrix to a local coordinate system 
            // with B along x and E in the x-z plane.
            RotationMatrix(bx, by, bz, bmag, ex, ey, ez, rot);
            // Calculate the cyclotron frequency.
            omega = OmegaCyclotronOverB * bmag;
            // Calculate the electric field in the local frame.
            ToLocal(rot, ex, ey, ez, ex, ey, ez);
            ezovb = bmag > Small ? ez / bmag : 0.;
        }*/
        // Trace the electron/hole.
        while (1) {
#ifndef GPUOPTIMISE
            if (thread_idx == debug_electron) printf("GPU %d (B, line %d):   %.8f %.8f %.8f %.8f %.8f %.8f %.8f\n", thread_idx, __LINE__, x, y, z, en, ex, ey, ez);
#endif
            bool isNullCollision = false;

            // Make sure the electron energy exceeds the transport cut.
            if (en < m_deltaCut) {
                Update(raw_ptr_stack, particle_idx, x, y, z, t, en, kx, ky, kz, band);
                raw_ptr_stack.status[particle_idx] = StatusBelowTransportCut;
                all_status_array[thread_idx] = -1;
                //GPUREMOVE: AddToEndPoints(*it, hole);
                ok = false;
                break;
            }

            /* GPUREMOVE: // Fill the energy distribution histogram.
            if (hole && m_histHoleEnergy) {
                m_histHoleEnergy->Fill(en);
            } else if (!hole && m_histElectronEnergy) {
                m_histElectronEnergy->Fill(en);
            }

            // Check if the electron is within the specified time window.
            if (m_hasTimeWindow && (t < m_tMin || t > m_tMax)) {
                Update(it, x, y, z, t, en, kx, ky, kz, band);
                (*it).status = StatusOutsideTimeWindow;
                AddToEndPoints(*it, hole);
                if (m_debug) PrintStatus(hdr, "left the time window", x, y, z, hole);
                ok = false;
                break;
            }*/

            /* GPUREMOVE
            if (medium->GetId() != id) {
                // Medium has changed.
                if (!medium->IsMicroscopic()) {
                    // Electron/hole has left the microscopic drift medium.
                    Update(raw_ptr_stack, thread_idx, x, y, z, t, en, kx, ky, kz, band);
                    raw_ptr_stack.status[thread_idx] = StatusLeftDriftMedium;
                    //GPUREMOVE: AddToEndPoints(*it, hole);
                    ok = false;
                    break;
                }
                id = medium->GetId();
                useBandStructure =
                    (medium->IsSemiconductor() && m_useBandStructureDefault);
                // Update the null-collision rate.
                fLim = medium->GetElectronNullCollisionRate(band);
                if (fLim <= 0.) {
                    std::cerr << hdr << "Got null-collision rate <= 0.\n";
                    return false;
                }
                fInv = 1. / fLim;
            }*/

        GPUFLOAT a1 = 0., a2 = 0.;
        // Initial velocity.
        GPUFLOAT vx = 0., vy = 0., vz = 0.;
        /* GPUREMOVE
        if (m_useBfield) {
            // Calculate the velocity vector in the local frame.
            const double vmag = c1 * sqrt(en);
            ToLocal(rot, vmag * kx, vmag * ky, vmag * kz, vx, vy, vz);
            a1 = vx * ex;
            a2 = c2 * ex * ex;
            if (omega > Small) {
                vy -= ezovb;
            } else {
                a1 += vz * ez;
                a2 += c2 * ez * ez;
            }
        } else if (useBandStructure) {
            en = medium->GetElectronEnergy(kx, ky, kz, vx, vy, vz, band);
        } 
        else
            */
        {
            // No band structure, no magnetic field.
            // Calculate the velocity vector.
            const GPUFLOAT vmag = c1 * sqrt(en);
            vx = vmag * kx;
            vy = vmag * ky;
            vz = vmag * kz;
            a1 = vx * ex + vy * ey + vz * ez;
            a2 = c2 * (ex * ex + ey * ey + ez * ez);
        }

        /* GPUREMOVE
        if (m_userHandleStep) {
            m_userHandleStep(x, y, z, t, en, kx, ky, kz, hole);
        }*/

        // Energy after the step.
        GPUFLOAT en1 = en;
        // Determine the timestep.
        GPUFLOAT dt = 0.;
        // Parameters for B-field stepping.
        //GPURMOVE: GPUFLOAT cphi = 1., sphi = 0.;
        //GPURMOVE: GPUFLOAT a3 = 0., a4 = 0.;

        if (thread_idx == debug_electron) printf("GPU %d (C, line %d):   %.8f %.8f %.8f %.8f %.8f %.8f %.8f\n", thread_idx, __LINE__, x, y, z, en, ex, ey, ez);

        while (1) {
            // Sample the flight time.
            const GPUFLOAT r = RndmUniformPosGPU();
            dt += -log(r) * fInv;
            // Calculate the energy after the proposed step.
            /*GPUREMOVE
            if (m_useBfield) {
                en1 = en + (a1 + a2 * dt) * dt;
                if (omega > Small) {
                    cphi = cos(omega * dt);
                    sphi = sin(omega * dt);
                    a3 = sphi / omega;
                    a4 = (1. - cphi) / omega;
                    en1 += ez * (vz * a3 - vy * a4);
                }
            } else if (useBandStructure) {
                const double cdt = dt * SpeedOfLight;
                const double kx1 = kx + ex * cdt;
                const double ky1 = ky + ey * cdt;
                const double kz1 = kz + ez * cdt;
                double vx1 = 0., vy1 = 0., vz1 = 0.;
                en1 = medium->GetElectronEnergy(kx1, ky1, kz1, 
                                                vx1, vy1, vz1, band);
            } else*/ {
                en1 = en + (a1 + a2 * dt) * dt;
            }
            en1 = fmax(en1, SmallGPU);

            // Get the real collision rate at the updated energy.
            GPUFLOAT fReal = medium->GetElectronCollisionRate(en1, band);
            /*GPUREMOVE
            if (fReal <= 0.) {
                printf("Got collision rate <= 0 at %f.\n", en1);
                return;
            }
            if (fReal > fLim) {
                // Real collision rate is higher than null-collision rate.
                dt += log(r) * fInv;
                // Increase the null collision rate and try again.
                printf("GPU: Increasing null-collision rate by 5%.\n");
                //GPUREMOVE if (useBandStructure) std::cerr << "    Band " << band << "\n";
                fLim *= 1.05;
                fInv = 1. / fLim;
                continue;
            }*/
            // Check for real or null collision.
            if (RndmUniformGPU() <= fReal * fInv) break;
            /* GPUREMOVE if (m_useNullCollisionSteps) {
                isNullCollision = true;
                break;
            }*/
        }
        ++nCollTemp;

        if (!ok) break;
        if (thread_idx == debug_electron) printf("GPU %d (D, line %d):   %.8f %.8f %.8f %.8f %.8f %.8f %.8f\n", thread_idx, __LINE__, x, y, z, en, ex, ey, ez);
        // Increase the collision counter.
        
        // Calculate the direction at the instant before the collision
        // and the proposed new position.
        GPUFLOAT kx1 = 0., ky1 = 0., kz1 = 0.;
        GPUFLOAT dx = 0., dy = 0., dz = 0.;
        /* GPUREMOVE:
        if (m_useBfield) {
            // Calculate the new velocity.
            double vx1 = vx + 2. * c2 * ex * dt;
            double vy1 = vy * cphi + vz * sphi + ezovb;
            double vz1 = vz * cphi - vy * sphi;
            if (omega < Small) vz1 += 2. * c2 * ez * dt;
            // Rotate back to the global frame and normalise.
            ToGlobal(rot, vx1, vy1, vz1, kx1, ky1, kz1);
            const double scale = 1. / Mag(kx1, ky1, kz1);
            kx1 *= scale;
            ky1 *= scale;
            kz1 *= scale;
            // Calculate the step in coordinate space.
            dx = vx * dt + c2 * ex * dt * dt;
            if (omega > Small) {
            dy = vy * a3 + vz * a4 + ezovb * dt;
            dz = vz * a3 - vy * a4;
            } else {
            dy = vy * dt;
            dz = vz * dt + c2 * ez * dt * dt;
            } 
            // Rotate back to the global frame.
            ToGlobal(rot, dx, dy, dz, dx, dy, dz);
        } else if (useBandStructure) {
            // Update the wave-vector.
            const double cdt = dt * SpeedOfLight;
            kx1 = kx + ex * cdt;
            ky1 = ky + ey * cdt;
            kz1 = kz + ez * cdt;
            double vx1 = 0., vy1 = 0, vz1 = 0.;
            en1 = medium->GetElectronEnergy(kx1, ky1, kz1, 
                                            vx1, vy1, vz1, band);
            dx = 0.5 * (vx + vx1) * dt;
            dy = 0.5 * (vy + vy1) * dt;
            dz = 0.5 * (vz + vz1) * dt;
        } 
        else */
        {
            // Update the direction.
            const GPUFLOAT b1 = sqrt(en / en1);
            const GPUFLOAT b2 = 0.5 * c1 * dt / sqrt(en1);
            kx1 = kx * b1 + ex * b2;
            ky1 = ky * b1 + ey * b2;
            kz1 = kz * b1 + ez * b2;

            // Calculate the step in coordinate space.
            const GPUFLOAT b3 = dt * dt * c2;
            dx = vx * dt + ex * b3;
            dy = vy * dt + ey * b3;
            dz = vz * dt + ez * b3;
        }
        GPUFLOAT x1 = x + dx;
        GPUFLOAT y1 = y + dy;
        GPUFLOAT z1 = z + dz;
        GPUFLOAT t1 = t + dt;
        // Get the electric field and medium at the proposed new position.
#ifndef GPUOPTIMISE
        if (thread_idx == debug_electron) printf("GPU %d (E, line %d):   %.8f %.8f %.8f %.8f %.8f %.8f %.8f\n", thread_idx, __LINE__, x, y, z, en, ex, ey, ez);
#endif
        m_sensor->ElectricField(x1, y1, z1, ex, ey, ez, medium, status);

        if (!hole) {
            ex = -ex;
            ey = -ey;
            ez = -ez;
        }
        // Check if the electron is still inside a drift medium/the drift area.
        if ((status != 0 ) || (!m_sensor->IsInArea(x1, y1, z1))) {
            // Try to terminate the drift line close to the boundary (endpoint
            // outside the drift medium/drift area) using iterative bisection.
            
            Terminate(x, y, z, t, x1, y1, z1, t1, m_sensor);
            /* GPUREMOVE
            if (m_doSignal) {
            const int q = hole ? 1 : -1;
            m_sensor->AddSignal(q, t, t1, x, y, z, x1, y1, z1, 
                                m_integrateWeightingField,
                                m_useWeightingPotential);
            }*/
            Update(raw_ptr_stack, particle_idx, x1, y1, z1, t1, en, kx1, ky1, kz1, band);
            
            if (status != 0) {
                raw_ptr_stack.status[particle_idx] = StatusLeftDriftMedium;
                all_status_array[thread_idx] = -1;
            } else {
                raw_ptr_stack.status[particle_idx] = StatusLeftDriftArea;
                all_status_array[thread_idx] = -1;
            }
            //GPUREMOVE: AddToEndPoints(*it, hole);
            ok = false;
            break;
        }
        
        // Check if the electron/hole has crossed a wire.
        /* GPUREMOVE:
        GPUFLOAT xc = x, yc = y, zc = z;
        GPUFLOAT rc = 0.;
        
        if (m_sensor->IsWireCrossed(x, y, z, x1, y1, z1, 
                                    xc, yc, zc, false, rc)) {
            const double dc = Mag(xc - x, yc - y, zc - z);
            const double tc = t + dt * dc / Mag(dx, dy, dz);
            // If switched on, calculated the induced signal over this step.
            if (m_doSignal) {
            const int q = hole ? 1 : -1;
            m_sensor->AddSignal(q, t, tc, x, y, z, xc, yc, zc,
                                m_integrateWeightingField,
                                m_useWeightingPotential);
            }
            Update(it, xc, yc, zc, tc, en, kx1, ky1, kz1, band);
            (*it).status = StatusLeftDriftMedium;
            AddToEndPoints(*it, hole);
            ok = false;
            if (m_debug) PrintStatus(hdr, "hit a wire", x, y, z, hole);
            break;
        }

        // If switched on, calculate the induced signal.
        if (m_doSignal) {
            const int q = hole ? 1 : -1;
            m_sensor->AddSignal(q, t, t + dt, x, y, z, x1, y1, z1,
                                m_integrateWeightingField,
                                m_useWeightingPotential);
        }*/

        // Update the coordinates.
        x = x1;
        y = y1;
        z = z1;
        t = t1;
        /* GPUREMOVE:
        // If switched on, get the magnetic field at the new location.
        if (m_useBfield) {
            m_sensor->MagneticField(x, y, z, bx, by, bz, status);
            const double scale = hole ? Tesla2Internal : -Tesla2Internal;
            bx *= scale;
            by *= scale;
            bz *= scale;
            const double bmag = Mag(bx, by, bz);
            // Update the rotation matrix.
            RotationMatrix(bx, by, bz, bmag, ex, ey, ez, rot);
            omega = OmegaCyclotronOverB * bmag;
            // Calculate the electric field in the local frame.
            ToLocal(rot, ex, ey, ez, ex, ey, ez);
            ezovb = bmag > Small ? ez / bmag : 0.;
        }*/

        if (isNullCollision) {
            en = en1;
            kx = kx1;
            ky = ky1;
            kz = kz1;
            continue;
        }

        // Get the collision type and parameters.
        int cstype = 0;
        int level = 0;
        int ndxc = 0;
        medium->GetElectronCollision(en1, cstype, level, en, kx1,
                                        ky1, kz1, secondaries_type, secondaries_energy, num_secondaries, ndxc, band);
        /* GPUREMOVE:
        // If activated, histogram the distance with respect to the
        // last collision.
        if (m_histDistance && !m_distanceHistogramType.empty()) {
            for (const auto& htype : m_distanceHistogramType) {
            if (htype != cstype) continue;
            switch (m_distanceOption) {
                case 'x':
                m_histDistance->Fill((*it).xLast - x);
                break;
                case 'y':
                m_histDistance->Fill((*it).yLast - y);
                break;
                case 'z':
                m_histDistance->Fill((*it).zLast - z);
                break;
                case 'r':
                m_histDistance->Fill(Mag((*it).xLast - x, 
                                            (*it).yLast - y, 
                                            (*it).zLast - z));
                break;
            }
            (*it).xLast = x;
            (*it).yLast = y;
            (*it).zLast = z;
            break;
            }
        }

        if (m_userHandleCollision) {
            m_userHandleCollision(x, y, z, t, cstype, level, medium, en1,
                                en, kx, ky, kz, kx1, ky1, kz1);
        }*/

        
        switch (cstype) {
            // Elastic collision
            case ElectronCollisionTypeElastic:
                break;
            // Ionising collision
            case ElectronCollisionTypeIonisation:
            /* GPUREMOVE
                if (m_viewer && m_plotIonisations) {
                    m_viewer->AddIonisationMarker(x, y, z);
                }
                if (m_userHandleIonisation) {
                    m_userHandleIonisation(x, y, z, t, cstype, level, medium);
                }
                for (const auto& secondary : secondaries) {
                    if (secondary.first == IonProdTypeElectron) {
                    const GPUFLOAT esec = std::max(secondary.second, Small);
                    if (m_histSecondary) m_histSecondary->Fill(esec);
                    // Increment the electron counter.
                    ++m_nElectrons;
                    if (!aval) continue;
                    // Add the secondary electron to the stack.
                    if (useBandStructure) {
                        GPUFLOAT kxs = 0., kys = 0., kzs = 0.;
                        int bs = -1;
                        medium->GetElectronMomentum(esec, kxs, kys, kzs, bs);
                        AddToStack(x, y, z, t, esec, kxs, kys, kzs, bs, false,
                                    stackNew);
                    } else {
                        AddToStack(x, y, z, t, esec, false, stackNew);
                    }
                    } else if (secondary.first == IonProdTypeHole) {
                    const GPUFLOAT esec = std::max(secondary.second, Small);
                    // Increment the hole counter.
                    ++m_nHoles;
                    if (!aval) continue;
                    // Add the secondary hole to the stack.
                    if (useBandStructure) {
                        GPUFLOAT kxs = 0., kys = 0., kzs = 0.;
                        int bs = -1;
                        medium->GetElectronMomentum(esec, kxs, kys, kzs, bs);
                        AddToStack(x, y, z, t, esec, kxs, kys, kzs, bs, true,
                                    stackNew);
                    } else {
                        AddToStack(x, y, z, t, esec, true, stackNew);
                    }
                    } else if (secondary.first == IonProdTypeIon) {
                    ++m_nIons;
                    }
                }
                secondaries.clear();*/

                for (int k = 0; k < num_secondaries; k++) {

                    if (secondaries_type[k] == IonProdTypeElectron) {
                        const GPUFLOAT esec = fmax(secondaries_energy[k], SmallGPU);
                        
                        // Add the secondary electron to the stack.                                                                                                                                                                                                                  
                        AddToStack(raw_ptr_stack_new, num_new_particles, thread_idx, new_status_array, x, y, z, t, esec, false);
                                                                                                                                                                                                                       
                    } else if (secondaries_type[k] == IonProdTypeHole) {
                        const GPUFLOAT esec = fmax(secondaries_energy[k], SmallGPU);
            
                        // Add the secondary hole to the stack.                                                                                                                                                                                                                      
                        printf("ERROR: HOLE CREATEAD!\n");
                        //GPUAddToStack(x, y, z, t, esec, true, stackNew, new_stack_idx);                                                                                                                                                                                            
                
                    }
                }
                
                num_secondaries = 0;

                break;
            // Attachment
            case ElectronCollisionTypeAttachment:
            /* GPUREMOVE
                if (m_viewer && m_plotAttachments) {
                    m_viewer->AddAttachmentMarker(x, y, z);
                }
                if (m_userHandleAttachment) {
                    m_userHandleAttachment(x, y, z, t, cstype, level, medium);
                }*/
                Update(raw_ptr_stack, particle_idx, x1, y1, z1, t1, en, kx1, ky1, kz1, band);
                raw_ptr_stack.status[particle_idx] = StatusAttached;
                all_status_array[thread_idx] = -1;
                /*GPUREMOVE if (hole) {
                    m_endpointsHoles.push_back(*it);
                    --m_nHoles;
                } else {
                    m_endpointsElectrons.push_back(*it);
                    --m_nElectrons;
                }*/
                ok = false;
                break;
            // Inelastic collision
            case ElectronCollisionTypeInelastic:
                /*GPUREMOVEif (m_userHandleInelastic) {
                    m_userHandleInelastic(x, y, z, t, cstype, level, medium);
                }*/
                break;
            // Excitation
            case ElectronCollisionTypeExcitation:
                if (ndxc <= 0) break;
            /*GPUREMOVE
                if (m_viewer && m_plotExcitations) {
                    m_viewer->AddExcitationMarker(x, y, z);
                }
                if (m_userHandleInelastic) {
                    m_userHandleInelastic(x, y, z, t, cstype, level, medium);
                }
                if (ndxc <= 0) break;
                // Get the electrons/photons produced in the deexcitation cascade.
                stackPhotons.clear();
                for (int j = ndxc; j--;) {
                    double tdx = 0., sdx = 0., edx = 0.;
                    int typedx = 0;
                    if (!medium->GetDeexcitationProduct(j, tdx, sdx, typedx, edx)) {
                    std::cerr << hdr << "Cannot retrieve deexcitation product " << j
                                << "/" << ndxc << ".\n";
                    break;
                    }

                    if (typedx == DxcProdTypeElectron) {
                    // Penning ionisation
                    double xp = x, yp = y, zp = z;
                    if (sdx > Small) {
                        // Randomise the point of creation.
                        double dxp = 0., dyp = 0., dzp = 0.;
                        RndmDirection(dxp, dyp, dzp);
                        xp += sdx * dxp;
                        yp += sdx * dyp;
                        zp += sdx * dzp;
                    }
                    // Get the electric field and medium at this location.
                    Medium* med = nullptr;
                    double fx = 0., fy = 0., fz = 0.;
                    m_sensor->ElectricField(xp, yp, zp, fx, fy, fz, med, status);
                    // Check if this location is inside a drift medium/area.
                    if (status != 0 || !m_sensor->IsInArea(xp, yp, zp)) continue;
                    // Make sure we haven't jumped across a wire.
                    if (m_sensor->IsWireCrossed(x, y, z, xp, yp, zp, xc, yc, zc, 
                                                false, rc)) {
                        continue;
                    }
                    // Increment the electron and ion counters.
                    ++m_nElectrons;
                    ++m_nIons;
                    if (m_userHandleIonisation) {
                        m_userHandleIonisation(xp, yp, zp, t, cstype, level, medium);
                    }
                    if (!aval) continue;
                    // Add the Penning electron to the list.
                    AddToStack(xp, yp, zp, t + tdx, std::max(edx, Small), false,
                                stackNew);
                    } else if (typedx == DxcProdTypePhoton && m_usePhotons &&
                                edx > m_gammaCut) {
                    // Radiative de-excitation
                    stackPhotons.emplace_back(std::make_pair(t + tdx, edx));
                    }
                }

                // Transport the photons (if any)
                if (aval) {
                    for (const auto& ph : stackPhotons) {
                    TransportPhoton(x, y, z, ph.first, ph.second, stackNew);
                    }
                }*/
                break;
            // Super-elastic collision
            case ElectronCollisionTypeSuperelastic:
                break;
            // Virtual/null collision
            case ElectronCollisionTypeVirtual:
                break;
            // Acoustic phonon scattering (intravalley)
            case ElectronCollisionTypeAcousticPhonon:
                break;
            // Optical phonon scattering (intravalley)
            case ElectronCollisionTypeOpticalPhonon:
                break;
            // Intervalley scattering (phonon assisted)
            case ElectronCollisionTypeIntervalleyG:
            case ElectronCollisionTypeIntervalleyF:
            case ElectronCollisionTypeInterbandXL:
            case ElectronCollisionTypeInterbandXG:
            case ElectronCollisionTypeInterbandLG:
                break;
            // Coulomb scattering
            case ElectronCollisionTypeImpurity:
                break;
            default:
                printf( "Unknown collision type.\n");
                ok = false;
                break;
        }
    
            // Continue with the next electron/hole?
            // GPUREMOVE m_nCollSKip
            if (!ok || nCollTemp > 100 ||
                cstype == ElectronCollisionTypeIonisation ||
                cstype == ElectronCollisionTypeExcitation ||
                cstype == ElectronCollisionTypeAttachment
                /*GPUREMOVE||
                (m_plotExcitations && cstype == ElectronCollisionTypeExcitation) ||
                (m_plotAttachments && cstype == ElectronCollisionTypeAttachment)*/) {
                break;
            }
            kx = kx1;
            ky = ky1;
            kz = kz1;
        }
    
        if (!ok) return;

        if (!useBandStructure) {
            // Normalise the direction vector.
            const GPUFLOAT scale = 1. / Mag(kx, ky, kz);
            kx *= scale;
            ky *= scale;
            kz *= scale;
        }

        // Update the stack.
        Update(raw_ptr_stack, particle_idx, x, y, z, t, en, kx, ky, kz, band);
        /* GPUREMOVE:
        // Add a new point to the drift line (if enabled).
        if (m_useDriftLines) {
            point newPoint;
            newPoint.x = x;
            newPoint.y = y;
            newPoint.z = z;
            newPoint.t = t;
            (*it).driftLine.push_back(std::move(newPoint));
        }
        */
    }


    bool AvalancheMicroscopicGPU::transportParticleStack(const bool aval, AvalancheMicroscopic *aval_ptr, int id, bool useBandStructure,
                              const double c1, const double c2, double fLim, double fInv, int debug_electron) {

        // check we have enough space
        if (stackOldGPU.stack_size > MAXPARTICLES)
        {
            std::cout << "ERROR:  particle stack overflow. Please increase MAXPARTICLES and recompile." << std::endl;
            return false;
        }

        // Send off the threads to transport each particle
        transportSingleParticleGPU<<<1 + numActiveParticles / 256, 256>>>(stackOldGPU, 
            stackNewGPU,
            activeIndexArray,
            newIndexArray,
            m_sensor,
            aval_ptr->m_deltaCut,
            id,
            useBandStructure,
            c1,
            c2,
            fLim,
            fInv,
            numActiveParticles,
            debug_electron);
        cudaDeviceSynchronize();

        return true;
    }


}
