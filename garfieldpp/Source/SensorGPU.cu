#define __GPUCOMPILE__
#include "Sensor.cc"
#undef __GPUCOMPILE__

#include "Garfield/Sensor.hh"

namespace Garfield {

  double Sensor::CreateGPUTransferObject(SensorGPU *&sensor_gpu)
  {
      // create main sensor GPU class
      checkCudaErrors(cudaMallocManaged(&sensor_gpu, sizeof(SensorGPU)));
      double alloc{sizeof(SensorGPU)};

      // transfer sizes
      sensor_gpu->m_xMinUser = m_xMinUser;
      sensor_gpu->m_yMinUser = m_yMinUser;
      sensor_gpu->m_zMinUser = m_zMinUser;
      sensor_gpu->m_xMaxUser = m_xMaxUser;
      sensor_gpu->m_yMaxUser = m_yMaxUser;
      sensor_gpu->m_zMaxUser = m_zMaxUser;

      // create arrays
      alloc += CreateGPUObjectArrayFromVector<ComponentBase*, ComponentBaseGPU**>(m_components, 
        sensor_gpu->m_numComponents, sensor_gpu->m_components);

      return alloc;
  }
}