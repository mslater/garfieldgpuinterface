#include "Garfield/GPUFunctions.h"
#define __GPUCOMPILE__
#include "Garfield/ComponentBaseGPU.h"
#undef __GPUCOMPILE__
#include "Garfield/ComponentAnsys123.hh"


#define __GPUCOMPILE__

namespace Garfield {

    #include "ComponentFieldMap.cc"
    #include "ComponentAnsys123.cc"

    __device__ void ComponentBaseGPU::ElectricField(const double xin, const double yin,
                                      const double zin, double& ex, double& ey,
                                      double& ez, MediumGPU*& m,
                                      int& status)
    {
        switch(m_ComponentBaseType)
        {
        case ComponentBaseType::ComponentBase:
            {
                return;
                break;
            }
        case ComponentBaseType::ComponentAnsys123:
            {
                ElectricField__ComponentAnsys123(xin, yin, zin, ex, ey, ez, m, status);
                break;
            }

        }
    }

    double ComponentBase::CreateGPUTransferObject(ComponentBaseGPU *&comp_gpu)
    {
        // copy periodicity and min/max
        for (int i = 0; i < 3; i++)
        {
            comp_gpu->m_periodic[i] = m_periodic[i];
            comp_gpu->m_mirrorPeriodic[i] = m_mirrorPeriodic[i];
            comp_gpu->m_axiallyPeriodic[i] = m_axiallyPeriodic[i];
            comp_gpu->m_rotationSymmetric[i] = m_rotationSymmetric[i];
        }

        comp_gpu->m_ComponentBaseType = ComponentBaseGPU::ComponentBaseType::ComponentBase;

        return 0;
    }

    double ComponentFieldMap::CreateGPUTransferObject(ComponentBaseGPU *&comp_gpu)
    {
        double alloc{0};

        // Check if bounding boxes of elements have been computed
        if (!m_cacheElemBoundingBoxes) {
            std::cout << m_className << "::CreateGPUTransferObject:\n"
                    << "    Caching the bounding boxes of all elements...";
            CalculateElementBoundingBoxes();
            std::cout << " done.\n";
            m_cacheElemBoundingBoxes = true;
        }
        
        // initialise the tetraheral tree
        if (m_useTetrahedralTree) {
            if (!m_isTreeInitialized) {
                if (!InitializeTetrahedralTree()) {
                    std::cerr << m_className << "::FindElement13:\n";
                    std::cerr << "    Tetrahedral tree initialization failed.\n";
                    return alloc;
                }
            }
        }

        // copy periodicity and min/max
        for (int i = 0; i < 3; i++)
        {
            comp_gpu->m_mapmin[i] = m_mapmin[i];
            comp_gpu->m_mapmax[i] = m_mapmax[i];
            comp_gpu->m_mapamin[i] = m_mapamin[i];
            comp_gpu->m_mapamax[i] = m_mapamax[i];
        }

        // materials
        comp_gpu->numMaterials = materials.size();
        checkCudaErrors(cudaMallocManaged(&(comp_gpu->materials), sizeof(ComponentBaseGPU::Material) * comp_gpu->numMaterials));
        alloc += sizeof(ComponentBaseGPU::Material) * comp_gpu->numMaterials;

        for (int i = 0; i < comp_gpu->numMaterials; i++)
        {
            comp_gpu->materials[i].eps = materials[i].eps;
            comp_gpu->materials[i].ohm = materials[i].ohm;
            comp_gpu->materials[i].driftmedium = materials[i].driftmedium;

            if (materials[i].medium)
            {
                alloc += materials[i].medium->CreateGPUTransferObject(comp_gpu->materials[i].medium);
            }
        }

        // tetrahedral tree related things
        checkCudaErrors(cudaMallocManaged(&(comp_gpu->m_lastElement), sizeof(int) * MAXSTACKSIZE));
        alloc += sizeof(int) * MAXSTACKSIZE;

        for (int i = 0; i < MAXSTACKSIZE; i++)
        {
            comp_gpu->m_lastElement[i] = -1;
        }
        comp_gpu->m_checkMultipleElement = m_checkMultipleElement;
        comp_gpu->m_useTetrahedralTree = m_useTetrahedralTree;

        // elements
        comp_gpu->numElements = elements.size();
        checkCudaErrors(cudaMallocManaged(&(comp_gpu->elements), sizeof(ComponentBaseGPU::Element) * comp_gpu->numElements));
        alloc += sizeof(ComponentBaseGPU::Element) * comp_gpu->numElements;

        for (int i = 0; i < comp_gpu->numElements; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                comp_gpu->elements[i].emap[j] = elements[i].emap[j];
            }

            comp_gpu->elements[i].matmap = elements[i].matmap;
            comp_gpu->elements[i].degenerate = elements[i].degenerate;
            comp_gpu->elements[i].xmin = elements[i].xmin;
            comp_gpu->elements[i].ymin = elements[i].ymin;
            comp_gpu->elements[i].zmin = elements[i].zmin;
            comp_gpu->elements[i].xmax = elements[i].xmax;
            comp_gpu->elements[i].ymax = elements[i].ymax;
            comp_gpu->elements[i].zmax = elements[i].zmax;
        }

        comp_gpu->numNodes = nodes.size();
        checkCudaErrors(cudaMallocManaged(&(comp_gpu->nodes), sizeof(ComponentBaseGPU::Node) * comp_gpu->numNodes));
        alloc += sizeof(ComponentBaseGPU::Node) * comp_gpu->numNodes;

        for (int i = 0; i < comp_gpu->numNodes; i++)
        {
            comp_gpu->nodes[i].x = nodes[i].x;
            comp_gpu->nodes[i].y = nodes[i].y;
            comp_gpu->nodes[i].z = nodes[i].z;
            comp_gpu->nodes[i].v = nodes[i].v;
        }

        if (comp_gpu->m_useTetrahedralTree)
        {
            alloc += m_tetTree->CreateGPUTransferObject(comp_gpu->m_tetTree);
        }

        comp_gpu->nElements = nElements;

        comp_gpu->m_ComponentBaseType = ComponentBaseGPU::ComponentBaseType::ComponentFieldMap;

        return alloc;
    }

    double ComponentAnsys123::CreateGPUTransferObject(ComponentBaseGPU *&comp_gpu)
    {
        // create main sensor GPU class
        checkCudaErrors(cudaMallocManaged(&comp_gpu, sizeof(ComponentBaseGPU)));
        double alloc{sizeof(ComponentBaseGPU)};

        alloc += ComponentFieldMap::CreateGPUTransferObject(comp_gpu);
        alloc += ComponentBase::CreateGPUTransferObject(comp_gpu);

        comp_gpu->m_ComponentBaseType = ComponentBaseGPU::ComponentBaseType::ComponentAnsys123;
        return alloc;
    }
}