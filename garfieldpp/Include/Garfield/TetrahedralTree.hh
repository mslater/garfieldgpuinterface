// Include this header if we're compiling with the GPU or this is the first time without
#if defined(__GPUCOMPILE__) || !defined(TETRAHEDRAL_TREE_H)

#if !defined(__GPUCOMPILE__) && !defined(TETRAHEDRAL_TREE_H)
#define TETRAHEDRAL_TREE_H
#endif

// undefine everything first 
#ifdef __TETRAHEDRALTREECLASS__
#undef __TETRAHEDRALTREECLASS__
#undef __VEC3CLASS__
#undef __GPULABEL__
#endif

#ifdef __GPUCOMPILE__


#define __TETRAHEDRALTREECLASS__ TetrahedralTreeGPU
#define __VEC3CLASS__ Vec3GPU
#define __GPULABEL__ __device__

#else

#include <vector>
class TetrahedralTreeGPU;

#define __TETRAHEDRALTREECLASS__ TetrahedralTree
#define __VEC3CLASS__ Vec3
#define __GPULABEL__

#endif

#define BLOCK_CAPACITY 10  // k_v

namespace Garfield {

// TODO: replace this class with ROOT's TVector3 class

struct __VEC3CLASS__ {
  float x, y, z;

  __GPULABEL__ __VEC3CLASS__() {}
  __GPULABEL__ __VEC3CLASS__(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}

  __GPULABEL__ __VEC3CLASS__ operator+(const __VEC3CLASS__& r) const {
    return __VEC3CLASS__(x + r.x, y + r.y, z + r.z);
  }

  __GPULABEL__ __VEC3CLASS__ operator-(const __VEC3CLASS__& r) const {
    return __VEC3CLASS__(x - r.x, y - r.y, z - r.z);
  }

  __GPULABEL__ __VEC3CLASS__& operator+=(const __VEC3CLASS__& r) {
    x += r.x;
    y += r.y;
    z += r.z;
    return *this;
  }

  __GPULABEL__ __VEC3CLASS__& operator-=(const __VEC3CLASS__& r) {
    x -= r.x;
    y -= r.y;
    z -= r.z;
    return *this;
  }

  __GPULABEL__ __VEC3CLASS__ operator*(float r) const { return __VEC3CLASS__(x * r, y * r, z * r); }

  __GPULABEL__ __VEC3CLASS__ operator/(float r) const { return __VEC3CLASS__(x / r, y / r, z / r); }
};

/**

\brief Helper class for searches in field maps.

This class stores the mesh nodes and elements in an Octree data
structure to optimize the element search operations

Author: Ali Sheharyar

Organization: Texas A&M University at Qatar

*/
class __TETRAHEDRALTREECLASS__ {
 private:
  // Physical position/size. This implicitly defines the bounding
  // box of this node
  __VEC3CLASS__ m_origin;         // The physical center of this node
  __VEC3CLASS__ m_halfDimension;  // Half the width/height/depth of this node

  __VEC3CLASS__ min, max;  // storing min and max points for convenience

  // The tree has up to eight children and can additionally store
  // a list of mesh nodes and mesh elements (Tetrahedron)
  __TETRAHEDRALTREECLASS__* children[8];  // Pointers to child octants

  // Children follow a predictable pattern to make accesses simple.
  // Here, - means less than 'origin' in that dimension, + means greater than.
  // child:	0 1 2 3 4 5 6 7
  // x:      - - - - + + + +
  // y:      - - + + - - + +
  // z:      - + - + - + - +
  #ifdef __GPUCOMPILE__
  int *tetList{nullptr};
  int numTetList{0};
  #else
  struct OctreeBlockElem {
    Vec3 point;
    int nodeIndex;

    OctreeBlockElem(const Vec3& _point, const int _ni)
        : point(_point), nodeIndex(_ni) {}
  };

  std::vector<OctreeBlockElem> iBlockElems;
  std::vector<int> tetList;
  #endif

 public:
 #ifdef __GPUCOMPILE__
  // Constructor
  TetrahedralTreeGPU() = default;

  // Destructor
  ~TetrahedralTreeGPU() {};
 #else
  // Constructor
  TetrahedralTree(const Vec3& origin, const Vec3& halfDimension);

  // Destructor
  ~TetrahedralTree();
#endif

  #ifndef __GPUCOMPILE__
  // Insert a mesh node (a vertex/point) to the tree
  void InsertMeshNode(Vec3 point, const int nodeIndex);

  // Insert the mesh element (a tetrahedron) to the tree
  void InsertTetrahedron(const double elemBoundingBox[6], const int elemIndex);

  /// Create and initialise GPU Transfer class
  double CreateGPUTransferObject(TetrahedralTreeGPU *&tree_gpu);

 private:
  // Check if the given box overlaps with the box corresponding to this tree
  // node
  bool DoesBoxOverlap(const Vec3& b_min, const Vec3& b_max) const;

  // Check if the tree node is full
  bool IsFull() const;

  // Check if the tree node is empty
  bool IsEmpty() const;

  // Check if this tree node is a leaf or intermediate node
  bool IsLeafNode() const;
#endif

#ifdef __GPUCOMPILE__
public:
    __device__ void GetTetListInBlock(const Vec3GPU& point, const int *&tet_list_elems, int &num_elems);

private:
    __device__ const TetrahedralTreeGPU* GetBlockFromPoint(const Vec3GPU& point);
    __device__ const TetrahedralTreeGPU* GetBlockFromPointHelper(const Vec3GPU& point);
    __device__ int GetOctantContainingPoint(const Vec3GPU& point) const;

  friend class TetrahedralTree;
#else
public:
  // Get all tetrahedra linked to a block corresponding to the given point
  std::vector<int> GetTetListInBlock(const Vec3& point);
private:
  int GetOctantContainingPoint(const Vec3& point) const;

  // Get a block containing the input point
  const TetrahedralTree* GetBlockFromPoint(const Vec3& point);

  // A helper function used by the function above.
  // Called recursively on the child nodes.
  const TetrahedralTree* GetBlockFromPointHelper(const Vec3& point);

#endif

};

}

#endif
