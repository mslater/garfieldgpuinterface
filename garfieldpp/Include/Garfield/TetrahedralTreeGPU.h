#ifndef TETRAHEDRAL_TREE_GPU_H
#define TETRAHEDRAL_TREE_GPU_H

#ifndef __GPUCOMPILE__
#error GPU HEADER INCLUDED WITHOUT SETTING __GPUCOMPILE__
#endif

#include "TetrahedralTree.hh"

#endif