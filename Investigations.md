# Investigations and work done on GPU conversion for garfield/GEM work

## Single Particle comparisons

3d1588fc0f60431a7b15469795f0ab79af583ac7 / 2020-08-26 10:35:
e486adbd260ad857e7d61e13e738532a0c46a23e / 2020-08-27 11:21:

* 1:  0.0236611 /0.00189805 = 12.47
* 10: 0.068476 / 0.00453997 = 15.08
* 20: 0.108611 / 0.00614905 = 17.66
* 23: 0.127394 / 0.00726795 = 17.53

Beyond this, you get multiple electrons

Given 3564 Cores on a P100 (assuming compute bound) => 3564/17.53 = ~200 times speed up with full utilisation, x2 for both cards

## Putting more electrons on the stack

With only one iteration, try putting in increasing numbers for the GPU and check the time taken. It should be ~constant up to 3500 electrons

* 1: 0.022548
* 100: 0.27237

It is NOT!! However, forcing the 256 threads in a block to all process Electron id 0 did give (mostly) the same number (0.0241909->0.031745) - Note
that this would probably get affected by it reading and writing to the same stackOld area.

By forcing the RNG draw to come from a single entry, we get back to normal so it must be the transfer of this data

In fact, current setup with 130000 electrons and up to 16000 draws gives 15GB!!!

2020-08-27 10:41:

I sorted out the RNG setup and put made sure memory was allocated on the device. This didn't help. However, if I just change all the RNG arrays
to start at Seed(1), then the time is back to where it should be. I'm guessing this means that the randomisation of the movement caused it to
access/compute other things that are different from the starting electron.

The outermost while loop iterations can be limited by nCollSkip. Changing this to 1 gives:

* 1: 0.0118411
* 100: 0.015835

So still a factor 1.5 difference even after just performing one iteration for all electrons with the same starting position.

Checked the inner loop (to find the next collision) and electron id 0 had one of the largest counts on this loop (1453) so this doesn't seem
to be responsible for the difference in timings between the electrons in a block

2020-08-31  9:39:

Progress - possibly. After getting the Visual Profiler to do a full analysis by following these instructions:

https://developer.nvidia.com/nvidia-development-tools-solutions-ERR_NVGPUCTRPERM-permission-issue-performance-counters#SolnAdminTag

and running it with 'nvvp', it showed that the Warp Execution Efficieny was low due to high divergence in the code. Turns out,
each warp of 32 threads *shares* a program counter so if there's divergence in the code all 32 threads have to run in serial
until they converge again. This was happening *massively* in the code as there was a break in the tracking loop when a
collision was found to happen.


2020-09-02 15:40:

Been playing a lot today and possibly making slow progress. So, if you want to check Warp Execution Efficiency - make sure
there are 32 electrons in the queue but only 1 iteration in the main collision loop. This leads to:

* post random walk loop (with NO break): 99.3, 93.9, 0.00384593
* +GPUSensorElectricField: 85.6%, 81.2%, 0.00439715
* +GetElectronCollision: 85.3%, 80.9%, 0.012609
* Whole Loop: 85.7%, 81.4%, 0.0130169

To have a better idea of timings, I upped the iterations to 50 in the collision loop but 'continue'd at appropriate points:

* post random walk loop (with NO break): 0.02878
* +GPUSensorElectricField: 0.0298469 
* +GetElectronCollision: 0.0468528
* Whole Loop: 0.056972

Just for fun (commented out the break for 1 electron getting 'Attached'):

* 50 iterations, 32 electrons: 68.6%, 64.8%, 0.054975
* 100 iterations, 32 electrons: 71.6%, 67.6%, 0.0904741
* 100 iterations, 100 electrons: 60.1%, 56.8%, 0.114684

(actually uncommenting the Attached electron didn't make a big difference. Probably gets aborted elsewhere).

2020-09-03 13:42:

With the change to the step loop (putting in loop_count variable which greatly improved warp efficiency), started checking final numbers again.

Turn over occured at max_count = 55:
GPU: 0.703874
CPU: 0.692321

--------------------------------------------
Normal Old Stack Size:    1474
RUNNING LOOP...
Normal Old Stack Size:    1474
Normal New Stack Size:    352
--------------------------------------------

Got up to max_count = 77 before there were problems:

Starting GPU Loop...
RNG Setup:   100663x16000x8 = 11GB
-----------------------------------------
GPU Old Stack Size:    82234
RUNNING LOOP....
ERROR: loop_count max hit
ERROR: loop_count max hit
GPU Old Stack Size:    82234
GPU New Stack Size:    11874
-----------------------------------------
ALL END POINTS:   19448
Stopping GPU Loop...
Timer = 3.95433 3.7
ComponentAnsys123::FindElement13:
    Caching the bounding boxes of all elements... done.
    BAND STRUCTURE:   0
    --------------------------------------------
    Normal Old Stack Size:    82234
    RUNNING LOOP...
    Normal Old Stack Size:    82234
    Normal New Stack Size:    11874
    --------------------------------------------
    here, np= 19447
    Timer = 55.7891 55.65
    end
    bash-4.2$


However, this was due to the loop_count max being hit. This change also doesn't seem to be helping either :( You can get up to max_count=82 before there's a difference:

Starting GPU Loop...
RNG Setup:   117440x16000x8 = 13GB
ERROR: loop_count max hit 319
ERROR: loop_count max hit 278
ERROR: loop_count max hit 544
-----------------------------------------
GPU Old Stack Size:    110323
RUNNING LOOP....
GPU Old Stack Size:    110323
GPU New Stack Size:    10984
-----------------------------------------
ALL END POINTS:   42071
Stopping GPU Loop...
Timer = 5.85535 5.47
ComponentAnsys123::FindElement13:
    Caching the bounding boxes of all elements... done.
    BAND STRUCTURE:   0
    --------------------------------------------
    Normal Old Stack Size:    110323
    RUNNING LOOP...
    Normal Old Stack Size:    110323
    Normal New Stack Size:    10985
    --------------------------------------------
    here, np= 42071
    Timer = 96.1637 95.9
    end



Also, looking at the last iteration of the loop with nvvp showed 50% of the time for that iteration (332ms/595ms) was spent on data transfer.

2020-09-08 10:04:

After doing a lot of work shifting the stacks to be 100% on the GPU, there was speed up already shown as the turn over is now slightly earlier:

Turn over occured at max_count = 54:
GPU: 0.595249
CPU: 0.582554

For max_count = 55 (the previous turnover):

GPU: 0.611416
CPU: 0.694652

Which is a 15% improvement already :)



2020-09-11 10:10:

At max_count = 87, we get the peak of concurrent electrons (~119000)


1 electron ~10K gain (high end)
few hundred electrons (227)
200K electrons (!!!!)




2020-11-23:

So we now have a 'working' version of the decent code port. However, there are questions:

* If I increase the thread count above (64+) it becomes non-determinant, i.e. run it multiple times and get different results.
* It's *slow*

Plan now is to get this running what I had before/above with the THGEM and use that to check/fix the above points


2020-11-30:

Fixed both of the above issues. non-determinant was due to bad handling of m_lastElement. Switched this to per-thread and it worked.
Slowness seems to principally come from the conditional in the RNG Draw function. Commenting this out helped at lot at low levels
(maybe not so much at high levels). Also, switched away from thrust.


2021-02-08:

Interesting article about differences expected between CPU and GPU calcs. Need to invstigate more.

http://www.cudahandbook.com/2013/08/floating-point-cpu-and-gpu-differences/


2021-02-08:

Looking at rounding to significant figures (ignoring GPU):

 make && ./simulation  --singleStandardTHGEM -b --gas-pressure 200 -n 1 --max-iterations 5 --num-sig-figs 5
 0 CPU orig A 0.0152600000 0.0112200000 0.0700000000 0.1000000000
 Current Loop number:    1
 0 CPU orig A 0.0123250000 0.0090014000 0.0664640000 0.8677400000
 Current Loop number:    2
 0 CPU orig A 0.0094503000 0.0123270000 0.0617290000 2.2611000000
 Current Loop number:    3
 0 CPU orig A 0.0072158000 0.0141970000 0.0579180000 6.1272000000
 Current Loop number:    4
 0 CPU orig A 0.0085663000 0.0135320000 0.0552620000 7.9389000000

 make && ./simulation  --singleStandardTHGEM -b --gas-pressure 200 -n 1 --max-iterations 5
0 CPU orig A 0.0152598216 0.0112200049 0.0700000000 0.1000000000
Current Loop number:    1
0 CPU orig A 0.0123249191 0.0090013891 0.0664644926 0.8677902545
Current Loop number:    2
0 CPU orig A 0.0099795571 0.0089740616 0.0613299181 2.5738003839
Current Loop number:    3
0 CPU orig A 0.0079841323 0.0087451203 0.0590481204 6.8255459895
Current Loop number:    4
0 CPU orig A 0.0079475019 0.0088268419 0.0576122078 7.1523591659