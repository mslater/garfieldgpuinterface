# Notes on Garfield GPU studies

* Current situation:
    * `test-event 1` gives ~35x improvement (full GEM shower sim, single e- start)
    * `test-event 2 -n 100000` gives ~50x improvement (100000 e-'s created at start of GEM and processed for one iteration)

* General idea:
    - Process the particle stack (shift new particles from `newStack` -> `oldStack` and remove old ones)
    - Transport each particle in the stack
        1. Check particle pos (Determine **Electric Field**)
        2. Add random time/mean path
        3. Continue until collision is determined
        4. Determine **Electric Field** at new position
        5. Process collision (Elastic, Ineleastic, shower, etc.)
        6. Repeat up to m_nCollSkip OR energy is too small, out of time window, left medium, crossed a wire, Attachment collision
* Slow points: Loop over poistion change, determining the Electric Field, number of collisions loop

* GPU Changes for Stack processing:
    - Use thrust to remove particles with status -1
    - Use thrust to sort the arrays storing new particle info to get the ones that have been created
    - memcpy the now contiguous blocks to the current stack process

* GPU changes for Transport:
    - stop changes happening to geometry, etc. (i.e. essentially make model setup thread-safe)
    - Store newly created particles in thread-specific arrays

* Additionals:
    - Copy the geometry/model info into CUDA managed arrays
    - Created a new RNG class to pre-calculate RNG values based on particle ID
    - Added option to limit to certain number of iterations

* Multithreading (MT) investigations:
    - Investigated using GPU code (i.e. thread-safe version) for normal CPU threading
    - Found that a lot worse performance was found when just doing 1 particle => 1 thread. A thread needs to handle multiple particles.
    - Same results found but speed was equal (at best) to single threaded version
    - After *a lot* of work, this was found to be due to optimisations in compilation:
        * Garfield is compiled with `-O3`
        * This basically breaks the multi-threaded benefits (or seems to)
        * Turning off optimisations showed expected speed up but slowed everything else down
    - More investigation needed to determine wether selectively turning off some optimisations will be lead to overall benefits with MT
    - Alternatively, selective use of OpenMP might be the way forward here

* Some more technical details:
    - `*.hh` is in the `makefile` but this will add it to root dictionaries. Shifted to `*.h` for CUDA header files
    - Currently ditching `GPUFLOAT` as it will make the code integration easier. Eventually shift to `GFDDECTYPE` or similar if we want to try to switch to `float`s over `double`s for possible speed gains again.
    - more cleanup possible by altering base code, e.g. range based loops -> normal for loops
    - GPU compilation done via includes and macros, see `Sensor.cu` and `Sensor.h`. This keeps the code in the same place.

* TODO and possible improvements:
    - Handle situations where changes to geometry needs to be done
    - Do dedicated studies with Toy sims of the particle movement and Electric Field sections to look for possible improvements particular to GPU architecture. Use proper profiling tools here.
    - Force GPU to process each particle in stack through the *whole* medium rather than stop after a certain number of collisions
    - Allow multiple showers to be processesed simulataneously
    - hand off to GPU and back at appropriate points (requires 'easy' shift of data to and from. Also needed for above)
    - Combined with above, use multiple GPUs where appropriate
    - continue MT investigations to see if any benefit can be had without breaking/turning off optimisations.

* Questions answered from original study:
    - Stack processing using thrust and index arrays *much faster*. Essentially now insignfiicant.
    - *Possible* reason for differences between GPU and CPU code is optimisations - these *can* lead to very small numerical difference (see Multi-threading work)
    - Differences between increasing and decreasing electrons due to geomoetry - Basically, when the electrons are decreasing they are just drifting after the GEM. This maximises the number of 'collision loops' and thus the benefit of the GPU
    - Automatic code generation idea has been ditched as I have managed to get similar working with macros, etc. Code for GPUs is in the same place and (where possible) is exactly the same as CPU version
    - Switching to `float` from `double` has some improvement but not a great deal. Needs more investigation really.


* Attempted to use virtual methods. Performance was worse:

Without:

----------------------------------------
BENCHMARKS AND STATS

          Iter       OldSize       NewSize         stack     transport     stack gpu transport gpu   stack ratiotransport ratio

   Sub Totals:                                  0.970947       871.712      0.255905       21.4185       3.79418        40.699

       Totals:                                   872.683                     21.6744                     40.2633

With:

----------------------------------------
BENCHMARKS AND STATS

          Iter       OldSize       NewSize         stack     transport     stack gpu transport gpu   stack ratiotransport ratio

   Sub Totals:                                  0.980421       865.042      0.262692       31.1544       3.73221       27.7663

       Totals:                                   866.022                     31.4171                     27.5653


* Things that can be updated during processing:
    - fLim: Note, this could be broken in the CPU version thanks to splitting the processing and transport
    - MediumMagboltz: GetElectronCollisionRate, etc. Can you just turn off AutoAdjust? If not, can we go through an iteration and *then* update the tables?
        - if we have to, can have a second buffer of stackOld that is copied every loop. If a re-calculation is needed, this buffer is
        copied over the new one and the iteration is re-run.
