int analysisPlots()
{
  TF1 *polya = new TF1("polya","[2]*(((x*(1+[0])/[1])**[0])*TMath::Exp((-1*x)*(1+[0])/[1]))",
                   2, 1e6);
  TFile*f=new TFile("output.root","READ");
  TCanvas*c1=new TCanvas("c","",600,600);
  TH1F*gain_0=(TH1F*)f->Get("gain_2");
  gain_0->GetXaxis()->SetTitle("Gain");
  gain_0->GetYaxis()->SetTitleOffset(1.5);
  gain_0->GetYaxis()->SetTitle("Events");
  polya->SetLineColor(kRed);
  gain_0->Rebin(10);
  polya->SetParameters(0.25, gain_0->GetMean(), 1000);
  int firstNonZero=-1;
  for(int i=gain_0->GetNbinsX();i>-1;i--)
    {
      if(gain_0->GetBinContent(i)>0)
	{
	  firstNonZero=i;
	  break;
	}
    }
  gain_0->GetXaxis()->SetRangeUser(0,gain_0->GetXaxis()->GetBinCenter(firstNonZero));
  //gain_0->GetXaxis()->SetRangeUser(0,1e3);
  gain_0->Fit(polya, "", "",11,1e6);//, 0, max);
  //f->Draw("same");
  gain_0->Draw();

  TString name("SingleTHGEM_150Torr_zstart100mum");
  c1->Print("Gain_"+name+".png");

  gain_0->GetYaxis()->SetTitleOffset(1.2);
  c1->SetLogy();
  gain_0->Draw();
  c1->Print("Gain_"+name+"_log.png");

  TCanvas*cc2=new TCanvas("cc2","",600,600);
  cc2->SetRightMargin(0.15);
  TH2F*electron_distribution=(TH2F*)f->Get("electron_distribution");
  cc2->SetTickx();
  cc2->SetTicky();
  electron_distribution->GetXaxis()->SetNdivisions(205);
  electron_distribution->GetYaxis()->SetNdivisions(205);
  electron_distribution->GetXaxis()->SetTitle("#Delta x [cm]");
  electron_distribution->GetYaxis()->SetTitleOffset(1.5);
  electron_distribution->GetYaxis()->SetTitle("#Delta y [cm]");
  electron_distribution->Draw("colz");
  cc2->Print("ElectronDistribution_"+name+"_log.png");
  electron_distribution->Draw("cont");
  cc2->Print("ElectronDistribution_"+name+"_surf.png");


  TCanvas*c2=new TCanvas("c2","",600,600);
  c2->SetRightMargin(0.15);
  TH2F*hole_efficiency_den=(TH2F*)f->Get("hole_efficiency_den");
  TH2F*hole_efficiency_num=(TH2F*)f->Get("hole_efficiency_num");
  hole_efficiency_num->GetXaxis()->SetTitle("x [cm]");
  hole_efficiency_num->GetYaxis()->SetTitle("y [cm]");
  hole_efficiency_num->GetXaxis()->SetNdivisions(205);
  hole_efficiency_num->GetYaxis()->SetNdivisions(205);
  /* hole_efficiency_den->RebinX(8); */
  /* hole_efficiency_num->RebinX(8); */
  /* hole_efficiency_den->RebinY(8); */
  /* hole_efficiency_num->RebinY(8); */
  hole_efficiency_num->Divide(hole_efficiency_den);
  hole_efficiency_num->Draw("colz");
  c2->Print("Efficiency_"+name+"_log.png");
    
  return 0;
}
