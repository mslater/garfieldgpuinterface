#include <iostream>
#include <iomanip>
#include <fstream>

#include <cxxopts.hpp>

#include <TApplication.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TFile.h>
#include <TTree.h>
#include <TStopwatch.h>

#include "Garfield/ComponentAnsys123.hh"
#include "Garfield/ViewField.hh"
#include "Garfield/ViewFEMesh.hh"
#include "Garfield/MediumMagboltz.hh"
#include "Garfield/Sensor.hh"
#include "Garfield/DriftLineRKF.hh"
#include "Garfield/AvalancheMicroscopic.hh"
#include "Garfield/AvalancheMC.hh"
#include "Garfield/Random.hh"
#include "Garfield/Plotting.hh"

#include "GeometryDetector.h"

#include "TRandom3.h"
using namespace Garfield;
std::vector<std::string> metadata_FieldMaps;
std::vector<double> metadata_FieldMaps_dx;
std::vector<double> metadata_FieldMaps_dy;
std::vector<double> metadata_FieldMaps_dz;
std::vector<double> metadata_FieldMaps_dV;
std::string metadata_DetectorType; //GEM or THGEM
int metadata_DetectorN; //1,2,3?
std::string metadata_GasFile;
double metadata_GasPressure;
double metadata_GasTemperature;

std::vector<double> Nt_el_init_x;
std::vector<double> Nt_el_init_y;
std::vector<double> Nt_el_init_z;
std::vector<double> Nt_el_fin_x;
std::vector<double> Nt_el_fin_y;
std::vector<double> Nt_el_fin_z;
std::vector<double> Nt_el_fin_status;

double Nt_time_Real_Event;
double Nt_time_CPU_Event;
double Nt_time_Real_EventPerInitElectron;
double Nt_time_CPU_EventPerInitElectron;
void clearVariables()
{
  Nt_el_init_x.clear();
  Nt_el_init_y.clear();
  Nt_el_init_z.clear();
  Nt_el_fin_x.clear();
  Nt_el_fin_y.clear();
  Nt_el_fin_z.clear();
  Nt_el_fin_status.clear();

  Nt_time_Real_Event=0.;
  Nt_time_CPU_Event=0.;	
  Nt_time_Real_EventPerInitElectron=0.;
  Nt_time_CPU_EventPerInitElectron=0.;
}

//int main(int argc, char * argv[])
int main(int argc, char **argv)
{

  TRandom3*rndm=new TRandom3(0);
  TStopwatch timer;

  cxxopts::Options options("simulation", "Simulation for as-yet-untitled Migdal experiment");
  // bools have an implicit value of true anyway but we'll include them here for clarity
  options.add_options()
    ("gpudevice", "Specify which GPU device to use (use nvidia-smi for a list)", cxxopts::value<int>()->default_value("0"))
    ("b,batch", "Run in batch mode", cxxopts::value<bool>()->implicit_value("true")->default_value("false"))
    ("n", "Number of events", cxxopts::value<int>()->default_value("1"))
    ("max-iterations", "Maximum number of iterations to run", cxxopts::value<int>()->default_value("-1"))
    ("debug-iteration", "Debug a particular iteration of an event", cxxopts::value<int>()->default_value("-1"))
    ("debug-electron", "Debug a particular electron for an event", cxxopts::value<int>()->default_value("-1"))
    ("test-event", "configure for a particular fixed test event", cxxopts::value<int>()->default_value("0"))
    ("drift-ions", "Whether to drift ions or not", cxxopts::value<bool>()->implicit_value("true")->default_value("false"))
    ("plane-offset", "Offset of plane", cxxopts::value<float>()->default_value("0"))
    ("debug", "Debug", cxxopts::value<bool>()->default_value("false"))
    ("gas-temperature", "Gas Temperatures", cxxopts::value<double>()->default_value("293.15"))
    ("gas-pressure", "Gas Pressure", cxxopts::value<double>()->default_value("100"))
    ("gas-file", "Gas File", cxxopts::value<std::string>()->default_value("gasFile_100_50_Ar_80_CF4.gas"))
    ("singleStandardGlassGEM", "Single StandardGlassGEM geometry", cxxopts::value<bool>()->implicit_value("true")->default_value("false"))
    ("doubleStandardGlassGEM", "Double StandardGlassGEM geometry", cxxopts::value<bool>()->implicit_value("true")->default_value("false"))
    ("singleStandardGEM", "Single StandardGEM geometry", cxxopts::value<bool>()->implicit_value("true")->default_value("false"))
    ("tripleStandardGEM", "Triple StandardGEM geometry", cxxopts::value<bool>()->implicit_value("true")->default_value("false"))
    ("singleStandardTHGEM", "Single StandardTHGEM geometry", cxxopts::value<bool>()->implicit_value("true")->default_value("false"))
    ("doubleStandardTHGEM", "Double StandardTHGEM geometry", cxxopts::value<bool>()->implicit_value("true")->default_value("false"))
    ("vDrift", "Drift Voltage", cxxopts::value<int>()->default_value("300"))
    ("vGEM", "GEM Voltage", cxxopts::value<int>()->default_value("600"))
    ("vInd", "Inuction Voltage", cxxopts::value<int>()->default_value("400"))
    ("vGEM2", "GEM Voltage 2", cxxopts::value<int>()->default_value("600"))
    ("vInd2", "Inuction Voltage 2", cxxopts::value<int>()->default_value("400"))
    ("light", "Minimum info in output", cxxopts::value<bool>()->implicit_value("true")->default_value("false"))
    ("h,help", "Print usage")
  ;
  auto cmdline_args = options.parse(argc, argv);

  if (cmdline_args.count("help")) {
     std::cout << options.help() << std::endl;
     return 1;
  }

  // ./simulation nEvents bBatch bDriftIons planeOffse
  int nDevice = cmdline_args["gpudevice"].as<int>();
  int nEvents = cmdline_args["n"].as<int>();
  bool bBatch = cmdline_args["batch"].as<bool>();
  bool bLight = cmdline_args["light"].as<bool>();
  bool bDriftIons = cmdline_args["drift-ions"].as<bool>();
  float planeOffset  = cmdline_args["plane-offset"].as<float>();
  int nMaxIter = cmdline_args["max-iterations"].as<int>();
  int nDebugIteration = cmdline_args["debug-iteration"].as<int>();
  int nDebugID = cmdline_args["debug-electron"].as<int>();
  int nTestEvent = cmdline_args["test-event"].as<int>();

  // configurations for particular test event
  int nParticles{1};
  if (nTestEvent == 2)
  {
    nParticles = nEvents;
    nEvents = 1;
    nMaxIter = 1;
  }

  if (nTestEvent > 0)
  {
    nEvents = 1;
    bBatch = true;
  }

  std::string m_gasFile  =cmdline_args["gas-file"].as<std::string>();
  double m_gasPressure   =cmdline_args["gas-pressure"].as<double>();
  double m_gasTemperature=cmdline_args["gas-temperature"].as<double>();

  std::cout << "Input Configuration"<<std::endl;
  std::cout << "nEvents = "<< nEvents <<std::endl;
  std::cout << "bBatch  = "<< bBatch <<std::endl;
  std::cout << "bDriftIons = "<< bDriftIons <<std::endl;
  std::cout << "planeOffset = "<< planeOffset<<std::endl;
  std::cout << "gasFile ="<< m_gasFile<<std::endl;
  std::cout << "gasPressure ="<<m_gasPressure<<std::endl;
  std::cout << "gasTemperature = "<<m_gasTemperature<<std::endl;
  
  const bool debug = cmdline_args["debug"].as<bool>();
  bool plotField = (bBatch?false:true);
  bool plotDrift = (bBatch?false:true);


  TFile*fileOut=new TFile("results.root","RECREATE");
  fileOut->cd();
  TTree*treeMetadata=new TTree("metadata","metadata");
  treeMetadata->Branch("FieldMaps",&metadata_FieldMaps);
  treeMetadata->Branch("FieldMaps_dx",&metadata_FieldMaps_dx);
  treeMetadata->Branch("FieldMaps_dy",&metadata_FieldMaps_dy);
  treeMetadata->Branch("FieldMaps_dz",&metadata_FieldMaps_dz);
  treeMetadata->Branch("FieldMaps_dV",&metadata_FieldMaps_dV);
  treeMetadata->Branch("DetectorType", &metadata_DetectorType);
  treeMetadata->Branch("DetectorN", &metadata_DetectorN);
  treeMetadata->Branch("GasFile", &metadata_GasFile);
  treeMetadata->Branch("GasPressure", &metadata_GasPressure);
  treeMetadata->Branch("GasTemperature", &metadata_GasTemperature);
  
  TTree*tree=new TTree("output","output");
  tree->Branch("el_init_x", &Nt_el_init_x);
  tree->Branch("el_init_y", &Nt_el_init_y);
  tree->Branch("el_init_z", &Nt_el_init_z);
  tree->Branch("el_fin_x", &Nt_el_fin_x);
  tree->Branch("el_fin_y", &Nt_el_fin_y);
  tree->Branch("el_fin_z", &Nt_el_fin_z);
  tree->Branch("el_fin_status", &Nt_el_fin_status);
  tree->Branch("time_Real_Event", &Nt_time_Real_Event);
  tree->Branch("time_CPU_Event", &Nt_time_CPU_Event);
  tree->Branch("time_Real_EventPerInitElectron", &Nt_time_Real_EventPerInitElectron);
  tree->Branch("time_CPU_EventPerInitElectron", &Nt_time_CPU_EventPerInitElectron);
  TApplication app("app", &argc, argv);
  plottingEngine.SetDefaultStyle();

  GeometryDetector MyGeometry;
  //MyGeometry.InitialiseStandardTripleGEM(0);
  //MyGeometry.SetZdimInCM(0.2);
  bool b_singleStandardGlassGEM = cmdline_args["singleStandardGlassGEM"].as<bool>();
  bool b_doubleStandardGlassGEM = cmdline_args["doubleStandardGlassGEM"].as<bool>();
  bool b_singleStandardGEM = cmdline_args["singleStandardGEM"].as<bool>();
  bool b_tripleStandardGEM = cmdline_args["tripleStandardGEM"].as<bool>();
  bool b_singleStandardTHGEM = cmdline_args["singleStandardTHGEM"].as<bool>();
  bool b_doubleStandardTHGEM = cmdline_args["doubleStandardTHGEM"].as<bool>();
  int b_vDrift = cmdline_args["vDrift"].as<int>();
  int b_vGEM   = cmdline_args["vGEM"].as<int>();
  int b_vInd   = cmdline_args["vInd"].as<int>();
  int b_vGEM2  = cmdline_args["vGEM2"].as<int>();
  int b_vInd2  = cmdline_args["vInd2"].as<int>();

  if ((nTestEvent == 1) || (nTestEvent == 2))
  {
    b_singleStandardTHGEM = true;
    b_singleStandardGlassGEM = b_doubleStandardGlassGEM = b_singleStandardGEM = b_tripleStandardGEM = b_doubleStandardTHGEM = false;
  }

  if(b_singleStandardGlassGEM)
    MyGeometry.InitialiseGlassGEM(b_vDrift,b_vGEM,b_vInd);
  else if(b_doubleStandardGlassGEM)
    MyGeometry.InitialiseDoubleGlassGEM(planeOffset,b_vDrift,b_vGEM,b_vInd,b_vGEM2,b_vInd2);
  else if(b_doubleStandardTHGEM)
    MyGeometry.InitialiseStandardDoubleTHGEM(0);
  else if(b_singleStandardTHGEM)
    MyGeometry.InitialiseStandardTHGEM();
  else if(b_tripleStandardGEM)
    MyGeometry.InitialiseStandardTripleGEM(0);
  else if(b_singleStandardGEM)
    MyGeometry.InitialiseStandardGEM();
  else
    {
      std::cerr<< "did not decide geometry"<<std::endl;
      return 0;
    }


  // Load the field maps.
  std::vector<ComponentAnsys123*> allFM;
  for(unsigned int ifieldMap=0; ifieldMap<MyGeometry.GetFieldMaps()->size(); ifieldMap++)
    {
      ComponentAnsys123* fm = new ComponentAnsys123();
      std::string pathGEM(MyGeometry.GetDetectorType()+"/");
      std::string fieldGEM(MyGeometry.GetFieldMaps()->at(ifieldMap));
      fm->SetOffSetNodes(MyGeometry.GetFieldMaps_dx()->at(ifieldMap),
			 MyGeometry.GetFieldMaps_dy()->at(ifieldMap),
			 MyGeometry.GetFieldMaps_dz()->at(ifieldMap),
			 MyGeometry.GetFieldMaps_dV()->at(ifieldMap));

      std::cout << pathGEM<<std::endl;
      fm->Initialise(pathGEM+"ELIST"+fieldGEM+".lis",
		     pathGEM+"NLIST"+fieldGEM+".lis",
		     pathGEM+"MPLIST"+fieldGEM+".lis",
		     pathGEM+"PRNSOL"+fieldGEM+".lis", "mm");
      fm->EnableMirrorPeriodicityX();
      fm->EnableMirrorPeriodicityY();
      fm->PrintRange();
      allFM.push_back(fm);

      metadata_FieldMaps.push_back(MyGeometry.GetFieldMaps()->at(ifieldMap).Data());
      metadata_FieldMaps_dx.push_back(MyGeometry.GetFieldMaps_dx()->at(ifieldMap));
      metadata_FieldMaps_dy.push_back(MyGeometry.GetFieldMaps_dy()->at(ifieldMap));
      metadata_FieldMaps_dz.push_back(MyGeometry.GetFieldMaps_dz()->at(ifieldMap));
      metadata_FieldMaps_dV.push_back(MyGeometry.GetFieldMaps_dV()->at(ifieldMap));
    }

  metadata_DetectorType=MyGeometry.GetDetectorType();
  metadata_DetectorN=(int)(MyGeometry.GetFieldMaps()->size());
  metadata_GasFile=m_gasFile;
  metadata_GasPressure=m_gasPressure;
  metadata_GasTemperature=m_gasTemperature;

  if (plotField)
    {
      ViewField* fieldView = new ViewField();
      for(unsigned int iFM=0;iFM<allFM.size();iFM++)
	fieldView->SetComponent(allFM[iFM]);

      // (0,-1,0) the normal direction to the viewing plane
      // (0,0,0) the view position.
      fieldView->SetPlane(0., -1., 0., 0., 0., 0.);
      fieldView->SetArea(-MyGeometry.GetPitchInCM() / 2., MyGeometry.GetDimZ0InCM()-MyGeometry.GetZdimInCM(),
			  MyGeometry.GetPitchInCM() / 2., MyGeometry.GetDimZ0InCM());
      fieldView->SetVoltageRange(0., MyGeometry.GetMaxVolt());
      TCanvas* cf = new TCanvas("cf", "", 600, 600);
      //cf->SetLeftMargin(0.16);
      fieldView->SetCanvas(cf);
      fieldView->PlotContour("v");
      cf->Print("GEMvfield.pdf");
      cf->Print("GEMvfield.png");
      cf->Print("GEMvfield.eps");
      TCanvas* cff = new TCanvas("cff", "", 600, 600);
      fieldView->PlotContour("e");
      cff->Print("GEMefield.pdf");
      cff->Print("GEMefield.png");
      cff->Print("GEMefield.eps");
      //fieldView->Plot("e","ARR");
      //      cf->Print("GEMefield2.pdf");
      //cf->Print("GEMefield2.png");
      //cf->Print("GEMefield2.eps");
    }

  // Setup the gas.
  MediumMagboltz* gas = new MediumMagboltz();
  const std::string pathGas = "GasFiles/";
  if(!gas->LoadGasFile(pathGas+m_gasFile))
    return -1;
  //gas->SetComposition("ar", 80., "co2", 20.);
  gas->SetTemperature(m_gasTemperature);
  gas->SetPressure(m_gasPressure);
  gas->EnableDebugging();
  gas->Initialise();
  gas->DisableDebugging();
  std::cout << " Temperature = "<< gas->GetTemperature() << ", Pressure = "<<gas->GetPressure()<<std::endl;
  //gas->PrintGas();

  // Set the Penning transfer efficiency.
  const double rPenning = 0.51;
  const double lambdaPenning = 0.;
  //gas->EnablePenningTransfer(rPenning, lambdaPenning, "ar");
  // Load the ion mobilities.
  //const std::string path = getenv("GARFIELD_HOME");
  //gas->LoadIonMobility(path + "/Data/IonMobility_Ar+_Ar.txt");
  // Associate the gas with the corresponding field map material.
  for(unsigned int iFM=0;iFM<allFM.size();iFM++)
    {
      ComponentAnsys123* fm=allFM[iFM];
      const unsigned int nMaterials = fm->GetNumberOfMaterials();
      for (unsigned int i = 0; i < nMaterials; ++i)
	{
	  const double eps = fm->GetPermittivity(i);
	  if (eps == 1.) fm->SetMedium(i, gas);
	}
      fm->PrintMaterials();
    }

  // Create the sensor.
  Sensor* sensor = new Sensor();
  for(unsigned int iFM=0;iFM<allFM.size();iFM++)
    sensor->AddComponent(allFM[iFM]);
  double FinalZ =  MyGeometry.GetDimZ0InCM()-MyGeometry.GetZdimInCM()+0.09;

  if ((nTestEvent == 1) || (nTestEvent == 2))
  {
    FinalZ = MyGeometry.GetDimZ0InCM()-MyGeometry.GetZdimInCM();
  }

  //I subtract 0.09cm so that we look at the electrons 1mm below the bottom GEM
  sensor->SetArea(-5*MyGeometry.GetPitchInCM(),-5*MyGeometry.GetPitchInCM(), FinalZ, 
		   5*MyGeometry.GetPitchInCM(), 5*MyGeometry.GetPitchInCM(), MyGeometry.GetDimZ0InCM());

  AvalancheMicroscopic* avalMicroscopic = new AvalancheMicroscopic();
  //AvalancheMC* avalMicroscopic  = new AvalancheMC();
  //avalMicroscopic->DisableDiffusion();
  avalMicroscopic->SetSensor(sensor);

  AvalancheMC* drift = new AvalancheMC();
  drift->SetSensor(sensor);
  drift->SetDistanceSteps(2.e-4);

  ViewDrift* driftView = new ViewDrift();
  ViewFEMesh* meshView = new ViewFEMesh();
  if (plotDrift)
    {
      for(unsigned int iFM=0;iFM<allFM.size();iFM++)
	meshView->SetComponent(allFM[iFM]);
      // meshView->SetArea(-2 *   MyGeometry.GetPitchInCM(), -2 *   MyGeometry.GetPitchInCM(), -0.5,
      // 			2 *   MyGeometry.GetPitchInCM(),  2 *   MyGeometry.GetPitchInCM(),  0.1);
      //meshView->SetArea(-2 *   MyGeometry.GetPitchInCM(), -2 *   MyGeometry.GetPitchInCM(), -0.02,
      //2 *   MyGeometry.GetPitchInCM(),  2 *   MyGeometry.GetPitchInCM(),  0.02);
      meshView->SetArea(-2*MyGeometry.GetPitchInCM(), -2*MyGeometry.GetPitchInCM(), MyGeometry.GetDimZ0InCM()-MyGeometry.GetZdimInCM(),
			 2*MyGeometry.GetPitchInCM(),  2*MyGeometry.GetPitchInCM(), MyGeometry.GetDimZ0InCM());

      //meshView->SetPlane(0, -1, 0, 0, 0, 0.0);
      meshView->SetPlane(0, 0, -1, 0, 0, 0.0);
      meshView->SetFillMesh(true);
      // Set the color of the kapton.
      meshView->SetColor(2, kYellow - 6);
      meshView->EnableAxes();
      meshView->SetXaxisTitle("x [cm]");
      //meshView->SetYaxisTitle("z [cm]");
      meshView->SetYaxisTitle("y [cm]");
      avalMicroscopic->EnablePlotting(driftView);
      drift->EnablePlotting(driftView);
    }
  std::vector<unsigned int> nVUV;
  TH1F*h=new TH1F("h","",20,0,20);
  for (int i = 0; i<nEvents; i++)
    { 
      timer.Start();
      clearVariables();
      ///if (debug || i % 10 == 0)
      if(debug || i%1==0)
	std::cout << i << "/" << nEvents << "\n";

      // Randomize the initial position. 
      //double x0 = -  MyGeometry.GetPitchInCM() / 2. + RndmUniform() *   MyGeometry.GetPitchInCM();
      //double y0 = -  MyGeometry.GetPitchInCM() / 2. + RndmUniform() *   MyGeometry.GetPitchInCM();
      //=randomEngine.Seed(i+1);
      int seed= rndm->Uniform(0,10000);
      //if ((nTestEvent == 1) || (nTestEvent == 2))
      {
        seed = i+1;
      }
      randomEngine.Seed(seed);
      std::cout << "SEED:  " << seed << std::endl;

      std::vector<double> x0_vec;
      std::vector<double> y0_vec;
      std::vector<double> z0_vec;
      std::vector<double> t0_vec;
      std::vector<double> e0_vec;
      std::vector<double> dx0_vec;
      std::vector<double> dy0_vec;
      std::vector<double> dz0_vec;

      for (int k = 0; k < nParticles; k++)
      {
        double x0 = 0 + RndmUniform() *   MyGeometry.GetPitchInCM()/2.;
        double y0 = 0 + RndmUniform() *   MyGeometry.GetPitchInCM()*sqrt(3.)/2.;
        double z0 = 0.;
        if(b_singleStandardGlassGEM || b_singleStandardTHGEM)
    z0 =  (MyGeometry.GetZdimInCM()-0.2)/2.+0.02;
        else if(b_doubleStandardGlassGEM || b_doubleStandardTHGEM)
    z0 =  (MyGeometry.GetZdimInCM()/2.-0.2)/2.+0.02;
        else if(b_tripleStandardGEM)
    z0 =  (MyGeometry.GetZdimInCM()/3.-0.2)/2.+0.02;
        double t0 = 0.;
        double e0 = 0.1;

        x0_vec.push_back(x0);
        y0_vec.push_back(y0);
        z0_vec.push_back(z0);
        t0_vec.push_back(t0);
        e0_vec.push_back(e0);
        dx0_vec.push_back(0);
        dy0_vec.push_back(0);
        dz0_vec.push_back(0);
      }
//std::cout<< x0 << "  " << y0 << "  " <<z0 << "  " << std::endl;
int ne = 0, ni = 0;
#ifdef USEGPU

      // set the GPU config options
      //avalMicroscopic->SetRunModeOptions(MPRunMode::CPUGPUComparison, nDevice);
      //avalMicroscopic->SetShowProgress(true);

      avalMicroscopic->SetRunModeOptions(MPRunMode::GPUExclusive, nDevice);
      //avalMicroscopic->SetRunModeOptions(MPRunMode::Normal, nDevice);
      avalMicroscopic->SetMaxNumShowerLoops(nMaxIter);
      if ((nDebugIteration != -1) && (nDebugID == -1))
      {
        nDebugID = 0;
      }

      //avalMicroscopic->SetDebugShowerIterationAndElectronID(nDebugIteration, nDebugID);

      if (nTestEvent == 1)
      {
        gas->SetMaxElectronEnergy(57.4604);
      }
      else
      {
        gas->SetMaxElectronEnergy(105.422);
      }

      //avalMicroscopic->EnableExcitationMarkers(false);
      //avalMicroscopic->EnableAttachmentMarkers(false);
      avalMicroscopic->AvalancheElectron(x0_vec, y0_vec, z0_vec, t0_vec, e0_vec, dx0_vec, dy0_vec, dz0_vec);

      avalMicroscopic->GetAvalancheSize(ne, ni);
      int neGPU = 0, niGPU = 0;
      avalMicroscopic->GetAvalancheSizeGPU(neGPU, niGPU);
      const int np = avalMicroscopic->GetNumberOfElectronEndpoints();
      const int npGPU = avalMicroscopic->GetNumberOfElectronEndpointsGPU();
      double xe1, ye1, ze1, te1, e1;
      double xe2, ye2, ze2, te2, e2;
      double xi1, yi1, zi1, ti1;
      double xi2, yi2, zi2, ti2;
      int status;
      int countElectrons = 0;
      std::cout<<"here, np= "<< np<< " "<< npGPU<<std::endl;

      for (int j = 0; j< np; j++)
	{
	  avalMicroscopic->GetElectronEndpoint(j,
	  				       xe1, ye1, ze1, te1, e1,
	  				       xe2, ye2, ze2, te2, e2,
	  				       status);
	  //std::cout<< j << " "<< xe1 << " "<< ye1 << " "<< ze1<< " "<<te1 << " "
	  //<< xe2 << " "<< ye2 << " "<< ze2<< " "<<te2 << "\n";
	  /*
	    Nt_el_init_x.push_back(xe1);
	  Nt_el_init_y.push_back(ye1);
	  Nt_el_init_z.push_back(ze1);
	  Nt_el_fin_x.push_back(xe2);
	  Nt_el_fin_y.push_back(ye2);
	  Nt_el_fin_z.push_back(ze2);
	  Nt_el_fin_status.push_back(status);

	  if(bDriftIons)
	    {
	      drift->DriftIon(xe1, ye1, ze1, te1);
	      drift->GetIonEndpoint(0,
				    xi1, yi1, zi1, ti1, 
				    xi2, yi2, zi2, ti2, status);
	    }
	  if(status==-1 && ze2<-0.009)
	    countElectrons++;
	  */
	}
      for (int j = 0; j< npGPU; j++)
	{
	  avalMicroscopic->GetElectronEndpointGPU(j,
	  				       xe1, ye1, ze1, te1, e1,
	  				       xe2, ye2, ze2, te2, e2,
	  				       status);
	  //std::cout<< j << " "<< xe1 << " "<< ye1 << " "<< ze1<< " "<<te1 << " "
	  //	   << xe2 << " "<< ye2 << " "<< ze2<< " "<<te2 << "\n";
	}

#else
  if (nTestEvent == 1)
  {
    gas->SetMaxElectronEnergy(57.4604);
  }
  else
  {
    gas->SetMaxElectronEnergy(105.422);
  }
  avalMicroscopic->SetMaxNumShowerLoops(nMaxIter);
  //avalMicroscopic->SetShowProgress(true);
  avalMicroscopic->AvalancheElectron(x0_vec, y0_vec, z0_vec, t0_vec, e0_vec, dx0_vec, dy0_vec, dz0_vec);

  int ne_cpu = 0, ni_cpu = 0;
  avalMicroscopic->GetAvalancheSize(ne_cpu, ni_cpu);
  const int np = avalMicroscopic->GetNumberOfElectronEndpoints();
  
  std::cout << "Avalanche Size (CPU):   " << ne_cpu << "  " << ni_cpu << std::endl;
  std::cout << "Number of Electron Endpoints (CPU):   " << np << std::endl;

  double xe1, ye1, ze1, te1, e1;
  double xe2, ye2, ze2, te2, e2;
  double xi1, yi1, zi1, ti1;
  double xi2, yi2, zi2, ti2;
  int status;
  int countElectrons = 0;
  
  for (int j = 0; j< np; j++)
	{
	  avalMicroscopic->GetElectronEndpoint(j,
	  				       xe1, ye1, ze1, te1, e1,
	  				       xe2, ye2, ze2, te2, e2,
	  				       status);
	  //std::cout<< j << " "<< xe1 << " "<< ye1 << " "<< ze1<< " "<<te1 << " "
	  //<< xe2 << " "<< ye2 << " "<< ze2<< " "<<te2 << "\n";
	  /*
	    Nt_el_init_x.push_back(xe1);
	  Nt_el_init_y.push_back(ye1);
	  Nt_el_init_z.push_back(ze1);
	  Nt_el_fin_x.push_back(xe2);
	  Nt_el_fin_y.push_back(ye2);
	  Nt_el_fin_z.push_back(ze2);
	  Nt_el_fin_status.push_back(status);

	  if(bDriftIons)
	    {
	      drift->DriftIon(xe1, ye1, ze1, te1);
	      drift->GetIonEndpoint(0,
				    xi1, yi1, zi1, ti1, 
				    xi2, yi2, zi2, ti2, status);
	    }
	  if(status==-1 && ze2<-0.009)
	    countElectrons++;
	  */
	}
      
#endif


      unsigned int nEl = 0;
      unsigned int nIon = 0;
      unsigned int nAtt = 0;
      unsigned int nInel = 0;
      unsigned int nExc = 0;
      unsigned int nSup = 0;
      gas->GetNumberOfElectronCollisions(nEl, nIon, nAtt, nInel, nExc, nSup);
      gas->ResetCollisionCounters();
      nVUV.push_back(nExc + ni);

      // std::cout << "  Number of electrons: " << np << " (" << nTopPlane 
      // 		<< " of them ended on the top electrode and " << nBottomPlane 
      // 		<< " on the bottom electrode)\n"
      // 		<< "  Number of ions: " << ni << "\n"
      // 		<< "  Number of excitations: " << nExc << "\n";

      timer.Stop();

      std::cout << "Timer = "<< timer.RealTime()<< " "<< timer.CpuTime()<<std::endl;

      Nt_time_Real_Event=timer.RealTime();
      Nt_time_CPU_Event =timer.CpuTime();		
      Nt_time_Real_EventPerInitElectron= timer.RealTime()/1.; //for the moment just single electrons
      Nt_time_CPU_EventPerInitElectron= timer.CpuTime()/1.; //for the moment just single electrons

      tree->Fill();
      h->Fill(countElectrons);

#ifdef USEGPU
      // print out stats
      Garfield::AvalancheMicroscopic::Statistics stats{avalMicroscopic->GetStatistics()};

      int ne_cpu = 0, ni_cpu = 0;
      avalMicroscopic->GetAvalancheSize(ne_cpu, ni_cpu);

      int ne_gpu = 0, ni_gpu = 0;
      avalMicroscopic->GetAvalancheSizeGPU(ne_gpu, ni_gpu);

      std::cout << "Avalanche Size (CPU):   " << ne_cpu << "  " << ni_cpu << std::endl;
      std::cout << "Avalanche Size (GPU):   " << ne_gpu << "  " << ni_gpu << std::endl << std::endl;

      std::cout << "Number of Electron Endpoints (CPU):   " << avalMicroscopic->GetNumberOfElectronEndpoints() << std::endl;
      std::cout << "Number of Electron Endpoints (GPU):   " << avalMicroscopic->GetNumberOfElectronEndpointsGPU() << std::endl;

      avalMicroscopic->PrintComparisonStats();

      if (nMaxIter > 0){
        // dump stacks from the last iteration reqested
        int i = 0;
        for (auto elec : avalMicroscopic->GetStackOld())
        {
          //std::cout << elec << std::endl;
        }
        
        i = 0;
        for (auto elec : avalMicroscopic->GetStackOldGPU())
        {
          //std::cout << elec << std::endl;
        }
      }

      const int np_gpu = avalMicroscopic->GetNumberOfElectronEndpointsGPU(); 
      const int np_cpu = avalMicroscopic->GetNumberOfElectronEndpoints();
      double xe1_gpu, ye1_gpu, ze1_gpu, te1_gpu, e1_gpu;
      double xe2_gpu, ye2_gpu, ze2_gpu, te2_gpu, e2_gpu;
      int status_gpu;
      std::cout<<"gpu here, np_gpu= "<< np_gpu<<std::endl;

      /*
      for (int j = 0; j< (np_gpu < 10 ? np_gpu : 10); j++)
	    {
	      avalMicroscopic->GetElectronEndpointGPU(j,
                          xe1_gpu, ye1_gpu, ze1_gpu, te1_gpu, e1_gpu,
                          xe2_gpu, ye2_gpu, ze2_gpu, te2_gpu, e2_gpu,
                          status_gpu);

        std::cout << "GPU Endpoint check:  " <<
          xe1_gpu << "  " << ye1_gpu << "  " <<  ze1_gpu << "  " <<  te1_gpu << "  " <<  e1_gpu << " " <<
          xe2_gpu << "  " << ye2_gpu << "  " <<  ze2_gpu << "  " <<  te2_gpu << "  " <<  e2_gpu <<
          " " << status_gpu << std::endl;
      }

      for (int j = 0; j< (np < 10 ? np : 10); j++)
	    {
	      avalMicroscopic->GetElectronEndpoint(j,
                          xe1_gpu, ye1_gpu, ze1_gpu, te1_gpu, e1_gpu,
                          xe2_gpu, ye2_gpu, ze2_gpu, te2_gpu, e2_gpu,
                          status_gpu);

        std::cout << "CPU Endpoint check:  " <<
          xe1_gpu << "  " << ye1_gpu << "  " <<  ze1_gpu << "  " <<  te1_gpu << "  " <<  e1_gpu << " " <<
          xe2_gpu << "  " << ye2_gpu << "  " <<  ze2_gpu << "  " <<  te2_gpu << "  " <<  e2_gpu <<
          " " << status_gpu << std::endl;
      }
      */

      //std::cout<< " ### "<< np_cpu<< " "<< np_gpu << " "<< total_process_cpu<< " "<< total_transport_cpu << " "<<  total_process_cpu + total_transport_cpu<< " " 
	    //   << total_process_gpu << " "<< total_transport_gpu << " "<<  total_process_gpu + total_transport_gpu<< " "
	    //   << total_process_cpu / total_process_gpu<< " "<< total_transport_cpu / total_transport_gpu<< " " << (total_process_cpu + total_transport_cpu) / (total_process_gpu + total_transport_gpu)<<std::endl;
#endif
    }

  /**
  // Fill distribution of the number of VUV photons.
  auto nMinVUV = *std::min_element(nVUV.cbegin(), nVUV.cend());
  auto nMaxVUV = *std::max_element(nVUV.cbegin(), nVUV.cend());
  TH1D hVUV("hVUV", "", nMaxVUV - nMinVUV, nMinVUV, nMaxVUV);
  hVUV.StatOverflows(true);
  for (const auto& n : nVUV) hVUV.Fill(n);
  std::cout << "\n\nAverage number of emitted VUV photons: "
	    << hVUV.GetMean() << "\n";
  std::cout << "Determined value of J: "
	    << (hVUV.GetRMS() * hVUV.GetRMS()) / hVUV.GetMean() << "\n";
  **/
  if (plotDrift)
    {
      TCanvas* cf2 = new TCanvas("cf2", "", 600, 600);
      driftView->SetCanvas(cf2);
      //driftView->Plot();
      meshView->SetViewDrift(driftView);
      meshView->Plot();
      cf2->Print("DriftView.pdf");
      cf2->Print("DriftView.eps");
      cf2->Print("DriftView.png");
    }

  if (plotDrift)
    {
      ViewDrift* driftViewLines = new ViewDrift();
      ViewFEMesh* meshViewLines = new ViewFEMesh();
      DriftLineRKF* driftLine = new DriftLineRKF();
      driftLine->SetSensor(sensor);
      driftLine->SetIntegrationAccuracy(5e-10); //1e-8 is the default
      //driftLine->SetMaximumStepSize(2e-3);//in mus
      driftLine->EnablePlotting(driftViewLines);


      for(unsigned int iFM=0;iFM<allFM.size();iFM++)
	meshViewLines->SetComponent(allFM[iFM]);
      meshViewLines->SetArea(-2*MyGeometry.GetPitchInCM(), -2*MyGeometry.GetPitchInCM(), MyGeometry.GetDimZ0InCM()-MyGeometry.GetZdimInCM(),
			 2*MyGeometry.GetPitchInCM(),  2*MyGeometry.GetPitchInCM(), MyGeometry.GetDimZ0InCM());

      meshViewLines->SetPlane(0, -1, 0, 0, 0, 0.0);
      meshViewLines->SetFillMesh(true);
      // Set the color of the kapton.
      meshViewLines->SetColor(2, kYellow - 6);
      meshViewLines->EnableAxes();
      meshViewLines->SetXaxisTitle("x [cm]");
      meshViewLines->SetYaxisTitle("z [cm]");
      
      const int nLines = 50;
      for (int iLine=0;iLine<nLines;iLine++)
	{
	  
	  double x0 = - 2*MyGeometry.GetPitchInCM()/2. +  2.*MyGeometry.GetPitchInCM()/(nLines-1.)*iLine;
	  double y0 = 0;
	  double z0 = 0.1; 
	  double t0 = 0.;
	  driftLine->DriftElectron(x0, y0, z0, t0);
	}
      const int nLines2 = 10;
      for (int iLine=0;iLine<nLines2;iLine++)
	{
	  double x0 = - MyGeometry.GetPitchInCM()/2. +  (MyGeometry.GetPitchInCM()/2.-MyGeometry.GetHoleRadiusOuterInCM())/(nLines2-1.)*iLine;
	  double y0 = 0;
	  //double z0 = -0.353;
	  double z0 = -0.353; 
	  double t0 = 0.;
	  std::cout << x0 << " "<< y0<<" "<<std::endl;
	  driftLine->DriftIon(x0, y0, z0, t0);
	  //z0 = -0.053; 
	  z0 = -0.053; 
	  driftLine->DriftIon(x0, y0, z0, t0);
	}

      TCanvas* cf3 = new TCanvas("cf3", "", 600, 600);
      driftViewLines->SetCanvas(cf3);
      meshViewLines->SetViewDrift(driftViewLines);
      meshViewLines->Plot();
      cf3->Print("DriftViewLines.pdf");
      cf3->Print("DriftViewLines.eps");
      cf3->Print("DriftViewLines.png");

    }
  
  //  TCanvas*c=new TCanvas("c","",600,600);
  //h->Draw();
  treeMetadata->Fill();
  treeMetadata->Write();
  tree->Write();
  h->Write();
  fileOut->Close();
  if(!bBatch)
    app.Run(kTRUE);
  std::cout<<"end"<<std::endl;
  ////delete h;
  //delete tree;
  //delete fileOut;
  return 0;
}
