#include <iostream>
#include "Reader.h"

#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"

int main()
{
  
  //  TFile*f1=new TFile("simulation_results_100Torr_SingleGEM_zstart50mum.root","READ");
  
  TFile*f1=new TFile("simulation_results_THGEM_100_0.root","READ");
  //TFile*f1=new TFile("simulation_results_620Torr_TripleGEM_0.root","READ");
  //TFile*f1=new TFile("simulation_results_620Torr_TripleGEM_1.root","READ");
  //TFile*f1=new TFile("simulation_620Torr_singleGEM.root","READ");
  //TFile*f1=new TFile("simulation_100Torr_singleGEM.root","READ");
  Reader reader1(f1);
  long int nentries1=reader1.fChain->GetEntries();

  TH2F*hole_efficiency_num=new TH2F("hole_efficiency_num","",14/2,0.,0.014/2.,14, 0.,0.014);
  TH2F*hole_efficiency_den=new TH2F("hole_efficiency_den","",14/2,0.,0.014/2.,14, 0.,0.014);

  TH2F*electron_distribution=new TH2F("electron_distribution","",200,-0.1,0.1,200,-0.1,0.1);
  std::vector<int> nelectrons[3];
  int countAttachment=0;
  for(long int jentry=0;jentry<nentries1;jentry++)
    {
      if(jentry%1000==0)
	std::cout << jentry << " out of "<< nentries1<<std::endl;
      reader1.GetEntry(jentry);
      //std::cout<<reader1.el_fin_x->size()<<std::endl;
      TH1F*resolution_x[3];
      TH1F*resolution_y[3];
      for(int ii=0;ii<3;ii++)
	{
	  TString namex=Form("resolx_%i",ii);
	  TString namey=Form("resoly_%i",ii);
	  resolution_x[ii]=new TH1F(namex,"",10000,-0.05,0.05);
	  resolution_y[ii]=new TH1F(namey,"",10000,-0.05,0.05);
	}
      int countElectrons[3]={0};
      double init_elec_posx=0;
      double init_elec_posy=0;
      int initialElectron=-1;
      for(long int i=0;i<reader1.el_init_z->size();i++)
	{
	  if(reader1.el_init_z->at(i)>0.099)
	    //if(reader1.el_init_z->at(i)>0.0049)
	    {
	      init_elec_posx=reader1.el_init_x->at(i);
	      init_elec_posy=reader1.el_init_y->at(i);
	      initialElectron=i;
	      //std::cout<< reader1.el_fin_status->at(i)<<std::endl;
	    }
	}
      for(long int i=0;i<reader1.el_fin_z->size();i++)
	{
	  
	  if(reader1.el_fin_z->at(i)<-0.09)
	    countElectrons[0]++;
	  if(reader1.el_fin_z->at(i)<-0.25)
	    countElectrons[1]++;
	  if(reader1.el_fin_z->at(i)<-0.09)
	    {
	      countElectrons[2]++;
	      electron_distribution->Fill(reader1.el_fin_x->at(i)-init_elec_posx,reader1.el_fin_y->at(i)-init_elec_posy);
	      //resolution_x[i]->Fill(reader1.el_fin_x->at(i)-init_elec_posx);
	      //resolution_y[i]->Fill(reader1.el_fin_y->at(i)-init_elec_posy);
	    }
	}

      hole_efficiency_den->Fill(init_elec_posx,init_elec_posy);
      if(countElectrons[0]>0)
	  hole_efficiency_num->Fill(init_elec_posx,init_elec_posy);

      for(long int i=0;i<reader1.el_fin_z->size();i++)
	{
	  
	  if(reader1.el_fin_z->at(i)<-0.02)
	    countElectrons[0]++;
	  if(reader1.el_fin_z->at(i)<-0.25)
	    countElectrons[1]++;
	  if(reader1.el_fin_z->at(i)<-0.09)
	    {
	      countElectrons[2]++;
	      //resolution_x[i]->Fill(reader1.el_fin_x->at(i)-init_elec_posx);
	      //resolution_y[i]->Fill(reader1.el_fin_y->at(i)-init_elec_posy);
	    }
	}
      if(countElectrons[2]==0)
	{
	  // std::cout<< "did not manage = "<< reader1.el_fin_status->at(initialElectron)<< " "
	  // 	 << reader1.el_fin_x->at(initialElectron) << " "
	  // 	 << reader1.el_fin_y->at(initialElectron) << " "
	  // 	 << reader1.el_fin_z->at(initialElectron) << " "
	  // 	 <<std::endl;
	  countAttachment++;
	}
      // else
      // 	std::cout<< "manage = "<< reader1.el_fin_status->at(initialElectron)<< " "
      // 		 << reader1.el_fin_x->at(initialElectron) << " "
      // 		 << reader1.el_fin_y->at(initialElectron) << " "
      // 		 << reader1.el_fin_z->at(initialElectron) << " "
      // 		 <<std::endl;

      for(int ii=0;ii<3;ii++)
	{
	  nelectrons[ii].push_back(countElectrons[ii]);	 
	  //std::cout<< resolution_x[ii]->GetMean() << "\t" << resolution_x[ii]->GetRMS()<<std::endl;
	  //
	  delete resolution_x[ii];
	  delete resolution_y[ii];
	}
    }

  TH1F*h_gain[3];
  for(int ii=0;ii<3;ii++)
    {
      auto nMin = *std::min_element(nelectrons[0].cbegin(), nelectrons[0].cend());
      auto nMax = *std::max_element(nelectrons[0].cbegin(), nelectrons[0].cend());
      std::cout<<nMin << " "<<nMax<<std::endl;
      TString name=Form("gain_%i",ii);
      h_gain[ii]=new TH1F(name,"",nMax-nMin,nMin,nMax);
      for (const auto& n : nelectrons[ii])
	h_gain[ii]->Fill(n);
    }
  TFile* fout=new TFile("output.root","RECREATE");
  electron_distribution->Write();
  h_gain[0]->Write();
  h_gain[1]->Write();
  h_gain[2]->Write();
  hole_efficiency_den->Write();
  hole_efficiency_num->Write();
  fout->Close();

  std::cout <<" Attachment: "<<countAttachment <<std::endl;
  return 0;
}

