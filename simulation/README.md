** GEM ANSYS simulations

File structure:
GEMansys_X_Y_Z

X: drift (or transition) electric field in V/cm
Y: voltage difference of the two sides of GEM
Z: induction (or transition) electric field in V/cm

** Gas files
File structure
e.g. pure gas: gasFile_X_Y_Ar.gas
e.g. binary gas: gasFile_X_Y_Ar_Z_CH4.gas

X: pressure
Y: contanminants (0 means no contaminants)
Z: fraction of first element

