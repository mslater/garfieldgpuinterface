#!/bin/bash
INPUT_process=$1
INPUT_initdir=$2
INPUT_events=$3
INPUT_batch=$4
INPUT_driftIons=$5
INPUT_planeOffset=$6
INPUT_gasPressure=$7
INPUT_geometry=$8
	   
# export GARFIELD_HOME=/home/$(whoami)/garfield
#export HEED_DATABASE=${INPUT_initdir}/../../Heed/heed++/database
pwd
cp -r ${INPUT_initdir}/../GEM .
cp -r ${INPUT_initdir}/../THGEM .
cp -r ${INPUT_initdir}/../gas .
cp ${INPUT_initdir}/../simulation .
source /home/kn/root-6.20.02/build/bin/thisroot.sh
# SHLIB_PATH=/home/kn/root-6.20.02/build/lib
# DYLD_LIBRARY_PATH=/home/kn/root-6.20.02/build/lib
# LIBPATH=/home/kn/root-6.20.02/build/lib
# ROOTSYS=/home/kn/root-6.20.02/build
# LD_LIBRARY_PATH=/home/kn/migdtest/build/lib:/home/kn/root-6.20.02/build/lib
source ${INPUT_initdir}/../../setup.sh

#ls -hlrt
root-config --cxx
env

#FOR 2 GAS INPUTS, USE THIS
args=""
if [ ${INPUT_batch} -eq 1 ]; then
    args+="-b "
fi
if [ ${INPUT_driftIons} -eq 1 ];then
    args+="--drift-ions "
fi
args+="--gas-pressure "
args+=${INPUT_gasPressure}
args+=" "
args+="--plane-offset "
args+=${INPUT_planeOffset}
args+=" --"
args+=${INPUT_geometry}
echo "./simulation -n ${INPUT_events} ${args}"
./simulation -n ${INPUT_events} ${args}

cp results.root simulation_results_${INPUT_gasPressure}_${INPUT_geometry}_${INPUT_planeOffset}_${INPUT_process}.root
rm simulation
