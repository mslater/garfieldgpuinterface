#include "GeometryDetector.h"
#include <iostream>
GeometryDetector::GeometryDetector()
{}
GeometryDetector::~GeometryDetector()
{}
void GeometryDetector::InitialiseGlassGEM(int v1, int v2, int v3)
{
  m_detectorType="GlassGEM";
  m_pitch = 0.280;//Remember in mm
  m_zdimension_start=1.285;
  m_zdimension=2.570;
  m_maxVolt=v1*0.1+v2*1.+v3*0.1;
  m_holeRadiusOuter = 0.170;// in mm
  m_holeRadiusInner = 0.170;// in mm
  //m_fieldMaps.push_back("_300_400_400");
  TString name=Form("_%i_%i_%i", v1,v2,v3);
  m_fieldMaps.push_back(name);
  //std::cout <<"voltages = "<< name <<std::endl;
  //m_fieldMaps.push_back("_300_600_400");
  //m_fieldMaps.push_back("_300_700_400");
  m_fieldMaps_dx.push_back(0.);
  m_fieldMaps_dy.push_back(0.);
  m_fieldMaps_dz.push_back(0.);
  m_fieldMaps_dV.push_back(0.);

}

void GeometryDetector::InitialiseDoubleGlassGEM(double planeOffset,int v1, int v2, int v3, int v4, int v5)
{
  m_detectorType="GlassGEM";
  m_pitch = 0.280;//Remember in mm
  m_zdimension_start=1.285;
  m_zdimension=2*2.570; //this is the full setup

  
  //m_maxVolt= 200*0.1+500+2000*0.1+2000*0.1+530.+4100*0.1;
  //m_maxVolt= 200*0.1+500+1000*0.1+1000*0.1+530.+1500*0.1;
  //m_maxVolt= 200*0.1+500+600*0.1+600*0.1+530.+700*0.1;
  //m_maxVolt= 200*0.1+550+600*0.1+600*0.1+570.+700*0.1;
  //m_maxVolt= 200*0.1+550+600*0.1+600*0.1+550.+700*0.1;
  //m_maxVolt= 200*0.1+530+600*0.1+600*0.1+550.+700*0.1;
  //m_maxVolt= 200*0.1+530+600*0.1+600*0.1+530.+700*0.1;

  m_maxVolt= v1*0.1+v2+v3*0.1+v3*0.1+v4+v5*0.1;

  m_holeRadiusOuter = 0.170;// in mm
  m_holeRadiusInner = 0.170;// in mm
  //TString name=Form("_%i_%i_%i", v1,v2,v3);
  //m_fieldMaps.push_back(name);

  //m_fieldMaps.push_back("_200_500_2000");
  //m_fieldMaps.push_back("_200_500_1000");
  //m_fieldMaps.push_back("_200_500_600");
  //m_fieldMaps.push_back("_200_550_600");
  //m_fieldMaps.push_back("_200_550_600");
  //m_fieldMaps.push_back("_200_530_600");
  //m_fieldMaps.push_back("_200_530_600");
  m_fieldMaps.push_back(Form("_%d_%d_%d",v1,v2,v3));
  m_fieldMaps_dx.push_back(0.);
  m_fieldMaps_dy.push_back(0.);
  m_fieldMaps_dz.push_back(0.);
  m_fieldMaps_dV.push_back(0.);

  //m_fieldMaps.push_back("_2000_530_4100");
  //m_fieldMaps.push_back("_1000_530_1500");
  //m_fieldMaps.push_back("_600_530_700");
  //m_fieldMaps.push_back("_600_570_700");
  //m_fieldMaps.push_back("_600_550_700");
  //m_fieldMaps.push_back("_600_550_700");
  //m_fieldMaps.push_back("_600_530_700");
  m_fieldMaps.push_back(Form("_%d_%d_%d",v3,v4,v5));

  m_fieldMaps_dx.push_back(planeOffset*m_pitch/2.);
  m_fieldMaps_dy.push_back(0.);
  m_fieldMaps_dz.push_back(-2.570);
  //m_fieldMaps_dV.push_back( 200*0.1+500+2000*0.1);
  //m_fieldMaps_dV.push_back( 200*0.1+500+1000*0.1);
  //m_fieldMaps_dV.push_back( 200*0.1+500+600*0.1);
  //m_fieldMaps_dV.push_back( 200*0.1+550+600*0.1);
  //m_fieldMaps_dV.push_back( 200*0.1+550+600*0.1);
  //m_fieldMaps_dV.push_back( 200*0.1+530+600*0.1);
  //m_fieldMaps_dV.push_back( 200*0.1+530+600*0.1);
  m_fieldMaps_dV.push_back( v1*0.1+v2+v3*0.1);
}

void GeometryDetector::InitialiseStandardTHGEM()
{
  m_detectorType="THGEM";
  m_pitch = 0.7;//Remember in mm
  m_zdimension_start=1.5;
  m_zdimension=3;
  m_maxVolt=970.;
  m_holeRadiusOuter = 0.2;// in mm
  m_holeRadiusInner = 0.2;// in mm
  m_fieldMaps.push_back("_300_900_400");
  m_fieldMaps_dx.push_back(0.);
  m_fieldMaps_dy.push_back(0.);
  m_fieldMaps_dz.push_back(0.);
  m_fieldMaps_dV.push_back(0.);

}
void GeometryDetector::InitialiseStandardDoubleTHGEM(double planeOffset)
{
  m_detectorType="THGEM";
  m_pitch = 0.7;//Remember in mm
  m_zdimension_start=1.5;
  m_zdimension=6;
  m_maxVolt=1670.;
  m_holeRadiusOuter = 0.2;// in mm
  m_holeRadiusInner = 0.2;// in mm

  m_fieldMaps.push_back("_300_900_400");
  m_fieldMaps_dx.push_back(0.);
  m_fieldMaps_dy.push_back(0.);
  m_fieldMaps_dz.push_back(0.);
  m_fieldMaps_dV.push_back(0.);

  m_fieldMaps.push_back("_400_610_500");
  m_fieldMaps_dx.push_back(planeOffset*m_pitch/2.);
  m_fieldMaps_dy.push_back(0.);
  m_fieldMaps_dz.push_back(-3.);
  m_fieldMaps_dV.push_back(970);

}

void GeometryDetector::InitialiseStandardTripleGEM(double planeOffset)
{
  m_detectorType="GEM";
  m_pitch = 0.14;//Remember in mm
  m_zdimension_start=1.5;
  m_zdimension=6;
  m_maxVolt=0.;
  m_holeRadiusOuter = 0.07;// in mm
  m_holeRadiusInner = 0.05;// in mm

  m_fieldMaps.push_back("_400_279_1400");
  m_fieldMaps_dx.push_back(0.);
  m_fieldMaps_dy.push_back(0.);
  m_fieldMaps_dz.push_back(0.);
  m_fieldMaps_dV.push_back(0.);
  m_fieldMaps.push_back("_1400_334_1670");
  m_fieldMaps_dx.push_back(planeOffset*m_pitch/2.);
  m_fieldMaps_dy.push_back(0.);
  m_fieldMaps_dz.push_back(-2.);
  m_fieldMaps_dV.push_back(459);
  m_fieldMaps.push_back("_1670_380_260");
  m_fieldMaps_dx.push_back(0.);
  m_fieldMaps_dy.push_back(0.);
  m_fieldMaps_dz.push_back(-4.);
  m_fieldMaps_dV.push_back(1110);
}

void GeometryDetector::InitialiseStandardGEM()
{
  m_detectorType="GEM";
  m_pitch = 0.14;//Remember in mm
  m_zdimension_start=1;
  m_zdimension=2;
  m_maxVolt=0.;
  m_holeRadiusOuter = 0.07;// in mm
  m_holeRadiusInner = 0.05;// in mm

  m_fieldMaps.push_back("_400_279_1400");
  m_fieldMaps_dx.push_back(0.);
  m_fieldMaps_dy.push_back(0.);
  m_fieldMaps_dz.push_back(0.);
  m_fieldMaps_dV.push_back(0.);
}

