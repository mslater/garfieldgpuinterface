#ifndef GEOMETRYDETECTOR_H
#define GEOMETRYDETECTOR_H
#include <string>
#include <vector>
#include <TString.h>

class GeometryDetector {
  //the distance units here are mm (what is in the ANSYS file)
  //potential in Volt
  //electric fields in V/cm
 public:
  GeometryDetector();
  ~GeometryDetector();
  void InitialiseStandardTripleGEM(double planeOffset=0);
  void InitialiseStandardGEM();
  void InitialiseStandardTHGEM();
  void InitialiseStandardDoubleTHGEM(double planeOffset=0.);
  void InitialiseGlassGEM(int v1=300, int v2=600, int v3=400);
  void InitialiseDoubleGlassGEM(double planeOffset=0, int v1=200, int v2=550, int v3=600, int v4=570, int v5=700);

  double GetPitchInCM(){return m_pitch/10.;}
  double GetPitchInMM(){return m_pitch;}
  double GetHoleRadiusOuterInCM(){return m_holeRadiusOuter/10.;}
  double GetHoleRadiusOuterInMM(){return m_holeRadiusOuter;}
  double GetHoleRadiusInnerInCM(){return m_holeRadiusInner/10.;}
  double GetHoleRadiusInnerInMM(){return m_holeRadiusInner;}
  double GetZdimInCM(){return m_zdimension/10.;}
  double GetZdimInMM(){return m_zdimension;}
  void SetZdimInCM(double zdim){m_zdimension=zdim*10.;}
  void SetZdimInMM(double zdim){m_zdimension=zdim;}

  double GetDimZ0InCM(){return m_zdimension_start/10.;}
  double GetDimZ0InMM(){return m_zdimension_start;}
  void SetDimDeltaZ0InCM(double zdim){m_zdimension_start=zdim*10.;}
  void SetDimDeltaZ0InMM(double zdim){m_zdimension_start=zdim;}

  double GetMaxVolt(){return m_maxVolt;}

  std::string GetDetectorType(){return m_detectorType;}
  std::vector<TString>* GetFieldMaps(){return &m_fieldMaps;}
  std::vector<double>* GetFieldMaps_dx(){return &m_fieldMaps_dx;}
  std::vector<double>* GetFieldMaps_dy(){return &m_fieldMaps_dy;}
  std::vector<double>* GetFieldMaps_dz(){return &m_fieldMaps_dz;}
  std::vector<double>* GetFieldMaps_dV(){return &m_fieldMaps_dV;}
 private:
  std::string m_detectorType;
  double m_pitch;
  double m_holeRadiusOuter;
  double m_holeRadiusInner;
  double m_zdimension_start;
  double m_zdimension;
  double m_maxVolt;
  std::vector<TString> m_fieldMaps;
  std::vector<double> m_fieldMaps_dx;
  std::vector<double> m_fieldMaps_dy;
  std::vector<double> m_fieldMaps_dz;
  std::vector<double> m_fieldMaps_dV;

};
#endif //GEOMETRYDETECTOR_H
