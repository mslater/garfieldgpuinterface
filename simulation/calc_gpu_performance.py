#!/usr/bin/python

import subprocess

i = 100
perf_dict = {}
while i < 100000:

    #out = subprocess.run(["./simulation", "--test-event", "2", "-n", "{}".format(i)], stdout=subprocess.PIPE).stdout
    out = subprocess.check_output(["./simulation", "--test-event", "2", "-n", "{}".format(i)])

    for ln in out.split('\n'):
        if ln.find('###') != -1:
            perf_dict[i] = float(ln.split()[-1])

    i += 100

perf_file = open("perf_results.txt", "w")

for num in sorted(perf_dict.keys()):
    perf_file.write("{}  {}\n".format(num, perf_dict[num]))
