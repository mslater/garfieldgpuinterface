//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Apr  1 13:50:54 2020 by ROOT version 6.20/02
// from TTree output/output
// found on file: simulation_results_620Torr_0.root
//////////////////////////////////////////////////////////

#ifndef Reader_h
#define Reader_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
using namespace std;
class Reader {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   vector<double>  *el_init_x;
   vector<double>  *el_init_y;
   vector<double>  *el_init_z;
   vector<double>  *el_fin_x;
   vector<double>  *el_fin_y;
   vector<double>  *el_fin_z;
   vector<double>  *el_fin_status;

   // List of branches
   TBranch        *b_el_init_x;   //!
   TBranch        *b_el_init_y;   //!
   TBranch        *b_el_init_z;   //!
   TBranch        *b_el_fin_x;   //!
   TBranch        *b_el_fin_y;   //!
   TBranch        *b_el_fin_z;   //!
   TBranch        *b_el_fin_status;   //!

   Reader(TTree *tree=0);
  Reader(TFile *file=0);
   virtual ~Reader();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Reader_cxx
Reader::Reader(TFile *file) : fChain(0) 
{
  TTree*tree=(TTree*)file->Get("output");
  // if (tree == 0) {
  //     TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("simulation_results_620Torr_0.root");
  //     if (!f || !f->IsOpen()) {
  //        f = new TFile("simulation_results_620Torr_0.root");
  //     }
  //     f->GetObject("output",tree);

  //  }
   Init(tree);
}
Reader::Reader(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("simulation_results_620Torr_0.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("simulation_results_620Torr_0.root");
      }
      f->GetObject("output",tree);

   }
   Init(tree);
}

Reader::~Reader()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Reader::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Reader::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Reader::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   el_init_x = 0;
   el_init_y = 0;
   el_init_z = 0;
   el_fin_x = 0;
   el_fin_y = 0;
   el_fin_z = 0;
   el_fin_status = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("el_init_x", &el_init_x, &b_el_init_x);
   fChain->SetBranchAddress("el_init_y", &el_init_y, &b_el_init_y);
   fChain->SetBranchAddress("el_init_z", &el_init_z, &b_el_init_z);
   fChain->SetBranchAddress("el_fin_x", &el_fin_x, &b_el_fin_x);
   fChain->SetBranchAddress("el_fin_y", &el_fin_y, &b_el_fin_y);
   fChain->SetBranchAddress("el_fin_z", &el_fin_z, &b_el_fin_z);
   fChain->SetBranchAddress("el_fin_status", &el_fin_status, &b_el_fin_status);
   Notify();
}

Bool_t Reader::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Reader::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Reader::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Reader_cxx
