# Garfield GPU Interface for `AvalancheMicroscopic` class

This repo contains the code for running Garfield++ `AvalancheMicroscopic` based jobs on NVidia GPUs. 

## Prerequisites

* Whatever usual setup is done for your particular use case
* CUDA installed and `nvcc` directory is in `PATH`

## Compiling

To compile with GPU support, do the following:

* Clone this repo
* Replace the `garfieldpp` directory you are using at present with the one in this repo
* Create a build directory
* Run the following:
```
cmake -DUSEGPU=ON <src dir>
make
```

## Using the GPU extensions

The easiest way to start using your GPU is to simply call the following before `AvalancheElectron`:
```
aval->SetRunModeOptions(MPRunMode::GPUExclusive, nDevice)
```
This will tell Garfield to **only** use the GPU and you can specify the device number with the second argument.
NOTE: At present the automatic changing of the energy lookup tables isn't done so it's recommended to also set
the maximum energy fairly high for your usecase, e.g.:
```
gas->SetMaxElectronEnergy(150.0);
```

You can retrieve the end points, etc. using the following:
```
avalMicroscopic->GetAvalancheSizeGPU(neGPU, niGPU);
const int npGPU = avalMicroscopic->GetNumberOfElectronEndpointsGPU();
avalMicroscopic->GetElectronEndpointGPU(j,
	  				       xe1, ye1, ze1, te1, e1,
	  				       xe2, ye2, ze2, te2, e2,
	  				       status);
```

## Specific Info for Birmingham/Migdal Users

How to compile and run:

* Ensure you're epldt001 or epldt003
* Clone the repo
* create a build directory somewhere else
* Run the following commands (or put them in a script to ```source``` from a new shell), putting in the path for the git repo:

```
export MIGDALHOME=<git_repo_path>
source /cvmfs/sft.cern.ch/lcg/views/LCG_94/x86_64-centos7-gcc62-opt/setup.sh
export GARFIELD_HOME=$MIGDALHOME/garfieldpp
export HEED_DATABASE=$MIGDALHOME/garfieldpp/Heed/heed++/database/
export PATH=/usr/local/cuda/bin:$PATH
```

* ```cd``` to your build dir and run (Note that the PATH for CUDA will be needed if you want to build with the GPU interface):
```
cmake -DUSEGPU=ON $MIGDALHOME
make
```
* Copy the GEM and Gas Files to your build directory:
```
cd simulation
cp -r $MIGDALHOME/simulation/GasFiles .
cp -r $MIGDALHOME/simulation/GEM .
cp -r $MIGDALHOME/simulation/THGEM .
```
* Run using, e.g.:
```
./simulation  --singleStandardTHGEM -b
```
* If GPU use has been selected it will automatically be setup and used.
* There are various extra options in `simulation.cxx` to drive the application. To view them, use:
```
./simulation --help
```

# More Detailed Info On Changes

Here are the various options that can be controlled:

* In `GPUInterface.h`:
    - `USEPRECALCRNG`: Set this define to force both GPU and CPU code to use a pre-calculated set of random numbers.
        This slows things down but means you can properly compare the same showers computed on the GPU and CPU. The default implies ~14GB of GPU memory needed.
    - `MAXRNGDRAWS`: The maximum number of RNG 'draws' for each thread/electron that can be done before a wrap around occurs and numbers are reused
    - `MAXPARTICLES`: The maximum number of particles that the GPU will hold in it's stacks. As the memory allocation is static, this needs to be set
        at compile time. If you're not using the the precalculated RNG setup, this can be really quite high.
    - `MAXSTACKSIZE`: The maximum number of **active** particles that the GPU will hold in it's stacks. This can be less than the value for `MAXPARTICLES`.
        If you hit problems with either of these, you will get a CUDA runtime error.

* `AvalancheMicroscopic` interface:
    - `SetRunModeOptions(MPRunMode mode, int device = -1)`: Allows the setting of the run mode (see below) and the CUDA device to use.
    - `SetMaxNumShowerLoops(int max_loops)`: Best used when investigating a single large event. This limits the number of **shower** iterations that will be carried out
        on that event.
    - `SetShowProgress(bool show_progress)`: Provides more debug info during an event processing showing size of stacks and time taken.
    - `SetDebugShowerIterationAndElectronID(int iter_num, int elec_id)`: Prints out detailed debug info for a particular electron and shower iteration during the processing.
        Very useful for direct and detailed comparisons of GPU/CPU processing
    - `PrintComparisonStats()`: Print out a nicely formatted set of stats for a particular event run to compare performaces. This data can be retrieved separately as well.

* `MPRunMode`:
    - `None`: Original, CPU only run
    - `GPUWhenAppropriate`: Switch to use GPU when showers get large enough (**NOT IMPLEMENTED YET**)
    - `GPUExclusive`: Only use the GPU for calculations
    - `CPUGPUComparison`: Run the same starting state through both CPU and GPU processing chains to compare results.
    

# TODO list

List of things still to implement:

* Proper switching between using the GPU and not
* Properly handle changes to the energy lookup maps

# Current benchmark

Running the following with `USEPRECALCRNG` defined:
```
./simulation --test-event 1
```

gave the following benchmark results:
```


```