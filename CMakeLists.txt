cmake_minimum_required (VERSION 3.3)
project (MigdalEffect)

add_subdirectory (garfieldpp)
add_subdirectory (simulation)
add_subdirectory (gasAnalysis)
set(CMAKE_ROOTSYS $ENV{ROOTSYS})
configure_file(env.sh.in
  ${CMAKE_BINARY_DIR}/setup.sh @ONLY)
message(STATUS "remember to source:" )
message(STATUS ${CMAKE_BINARY_DIR}/setup.sh )
message(STATUS "For day to day setup" )

