#include <TROOT.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TAxis.h>
#include <TString.h>
#include <iostream>
#include <string> 

#include "Garfield/MediumMagboltz.hh"
#include "Garfield/FundamentalConstants.hh"
#include "TransportParameters.hh"
  
using namespace Garfield;

void rootlogonATLAS(double ytoff = 1.0, bool atlasmarker =true, double left_margin=0.14);

std::vector<double> GenerateEFieldPoints(double npoints, double EMax, double EMin); //Generate logarithmically spaced E-Field Points between EMin and EMax for use in plotting.

std::vector<double> GetTownsendVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType); //Use these E-Field Points to Get out values of Townsend Coefficient for desired gas properties

std::vector<double> GetAttachmentVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType); //Use these E-Field Points to Get out values of Attachment Coefficient for desired gas properties

std::vector<double> GetAttachmentVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType, double O2ppm); //Function for Oxygen attachment scaling

std::vector<double> GetDriftVelocityVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType); //Use these E-Field Points to Get out values of Drift Velocity for desired gas properties

std::vector<double> GetTransverseDiffusionVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType); //Use these E-Field Points to Get out values of Transverse Diffusion for desired gas properties

std::vector<double> GetLongitudinalDiffusionVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType); //Use these E-Field Points to Get out values of Longitudinal Diffusion for desired gas properties

void giveAxis(int graphType, double* lowerLim, double* upperLim, std::string* axisTitle)
{
  switch(graphType)
    {
    case 0://Townsend Coefficient
      (*upperLim)=1e3;
      (*lowerLim)=1e-4;
      (*axisTitle) = "Townsend Coefficient [1/cm]";      
      break;
	
    case 1:// Attachment Coefficient
      (*upperLim)=1e3;
      (*lowerLim)=1e-5;
      (*axisTitle) = "Attachment Coefficient [1/cm]";
      break;
	
    case 2://Drift Velocity
      (*upperLim)=1e-2;
      (*lowerLim)=1e-6;
      (*axisTitle) = "Drift Velocity [cm/ns]";
      break;
	
    case 3://Transverse Diffusion
      (*upperLim)=0.8;
      (*lowerLim)=1e-2;
      (*axisTitle) = "Transverse Diffusion Coefficient [cm^{1/2}]";
      break;
	
    case 4://Longitudinal Diffusion
      (*upperLim)=0.8;
      (*lowerLim)=1e-2;
      (*axisTitle) = "Longitudinal Diffusion Coefficient [cm^{1/2}]";
      break;
	
    default:
      std::cout << "Invalid Plot Type!" << std::endl;
    }
}
//=================================MAIN CODE=================================//

int main(int argc, char *argv[])
{
  TApplication app("app", &argc, argv);
  
  rootlogonATLAS();
 
  // Set up TransportParameter Object
  //std::vector<MediumMagboltz*>
  MediumMagboltz* pure = new MediumMagboltz();
  pure->LoadGasFile("../gas/Ar_80_CO2_20.gas");
  // MediumMagboltz* O2 = new MediumMagboltz();
  // O2->LoadGasFile("../output/He_72p5_Ne_25_CH4_2p5_100ppm_O2.gas");
  // MediumMagboltz* H2O = new MediumMagboltz();
  // H2O->LoadGasFile("../output/He_72p5_Ne_25_CH4_2p5_10ppm_H2O.gas");
  TString label("TownsendTest");
  //TransportParameters* parameterTest = new TransportParameters(pure, O2, H2O, label);
  TransportParameters* parameterTest = new TransportParameters(pure, NULL, NULL, label);

  //Set up EField points vector
  int nPoints = 64;
  double EMax = 1e4;
  double EMin = 100;
  
  std::vector<double> EFields = GenerateEFieldPoints(nPoints, EMax, EMin);
  
  // Get values of Townsend coefficient using TransportParameter Object, EFields and desired properties
  double pressureBar = 100/760.*1.01325;
  int impurityType = 0;
  double O2ppm = 0.;
  std::vector<double> testVec = GetDriftVelocityVector(parameterTest, EFields, pressureBar, impurityType);
  //std::vector<double> o2attach = GetAttachmentVector(parameterTest, EFields, 3.1, 1, 0.1);
  
  std::vector<double> bar1Vec = GetLongitudinalDiffusionVector(parameterTest, EFields, pressureBar/2, 0);
  std::vector<double> bar2Vec = GetLongitudinalDiffusionVector(parameterTest, EFields, pressureBar/4, 0);
  std::vector<double> bar3Vec = GetLongitudinalDiffusionVector(parameterTest, EFields, pressureBar*2, 0);
  std::vector<double> bar4Vec = GetLongitudinalDiffusionVector(parameterTest, EFields, pressureBar*4, 0);

  std::vector<std::vector<double> > barVec;
  barVec.push_back(bar1Vec);
  barVec.push_back(bar2Vec);
  barVec.push_back(bar3Vec);
  barVec.push_back(bar4Vec);
  // Make a graph showing the plot
  TCanvas* c1 = new TCanvas("c1", "Test of TransportParameters Class", 600, 600);
  c1->SetLogx();
  //c1->SetLogy();
  
  TMultiGraph *mg = new TMultiGraph();
  int colors[4]={kBlue,kRed,kGreen,kViolet};
  std::vector<TGraph*> testGraph;
  for(int i=0;i<4;i++)
    {
      TGraph*testGraph1 = new TGraph(EFields.size(), &(EFields[0]), &(barVec[i][0]));
      testGraph1->SetLineColor(colors[i]);
      testGraph1->SetLineWidth(2);
      testGraph1->SetMarkerStyle(24);
      testGraph1->SetMarkerSize(0.7);
      testGraph1->SetMarkerColor(kBlack);
      testGraph.push_back(testGraph1);
    }
    
  int graphType = 4;
  double upperLim = 1;
  double lowerLim = 0;
  std::string axisTitle;
  std::string otherTitles("Test Curve; E-Field Strength [V/cm];");

  giveAxis(graphType,&lowerLim,&upperLim, &axisTitle);
  
  TString newaxisTitle=otherTitles + axisTitle;
    
  TH2F*dummyTest=new TH2F("dummyTest",newaxisTitle,10000,EMin,EMax,1000,lowerLim,upperLim);
  dummyTest->GetXaxis()->SetTitleOffset(1.2);
  dummyTest->GetYaxis()->SetTitleOffset(1.25);
  dummyTest->Draw();
  for(int i=0;i<4;i++)
    mg->Add(testGraph[i]);
  mg->Draw("PL");

  TLegend* legend = new TLegend(0.35,0.9,0.55,0.7);
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetTextSize(0.035);
  legend->SetFillStyle(1000);
  legend->SetHeader("Pressure (bar)","C");
  for(int i=0;i<4;i++)
    legend->AddEntry(testGraph[i], "1");
  legend->Draw();  

  c1->Print("./Ar_80_CO2_20_LD2.png");
  c1->Print("./Ar_80_CO2_20_LD2.pdf");
  c1->Print("./Ar_80_CO2_20_LD2.eps");
  c1->Print("./Ar_80_CO2_20_LD2.C");
  
  
  
  app.Run(); // Keeps up canvas
  return 0;
} 




//=================================FUNCTIONS=================================//


std::vector<double> GenerateEFieldPoints(double npoints, double EMax, double EMin)
{
  double EField=0;
  
  double LogMin = log(EMin);
  double LogMax = log(EMax);
  double step = (LogMax - LogMin)/(npoints-1);
  
  std::vector<double> EFieldPoints;
  
  for (double logEField = LogMin; logEField<LogMax+step/2; logEField+=step)
    {
      EField = exp(logEField);
      EFieldPoints.push_back(EField);
    }
  
  return EFieldPoints;
}

std::vector<double> GetTownsendVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType)
{
  std::vector<double> alphaVec;
  double alpha;
  
  for(unsigned int iPoint = 0; iPoint < EFields.size(); iPoint++){
    alpha = gas->GetElectronTownsend(EFields[iPoint], pressureBar, impurityType);
    alphaVec.push_back(alpha);
  }
  
  return alphaVec;
}

std::vector<double> GetAttachmentVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType)
{
  std::vector<double> etaVec;
  double eta;
  
  for(unsigned int iPoint = 0; iPoint < EFields.size(); iPoint++){
    eta = gas->GetElectronAttachment(EFields[iPoint], pressureBar, impurityType);
    etaVec.push_back(eta);
  }
  
  return etaVec;  
}

std::vector<double> GetAttachmentVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType, double O2ppm)
{
  std::vector<double> etaVec;
  double eta;
  
  if(impurityType == 1)
    for(unsigned int iPoint = 0; iPoint < EFields.size(); iPoint++)
      {
	eta = gas->GetElectronAttachment(EFields[iPoint], pressureBar, impurityType, O2ppm);
	etaVec.push_back(eta);
      }
  else
    {
      std::cout << "[transportPlots] ERROR: Entered a value for O2 contamination for impurity type != 1 \n"
		<< "Perhaps you meant to use GetAttachmentVector(TransportParameters*, std::vector<double>, double, int, double)?" << std::endl;
    }
  
  
  return etaVec;  
}

std::vector<double> GetDriftVelocityVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType)
{
  std::vector<double> driftvVec;
  double driftv;
  
  for(unsigned int iPoint = 0; iPoint < EFields.size(); iPoint++)
    {
      driftv = gas->GetElectronDriftVelocity(EFields[iPoint], pressureBar, impurityType);
      driftvVec.push_back(driftv);
    }

  return driftvVec;
}

std::vector<double> GetTransverseDiffusionVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType)
{
  std::vector<double> transDiffVec;
  double transDiff;

  for(unsigned int iPoint = 0; iPoint < EFields.size(); iPoint++)
    {
      transDiff = gas->GetElectronTransverseDiffusion(EFields[iPoint], pressureBar, impurityType);
      transDiffVec.push_back(transDiff);
    }
  
  return transDiffVec;
}


std::vector<double> GetLongitudinalDiffusionVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType)
{
  std::vector<double> longDiffVec;
  double longDiff;
  
  for(unsigned int iPoint = 0; iPoint < EFields.size(); iPoint++)
    {
      longDiff = gas->GetElectronLongitudinalDiffusion(EFields[iPoint], pressureBar, impurityType);
      longDiffVec.push_back(longDiff);
    }
  
  return longDiffVec;
}




void rootlogonATLAS(double ytoff, bool atlasmarker, double left_margin) 
{
  TStyle* atlasStyle= new TStyle("ATLAS","Atlas style");

  // use plain black on white colors
  Int_t icol=0;
  atlasStyle->SetFrameBorderMode(icol);
  atlasStyle->SetCanvasBorderMode(icol);
  atlasStyle->SetPadBorderMode(icol);
  atlasStyle->SetPadColor(icol);
  atlasStyle->SetCanvasColor(icol);
  atlasStyle->SetStatColor(icol);
  //atlasStyle->SetFillColor(icol);

  // set the paper & margin sizes
  atlasStyle->SetPaperSize(20,26);
  atlasStyle->SetPadTopMargin(0.05);
  atlasStyle->SetPadRightMargin(0.05);
  atlasStyle->SetPadBottomMargin(0.16);
  atlasStyle->SetPadLeftMargin(0.12);

  // use large fonts
  //Int_t font=72;
  Int_t font=42;
  Double_t tsize=0.05;
  atlasStyle->SetTextFont(font);


  atlasStyle->SetTextSize(tsize);
  atlasStyle->SetLabelFont(font,"x");
  atlasStyle->SetTitleFont(font,"x");
  atlasStyle->SetLabelFont(font,"y");
  atlasStyle->SetTitleFont(font,"y");
  atlasStyle->SetLabelFont(font,"z");
  atlasStyle->SetTitleFont(font,"z");

  atlasStyle->SetLabelSize(tsize,"x");
  atlasStyle->SetTitleSize(tsize,"x");
  atlasStyle->SetLabelSize(tsize,"y");
  atlasStyle->SetTitleSize(tsize,"y");
  atlasStyle->SetLabelSize(tsize,"z");
  atlasStyle->SetTitleSize(tsize,"z");


  //use bold lines and markers
  if ( atlasmarker ) {
    //lasStyle->SetMarkerStyle(20);
    //lasStyle->SetMarkerSize(0.7);
    // atlasStyle->SetMarkerColor(kBlue);
  }
  atlasStyle->SetHistLineWidth(2.);
  atlasStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  //get rid of X error bars and y error bar caps
  //atlasStyle->SetErrorX(0.001);

  //do not display any of the standard histogram decorations
  atlasStyle->SetOptTitle(0);
  //atlasStyle->SetOptStat(1111);
  atlasStyle->SetOptStat(0);
  //atlasStyle->SetOptFit(1111);
  atlasStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  atlasStyle->SetPadTickX(1);
  atlasStyle->SetPadTickY(1);

  //gStyle->SetPadTickX(1);
  //gStyle->SetPadTickY(1);

  // DLA overrides
  atlasStyle->SetPadLeftMargin(left_margin);
  atlasStyle->SetPadBottomMargin(0.13);
  atlasStyle->SetTitleYOffset(ytoff);
  atlasStyle->SetTitleXOffset(1.0);

  // Enforce the style.
  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();
}
