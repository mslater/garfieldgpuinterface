#include <TROOT.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TAxis.h>
#include <TString.h>
#include <iostream>
#include <string> 

#include "Garfield/MediumMagboltz.hh"
#include "Garfield/FundamentalConstants.hh"
#include "TransportParameters.hh"
  
using namespace Garfield;

void rootlogonATLAS(double ytoff = 1.0, bool atlasmarker =true, double left_margin=0.14);

std::vector<double> GenerateEFieldPoints(double npoints, double EMax, double EMin); //Generate logarithmically spaced E-Field Points between EMin and EMax for use in plotting.

std::vector<double> GetTownsendVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType); //Use these E-Field Points to Get out values of Townsend Coefficient for desired gas properties

std::vector<double> GetAttachmentVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType); //Use these E-Field Points to Get out values of Attachment Coefficient for desired gas properties

std::vector<double> GetAttachmentVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType, double O2ppm); //Function for Oxygen attachment scaling

std::vector<double> GetDriftVelocityVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType); //Use these E-Field Points to Get out values of Drift Velocity for desired gas properties

std::vector<double> GetTransverseDiffusionVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType); //Use these E-Field Points to Get out values of Transverse Diffusion for desired gas properties

std::vector<double> GetLongitudinalDiffusionVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType); //Use these E-Field Points to Get out values of Longitudinal Diffusion for desired gas properties

void giveAxis(int graphType, double* lowerLim, double* upperLim, std::string* axisTitle)
{
  switch(graphType)
    {
    case 0://Townsend Coefficient
      (*upperLim)=1e3;
      (*lowerLim)=1e-4;
      (*axisTitle) = "Townsend Coefficient [1/cm]";      
      break;
	
    case 1:// Attachment Coefficient
      (*upperLim)=1e3;
      (*lowerLim)=1e-4;
      (*axisTitle) = "Attachment Coefficient [1/cm]";
      break;
	
    case 2://Drift Velocity
      (*upperLim)=25;
      (*lowerLim)=0;
      (*axisTitle) = "Drift Velocity [cm/#mus]";
      break;
	
    case 3://Transverse Diffusion
      (*upperLim)=0.8;
      (*lowerLim)=1e-2;
      (*axisTitle) = "Transverse Diffusion Coefficient [cm^{1/2}]";
      break;
	
    case 4://Longitudinal Diffusion
      (*upperLim)=0.8;
      (*lowerLim)=1e-2;
      (*axisTitle) = "Longitudinal Diffusion Coefficient [cm^{1/2}]";
      break;
	
    default:
      std::cout << "Invalid Plot Type!" << std::endl;
    }
}
//=================================MAIN CODE=================================//

int main(int argc, char *argv[])
{
  std::cout<< "WARNING :: In Transport Parameters i multiply Drift Velocity by 1e3, so that I get it in cm/mus, instead of the original cm/ns"<<std::endl;
  TApplication app("app", &argc, argv);
  
  rootlogonATLAS();
 
  // Set up TransportParameter Object
  std::vector<MediumMagboltz*> gas;
  std::vector<TransportParameters*> parameterTest;
  std::vector<std::pair<TString*,TString*> > listGases;
  /*
  listGases.push_back(std::make_pair( new TString("gasFile_760_100_Xe.gas"), new TString("Xe")));	
  listGases.push_back(std::make_pair( new TString("gasFile_760_100_CF4.gas"), new TString("CF_{4}")));	
  listGases.push_back(std::make_pair( new TString("gasFile_760_100_Ar.gas"), new TString("Ar")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_CH4.gas"), new TString("CH_{4}")));
  */
  //listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_98_CH4_imp_O2_20.gas"), new TString("Ar:CH_{4} (98%:2%) O_{2} 2ppm")));
  
  // Ar:CF4 mixtures
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_90_CF4.gas"), new TString("Ar:CF_{4} (90%:10%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_80_CF4.gas"), new TString("Ar:CF_{4} (80%:20%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_70_CF4.gas"), new TString("Ar:CF_{4} (70%:30%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_60_CF4.gas"), new TString("Ar:CF_{4} (60%:40%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_50_CF4.gas"), new TString("Ar:CF_{4} (50%:50%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_40_CF4.gas"), new TString("Ar:CF_{4} (40%:60%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_100_Ar_20_CF4.gas"), new TString("Ar:CF_{4} (20%:80%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_100_Ar_10_CF4.gas"), new TString("Ar:CF_{4} (10%:90%)")));
  
  // Ar:CH4 mixtures
  /*
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_98_CH4.gas"), new TString("Ar:CH_{4} (98%:2%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_95_CH4.gas"), new TString("Ar:CH_{4} (95%:5%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_90_CH4.gas"), new TString("Ar:CH_{4} (90%:10%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_80_CH4.gas"), new TString("Ar:CH_{4} (80%:20%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_70_CH4.gas"), new TString("Ar:CH_{4} (70%:30%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_60_CH4.gas"), new TString("Ar:CH_{4} (50%:40%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_50_CH4.gas"), new TString("Ar:CH_{4} (50%:50%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_40_CH4.gas"), new TString("Ar:CH_{4} (40%:60%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_30_CH4.gas"), new TString("Ar:CH_{4} (40%:60%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_20_CH4.gas"), new TString("Ar:CH_{4} (20%:80%)")));
  listGases.push_back(std::make_pair( new TString("gasFile_760_50_Ar_10_CH4.gas"), new TString("Ar:CH_{4} (10%:90%)")));
  */
  //listGases.push_back(std::make_pair( new TString("gasFile_760_100_Ar_98_CH4_2.gas"), new TString("Ar:CH_{4} (98%:2%)")));
  //listGases.push_back(std::make_pair( new TString("gasFile_760_100_Ar_95_CH4_5.gas"), new TString("Ar:CH_{4} (95%:5%)")));
  //listGases.push_back(std::make_pair( new TString("gasFile_760_100_Ar_90_CH4_10.gas"), new TString("Ar:CH_{4} (90%:10%)")));
  //listGases.push_back(std::make_pair( new TString("gasFile_760_100_Ar_80_CH4_20.gas"), new TString("Ar:CH_{4} (80%:20%)")));
  //listGases.push_back(std::make_pair( new TString("gasFile_760_100_Ar_70_CH4_30.gas"), new TString("Ar:CH_{4} (70%:30%)")));
  //listGases.push_back(std::make_pair( new TString("gasFile_760_100_Ar_60_CH4_40.gas"), new TString("Ar:CH_{4} (50%:40%)")));
  //listGases.push_back(std::make_pair( new TString("gasFile_760_100_Ar_50_CH4_50.gas"), new TString("Ar:CH_{4} (50%:50%)")));
  //listGases.push_back(std::make_pair( new TString("gasFile_760_100_Ar_40_CH4_60.gas"), new TString("Ar:CH_{4} (40%:60%)")));

  
  TString dir="./GasFiles/";
  for(unsigned int i=0;i<listGases.size();i++)
    {
      MediumMagboltz* pure = new MediumMagboltz();
      pure->LoadGasFile((dir+ *((listGases[i]).first)).Data());
      gas.push_back(pure);
      TString label(*((listGases[i]).second));
      TransportParameters* parameterTest0 = new TransportParameters(pure, NULL, NULL, label);
      parameterTest.push_back(parameterTest0);
    }
  //Set up EField points vector
  int nPoints = 200;
  double EMin = 0.01;
  double EMax = 1e5;
  
  std::vector<double> EFields = GenerateEFieldPoints(nPoints, EMax, EMin);
  
  // Get values of Townsend coefficient using TransportParameter Object, EFields and desired properties
  double pressureTorr= 760.;
  double pressureBar = pressureTorr/760.*1.01325;
  int impurityType = 0;
  double O2ppm = 0.;
  //std::vector<double> testVec = GetDriftVelocityVector(parameterTest[0], EFields, pressureBar, impurityType);
  //std::vector<double> o2attach = GetAttachmentVector(parameterTest, EFields, 3.1, 1, 0.1);

  std::vector<std::vector<double> > LongitudinalDiffusionVec;
  std::vector<std::vector<double> > TransverseDiffusionVec;
  std::vector<std::vector<double> > DriftVelocityVec;
  std::vector<std::vector<double> > AttachmentVec;
  std::vector<std::vector<double> > TownsendVec;
  for(unsigned i=0;i<parameterTest.size();i++)
    {
      LongitudinalDiffusionVec.push_back(GetLongitudinalDiffusionVector(parameterTest[i], EFields, pressureBar, 0));
      TransverseDiffusionVec.push_back(GetTransverseDiffusionVector(parameterTest[i], EFields, pressureBar, 0));
      DriftVelocityVec.push_back(GetDriftVelocityVector(parameterTest[i], EFields, pressureBar, 0));
      AttachmentVec.push_back(GetAttachmentVector(parameterTest[i], EFields, pressureBar, 0));
      TownsendVec.push_back(GetTownsendVector(parameterTest[i], EFields, pressureBar, 0));
    }

  TString headerTitle("");

// Make a graph showing the plot
  const int nColor = gROOT->GetStyle("ATLAS")->GetNumberOfColors();
  int palette[nColor];
  for(int j=0;j<nColor;j++)
    palette[j] = gROOT->GetStyle("ATLAS")->GetColorPalette(j);
  //std::cout << nColor<<std::endl;
  TCanvas* call=new TCanvas("call","",600, 800);
  call->Divide(2,3);
  call->GetPad(1)->SetLogx();
  call->GetPad(2)->SetLogx();
  call->GetPad(3)->SetLogx();
  call->GetPad(4)->SetLogx();
  call->GetPad(4)->SetLogy();
  call->GetPad(5)->SetLogx();
  call->GetPad(5)->SetLogy();
  
  {
  TCanvas* c1 = new TCanvas("c1", "", 600, 600);
  c1->SetLogx();//c1->SetLogy();

  int graphType = 4;
  double upperLim = 1;
  double lowerLim = 0;
  std::string axisTitle;
  std::string otherTitles("Test Curve; E-Field Strength [V/cm];");

  giveAxis(graphType,&lowerLim,&upperLim, &axisTitle);
  
  TString newaxisTitle=otherTitles + axisTitle;
    

  std::vector<TGraph*> testGraphLD;
  double graphMax = 0;
  double graphMin = 1e10;
  for(int i=0;i<LongitudinalDiffusionVec.size();i++)
    {
      TGraph*testGraph1 = new TGraph(EFields.size(), &(EFields[0]), &(LongitudinalDiffusionVec[i][0]));
      std::cout << i << " "<< i*1./LongitudinalDiffusionVec.size()*nColor<< " "<< int(i*1./LongitudinalDiffusionVec.size()*nColor)<< " " <<palette[int(i*1./LongitudinalDiffusionVec.size()*nColor)]<<std::endl;
      testGraph1->SetLineColor(palette[int(i*1./LongitudinalDiffusionVec.size()*nColor)]);
      testGraph1->SetLineWidth(2);
      testGraph1->SetMarkerStyle(24);
      testGraph1->SetMarkerSize(0.7);
      testGraph1->SetMarkerColor(kBlack);
      testGraphLD.push_back(testGraph1);
      for(int j=0;j<LongitudinalDiffusionVec[i].size();j++)
	{
	  double val = LongitudinalDiffusionVec[i][j];
	  if(graphMax<val)
	    graphMax=val;
	  if(graphMin>val)
	    graphMin=val;
	}
    }
  //std::cout<< graphMax<< " " <<graphMin<<std::endl;
  lowerLim=0;
  upperLim=(graphMax*1.1<0.4?0.4:0.4);

  TH2F*dummyTest=new TH2F("dummyTest",newaxisTitle,10000,EMin,EMax,1000,lowerLim,upperLim);
  dummyTest->GetXaxis()->SetTitleOffset(1.2);
  dummyTest->GetYaxis()->SetTitleOffset(1.4);
  dummyTest->Draw();

  TLegend* legend = new TLegend(0.55,0.45,0.85,0.85,"","brNDC");
  //legend->SetNDC();
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetTextSize(0.035);
  legend->SetFillStyle(1000);
  headerTitle=Form("Pressure %2.0f Torr", pressureTorr);
  legend->SetHeader(headerTitle);//"Pressure (bar)","C");
  for(int i=0;i<LongitudinalDiffusionVec.size();i++)
    {
      testGraphLD[i]->SetLineWidth(2);
      testGraphLD[i]->Draw("L");
      legend->AddEntry(testGraphLD[i], parameterTest[i]->GetLabel(),"l");
    }
  legend->Draw();  
  c1->Update();
  TString plotNameLD("plot_LongDiff");
  c1->Print(plotNameLD+".png");
  c1->Print(plotNameLD+".eps");
  c1->Print(plotNameLD+".pdf");
  c1->Print(plotNameLD+".C");
  call->cd(1);c1->DrawClonePad();
  }
  
  {
  TCanvas* c1 = new TCanvas("c2", "", 600, 600);
  c1->SetLogx();//c1->SetLogy();

  int graphType = 3;
  double upperLim = 1;
  double lowerLim = 0;
  std::string axisTitle;
  std::string otherTitles("Test Curve; E-Field Strength [V/cm];");

  giveAxis(graphType,&lowerLim,&upperLim, &axisTitle);
  
  TString newaxisTitle=otherTitles + axisTitle;
    

  std::vector<TGraph*> testGraph;
  double graphMax = 0;
  double graphMin = 1e10;
  for(int i=0;i<TransverseDiffusionVec.size();i++)
    {
      TGraph*testGraph1 = new TGraph(EFields.size(), &(EFields[0]), &(TransverseDiffusionVec[i][0]));
      testGraph1->SetLineColor(palette[int(i*1./TransverseDiffusionVec.size()*nColor)]);
      testGraph1->SetLineWidth(2);
      testGraph1->SetMarkerStyle(24);
      testGraph1->SetMarkerSize(0.7);
      testGraph1->SetMarkerColor(kBlack);
      testGraph.push_back(testGraph1);
      for(int j=0;j<TransverseDiffusionVec[i].size();j++)
	{
	  double val = TransverseDiffusionVec[i][j];
	  if(graphMax<val)
	    graphMax=val;
	  if(graphMin>val)
	    graphMin=val;
	}
    }
  //std::cout<< graphMax<< " " <<graphMin<<std::endl;
  lowerLim=0;
  upperLim=(graphMax*1.1<0.4?0.4:0.4);

  TH2F*dummyTest=new TH2F("dummyTest",newaxisTitle,10000,EMin,EMax,1000,lowerLim,upperLim);
  dummyTest->GetXaxis()->SetTitleOffset(1.2);
  dummyTest->GetYaxis()->SetTitleOffset(1.4);
  dummyTest->Draw();

  TLegend* legend = new TLegend(0.55,0.50,0.85,0.90,"","brNDC");
  //legend->SetNDC();
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetTextSize(0.035);
  legend->SetFillStyle(1000);
  headerTitle=Form("Pressure %2.0f Torr", pressureTorr);
  legend->SetHeader(headerTitle);//"Pressure (bar)","C");
  for(int i=0;i<TransverseDiffusionVec.size();i++)
    {
      testGraph[i]->SetLineWidth(2);
      testGraph[i]->Draw("L");
      legend->AddEntry(testGraph[i], parameterTest[i]->GetLabel(),"l");
    }
  legend->Draw();  
  c1->Update();
  TString plotName("plot_TransDiff");
  c1->Print(plotName+".png");
  c1->Print(plotName+".eps");
  c1->Print(plotName+".pdf");
  c1->Print(plotName+".C");
  call->cd(2);c1->DrawClonePad();
    
    }

    {
  TCanvas* c1 = new TCanvas("c3", "", 600, 600);
  c1->SetLogx();//c1->SetLogy();

  int graphType = 2;
  double upperLim = 1;
  double lowerLim = 0;
  std::string axisTitle;
  std::string otherTitles("Test Curve; E-Field Strength [V/cm];");

  giveAxis(graphType,&lowerLim,&upperLim, &axisTitle);
  
  TString newaxisTitle=otherTitles + axisTitle;
    

  std::vector<TGraph*> testGraph;
  double graphMax = 0;
  double graphMin = 1e10;
  for(int i=0;i<DriftVelocityVec.size();i++)
    {
      TGraph*testGraph1 = new TGraph(EFields.size(), &(EFields[0]), &(DriftVelocityVec[i][0]));
      testGraph1->SetLineColor(palette[int(i*1./DriftVelocityVec.size()*nColor)]);
      testGraph1->SetLineWidth(2);
      testGraph1->SetMarkerStyle(24);
      testGraph1->SetMarkerSize(0.7);
      testGraph1->SetMarkerColor(kBlack);
      testGraph.push_back(testGraph1);
      for(int j=0;j<DriftVelocityVec[i].size();j++)
	{
	  double val = DriftVelocityVec[i][j];
	  if(graphMax<val)
	    graphMax=val;
	  if(graphMin>val)
	    graphMin=val;
	}
    }
  //std::cout<< graphMax<< " " <<graphMin<<std::endl;
  lowerLim=0;
  upperLim=graphMax*1.1;

  TH2F*dummyTest=new TH2F("dummyTest",newaxisTitle,10000,EMin,EMax,1000,lowerLim,upperLim);
  dummyTest->GetXaxis()->SetTitleOffset(1.2);
  dummyTest->GetYaxis()->SetTitleOffset(1.4);
  dummyTest->Draw();

  TLegend* legend = new TLegend(0.25,0.52,0.55,0.92,"","brNDC");
  //legend->SetNDC();
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetTextSize(0.035);
  legend->SetFillStyle(1000);
  headerTitle=Form("Pressure %2.0f Torr", pressureTorr);
  legend->SetHeader(headerTitle);//"Pressure (bar)","C");
  for(int i=0;i<DriftVelocityVec.size();i++)
    {
      testGraph[i]->SetLineWidth(2);
      testGraph[i]->Draw("L");
      legend->AddEntry(testGraph[i], parameterTest[i]->GetLabel(),"l");
    }
  legend->Draw();  
  c1->Update();
  TString plotName("plot_DriftVelocity");
  c1->Print(plotName+".png");
  c1->Print(plotName+".eps");
  c1->Print(plotName+".pdf");
  c1->Print(plotName+".C");
  call->cd(3);c1->DrawClonePad();
    }

    {
  TCanvas* c1 = new TCanvas("c4", "", 600, 600);
  c1->SetLogx();c1->SetLogy();

  int graphType = 1;
  double upperLim = 1;
  double lowerLim = 0;
  std::string axisTitle;
  std::string otherTitles("Test Curve; E-Field Strength [V/cm];");
  giveAxis(graphType,&lowerLim,&upperLim, &axisTitle);

  TString newaxisTitle=otherTitles + axisTitle;
   
  std::vector<TGraph*> testGraph;
  double graphMax = 0;
  double graphMin = 1e10;
  for(int i=0;i<DriftVelocityVec.size();i++)
    {
      TGraph*testGraph1 = new TGraph(EFields.size(), &(EFields[0]), &(AttachmentVec[i][0]));
      testGraph1->SetLineColor(palette[int(i*1./AttachmentVec.size()*nColor)]);
      testGraph1->SetLineWidth(2);
      testGraph1->SetMarkerStyle(24);
      testGraph1->SetMarkerSize(0.7);
      testGraph1->SetMarkerColor(kBlack);
      testGraph.push_back(testGraph1);
      for(int j=0;j<AttachmentVec[i].size();j++)
	{
	  double val = AttachmentVec[i][j];
	  if(graphMax<val)
	    graphMax=val;
	  if(graphMin>val)
	    graphMin=val;
	}
    }
  //std::cout<< graphMax<< " " <<graphMin<<std::endl;
  //lowerLim=0.01;
  //upperLim=graphMax*1.1;

  TH2F*dummyTest=new TH2F("dummyTest",newaxisTitle,10000,EMin,EMax,1000,lowerLim,upperLim);
  dummyTest->GetXaxis()->SetTitleOffset(1.2);
  dummyTest->GetYaxis()->SetTitleOffset(1.4);
  dummyTest->Draw();

  TLegend* legend = new TLegend(0.20,0.50,0.50,0.90,"","brNDC");
  //legend->SetNDC();
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetTextSize(0.035);
  legend->SetFillStyle(1000);
  headerTitle=Form("Pressure %2.0f Torr", pressureTorr);
  legend->SetHeader(headerTitle);//"Pressure (bar)","C");
  for(int i=0;i<AttachmentVec.size();i++)
    {
      testGraph[i]->SetLineWidth(2);
      testGraph[i]->Draw("L");
      legend->AddEntry(testGraph[i], parameterTest[i]->GetLabel(),"l");
    }
  legend->Draw();  
  c1->Update();
  TString plotName("plot_Attachment");
  c1->Print(plotName+".png");
  c1->Print(plotName+".eps");
  c1->Print(plotName+".pdf");
  c1->Print(plotName+".C");
  call->cd(4);c1->DrawClonePad();
    }

        {
  TCanvas* c1 = new TCanvas("c5", "", 600, 600);
  c1->SetLogx();c1->SetLogy();

  int graphType = 0;
  double upperLim = 1;
  double lowerLim = 0;
  std::string axisTitle;
  std::string otherTitles("Test Curve; E-Field Strength [V/cm];");
  giveAxis(graphType,&lowerLim,&upperLim, &axisTitle);
  
  TString newaxisTitle=otherTitles + axisTitle;
    
  std::vector<TGraph*> testGraph;
  double graphMax = 0;
  double graphMin = 1e10;
  for(int i=0;i<DriftVelocityVec.size();i++)
    {
      TGraph*testGraph1 = new TGraph(EFields.size(), &(EFields[0]), &(TownsendVec[i][0]));
      testGraph1->SetLineColor(palette[int(i*1./TownsendVec.size()*nColor)]);
      testGraph1->SetLineWidth(2);
      testGraph1->SetMarkerStyle(24);
      testGraph1->SetMarkerSize(0.7);
      testGraph1->SetMarkerColor(kBlack);
      testGraph.push_back(testGraph1);
      for(int j=0;j<TownsendVec[i].size();j++)
	{
	  double val = TownsendVec[i][j];
	  if(graphMax<val)
	    graphMax=val;
	  if(graphMin>val)
	    graphMin=val;
	}
    }
  //std::cout<< graphMax<< " " <<graphMin<<std::endl;
  //lowerLim=0;
  //upperLim=graphMax*1.1;

  TH2F*dummyTest=new TH2F("dummyTest",newaxisTitle,10000,EMin,EMax,1000,lowerLim,upperLim);
  dummyTest->GetXaxis()->SetTitleOffset(1.2);
  dummyTest->GetYaxis()->SetTitleOffset(1.4);
  dummyTest->Draw();

  TLegend* legend = new TLegend(0.20,0.50,0.50,0.90,"","brNDC");
  //legend->SetNDC();
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetTextSize(0.035);
  legend->SetFillStyle(1000);
  headerTitle=Form("Pressure %2.0f Torr", pressureTorr);
  legend->SetHeader(headerTitle);//"Pressure (bar)","C");
  for(int i=0;i<TownsendVec.size();i++)
    {
      testGraph[i]->SetLineWidth(2);
      testGraph[i]->Draw("L");
      legend->AddEntry(testGraph[i], parameterTest[i]->GetLabel(),"l");
    }
  legend->Draw();  
  c1->Update();
  TString plotName("plot_Townsend");
  c1->Print(plotName+".png");
  c1->Print(plotName+".eps");
  c1->Print(plotName+".pdf");
  c1->Print(plotName+".C");
  call->cd(5);c1->DrawClonePad();
    }
	call->Print("plot_all.png");
	call->Print("plot_all.eps");
	call->Print("plot_all.pdf");
	call->Print("plot_all.C");
	return 0;
	/// Plot different pressures
	int choice=0;
	for(auto g: listGases)
	  {
	    if(g.first->Contains("gasFile_760_50_Xe.gas"))
	      break;
	    choice++;
	  }
	const int gasChoice=choice;
	std::vector<TString*> listGasesLabelsPressures;
  listGasesLabelsPressures.push_back(new TString("50 Torr"));
  listGasesLabelsPressures.push_back(new TString("100 Torr"));
  listGasesLabelsPressures.push_back(new TString("200 Torr"));
  listGasesLabelsPressures.push_back(new TString("300 Torr"));
  listGasesLabelsPressures.push_back(new TString("400 Torr"));
  listGasesLabelsPressures.push_back(new TString("500 Torr"));
  //  listGasesLabelsPressures.push_back(new TString("600 Torr"));
  //listGasesLabelsPressures.push_back(new TString("700 Torr"));
  //listGasesLabelsPressures.push_back(new TString("760 Torr"));
  std::vector<double> listGasPressures;
  listGasPressures.push_back(50);
  listGasPressures.push_back(100);
  listGasPressures.push_back(200);
  listGasPressures.push_back(300);
  listGasPressures.push_back(400);
  listGasPressures.push_back(500);
  // listGasPressures.push_back(600);
  // listGasPressures.push_back(700);
  // listGasPressures.push_back(760);
  std::vector<std::vector<double> > LongitudinalDiffusionVecPressures;
  std::vector<std::vector<double> > TransverseDiffusionVecPressures;
  std::vector<std::vector<double> > DriftVelocityVecPressures;
  std::vector<std::vector<double> > AttachmentVecPressures;
  std::vector<std::vector<double> > TownsendVecPressures;
  std::cerr << " here "<<std::endl;
  for(unsigned i=0;i<listGasPressures.size();i++)
    {
      LongitudinalDiffusionVecPressures.push_back(GetLongitudinalDiffusionVector(parameterTest[gasChoice], EFields, listGasPressures[i]/760.*1.01325, 0));
      TransverseDiffusionVecPressures.push_back(GetTransverseDiffusionVector(parameterTest[gasChoice], EFields, listGasPressures[i]/760.*1.01325, 0));
      DriftVelocityVecPressures.push_back(GetDriftVelocityVector(parameterTest[gasChoice], EFields, listGasPressures[i]/760.*1.01325, 0));
      AttachmentVecPressures.push_back(GetAttachmentVector(parameterTest[gasChoice], EFields, listGasPressures[i]/760.*1.01325, 0));
      TownsendVecPressures.push_back(GetTownsendVector(parameterTest[gasChoice], EFields, listGasPressures[i]/760.*1.01325, 0));
      //for(unsigned j=0;j<TownsendVecPressures[i-1].size();j++);
	//std::cout<< (TownsendVecPressures[i-1]).size()<< " " << EFields[j]<<std::endl;
    }

    {
  TCanvas* cc1 = new TCanvas("cc1", "", 600, 600);
  cc1->SetLogx();//c1->SetLogy();

  int graphType = 4;
  double upperLim = 1;
  double lowerLim = 0;
  std::string axisTitle;
  std::string otherTitles("Test Curve; E-Field Strength [V/cm];");

  giveAxis(graphType,&lowerLim,&upperLim, &axisTitle);
  
  TString newaxisTitle=otherTitles + axisTitle;
    

  std::vector<TGraph*> testGraphLD;
  double graphMax = 0;
  double graphMin = 1e10;
  for(int i=0;i<LongitudinalDiffusionVecPressures.size();i++)
    {
      TGraph*testGraph1 = new TGraph(EFields.size(), &(EFields[0]), &(LongitudinalDiffusionVecPressures[i][0]));
      std::cout << i << " "<< i*1./LongitudinalDiffusionVecPressures.size()*nColor<< " "<< int(i*1./LongitudinalDiffusionVecPressures.size()*nColor)<< " " <<palette[int(i*1./LongitudinalDiffusionVecPressures.size()*nColor)]<<std::endl;
      testGraph1->SetLineColor(palette[int(i*1./LongitudinalDiffusionVecPressures.size()*nColor)]);
      testGraph1->SetLineWidth(2);
      testGraph1->SetMarkerStyle(24);
      testGraph1->SetMarkerSize(0.7);
      testGraph1->SetMarkerColor(kBlack);
      testGraphLD.push_back(testGraph1);
      for(int j=0;j<LongitudinalDiffusionVecPressures[i].size();j++)
	{
	  double val = LongitudinalDiffusionVecPressures[i][j];
	  if(graphMax<val)
	    graphMax=val;
	  if(graphMin>val)
	    graphMin=val;
	}
    }
  //std::cout<< graphMax<< " " <<graphMin<<std::endl;
  lowerLim=0;
  upperLim=(graphMax*1.1<0.4?0.4:0.4);

  TH2F*dummyTest=new TH2F("dummyTest",newaxisTitle,10000,EMin,EMax,1000,lowerLim,upperLim);
  dummyTest->GetXaxis()->SetTitleOffset(1.2);
  dummyTest->GetYaxis()->SetTitleOffset(1.4);
  dummyTest->Draw();

  TLegend* legend = new TLegend(0.55,0.45,0.85,0.85,"","brNDC");
  //legend->SetNDC();
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetTextSize(0.035);
  legend->SetFillStyle(1000);
  legend->SetHeader(*((listGases[gasChoice]).second));
  for(int i=0;i<LongitudinalDiffusionVecPressures.size();i++)
    {
      testGraphLD[i]->SetLineWidth(2);
      testGraphLD[i]->Draw("L");
      legend->AddEntry(testGraphLD[i], *listGasesLabelsPressures[i],"l");
    }
  legend->Draw();  
  cc1->Update();
  TString plotNameLD("plot_LongDiff_pressures");
  cc1->Print(plotNameLD+".png");
  cc1->Print(plotNameLD+".eps");
  cc1->Print(plotNameLD+".pdf");
  cc1->Print(plotNameLD+".C");
  }
  
  {
  TCanvas* c1 = new TCanvas("cc2", "", 600, 600);
  c1->SetLogx();//c1->SetLogy();

  int graphType = 3;
  double upperLim = 1;
  double lowerLim = 0;
  std::string axisTitle;
  std::string otherTitles("Test Curve; E-Field Strength [V/cm];");

  giveAxis(graphType,&lowerLim,&upperLim, &axisTitle);
  
  TString newaxisTitle=otherTitles + axisTitle;
    

  std::vector<TGraph*> testGraph;
  double graphMax = 0;
  double graphMin = 1e10;
  for(int i=0;i<TransverseDiffusionVecPressures.size();i++)
    {
      TGraph*testGraph1 = new TGraph(EFields.size(), &(EFields[0]), &(TransverseDiffusionVecPressures[i][0]));
      testGraph1->SetLineColor(palette[int(i*1./TransverseDiffusionVecPressures.size()*nColor)]);
      testGraph1->SetLineWidth(2);
      testGraph1->SetMarkerStyle(24);
      testGraph1->SetMarkerSize(0.7);
      testGraph1->SetMarkerColor(kBlack);
      testGraph.push_back(testGraph1);
      for(int j=0;j<TransverseDiffusionVecPressures[i].size();j++)
	{
	  double val = TransverseDiffusionVecPressures[i][j];
	  if(graphMax<val)
	    graphMax=val;
	  if(graphMin>val)
	    graphMin=val;
	}
    }
  //std::cout<< graphMax<< " " <<graphMin<<std::endl;
  lowerLim=0;
  upperLim=(graphMax*1.1<0.4?0.4:0.4);

  TH2F*dummyTest=new TH2F("dummyTest",newaxisTitle,10000,EMin,EMax,1000,lowerLim,upperLim);
  dummyTest->GetXaxis()->SetTitleOffset(1.2);
  dummyTest->GetYaxis()->SetTitleOffset(1.4);
  dummyTest->Draw();

  TLegend* legend = new TLegend(0.55,0.50,0.85,0.90,"","brNDC");
  //legend->SetNDC();
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetTextSize(0.035);
  legend->SetFillStyle(1000);
  legend->SetHeader(*((listGases[gasChoice]).second));
  for(int i=0;i<TransverseDiffusionVecPressures.size();i++)
    {
      testGraph[i]->SetLineWidth(2);
      testGraph[i]->Draw("L");
      legend->AddEntry(testGraph[i], *listGasesLabelsPressures[i],"l");
    }
  legend->Draw();  
  c1->Update();
  TString plotName("plot_TransDiff_pressures");
  c1->Print(plotName+".png");
  c1->Print(plotName+".eps");
  c1->Print(plotName+".pdf");
  c1->Print(plotName+".C");
    }

    {
  TCanvas* c1 = new TCanvas("cc3", "", 600, 600);
  c1->SetLogx();//c1->SetLogy();

  int graphType = 2;
  double upperLim = 1;
  double lowerLim = 0;
  std::string axisTitle;
  std::string otherTitles("Test Curve; E-Field Strength [V/cm];");

  giveAxis(graphType,&lowerLim,&upperLim, &axisTitle);
  
  TString newaxisTitle=otherTitles + axisTitle;
    

  std::vector<TGraph*> testGraph;
  double graphMax = 0;
  double graphMin = 1e10;
  for(int i=0;i<DriftVelocityVecPressures.size();i++)
    {
      TGraph*testGraph1 = new TGraph(EFields.size(), &(EFields[0]), &(DriftVelocityVecPressures[i][0]));
      testGraph1->SetLineColor(palette[int(i*1./DriftVelocityVecPressures.size()*nColor)]);
      testGraph1->SetLineWidth(2);
      testGraph1->SetMarkerStyle(24);
      testGraph1->SetMarkerSize(0.7);
      testGraph1->SetMarkerColor(kBlack);
      testGraph.push_back(testGraph1);
      for(int j=0;j<DriftVelocityVecPressures[i].size();j++)
	{
	  double val = DriftVelocityVecPressures[i][j];
	  if(graphMax<val)
	    graphMax=val;
	  if(graphMin>val)
	    graphMin=val;
	}
    }
  //std::cout<< graphMax<< " " <<graphMin<<std::endl;
  //lowerLim=0;
  //upperLim=graphMax*1.1;

  TH2F*dummyTest=new TH2F("dummyTest",newaxisTitle,10000,EMin,EMax,1000,lowerLim,upperLim);
  dummyTest->GetXaxis()->SetTitleOffset(1.2);
  dummyTest->GetYaxis()->SetTitleOffset(1.4);
  dummyTest->Draw();

  TLegend* legend = new TLegend(0.25,0.52,0.55,0.92,"","brNDC");
  //legend->SetNDC();
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetTextSize(0.035);
  legend->SetFillStyle(1000);
  legend->SetHeader(*((listGases[gasChoice]).second));
  for(int i=0;i<DriftVelocityVecPressures.size();i++)
    {
      testGraph[i]->SetLineWidth(2);
      testGraph[i]->Draw("L");
      legend->AddEntry(testGraph[i], *listGasesLabelsPressures[i],"l");
    }
  legend->Draw();  
  c1->Update();
  TString plotName("plot_DriftVelocity_pressures");
  c1->Print(plotName+".png");
  c1->Print(plotName+".eps");
  c1->Print(plotName+".pdf");
  c1->Print(plotName+".C");
    }

    {
  TCanvas* c1 = new TCanvas("cc4", "", 600, 600);
  c1->SetLogx();c1->SetLogy();

  int graphType = 1;
  double upperLim = 1;
  double lowerLim = 0;
  std::string axisTitle;
  std::string otherTitles("Test Curve; E-Field Strength [V/cm];");

  giveAxis(graphType,&lowerLim,&upperLim, &axisTitle);
  
  TString newaxisTitle=otherTitles + axisTitle;
    

  std::vector<TGraph*> testGraph;
  double graphMax = 0;
  double graphMin = 1e10;
  for(int i=0;i<DriftVelocityVecPressures.size();i++)
    {
      TGraph*testGraph1 = new TGraph(EFields.size(), &(EFields[0]), &(AttachmentVecPressures[i][0]));
      testGraph1->SetLineColor(palette[int(i*1./AttachmentVecPressures.size()*nColor)]);
      testGraph1->SetLineWidth(2);
      testGraph1->SetMarkerStyle(24);
      testGraph1->SetMarkerSize(0.7);
      testGraph1->SetMarkerColor(kBlack);
      testGraph.push_back(testGraph1);
      for(int j=0;j<AttachmentVecPressures[i].size();j++)
	{
	  double val = AttachmentVecPressures[i][j];
	  if(graphMax<val)
	    graphMax=val;
	  if(graphMin>val)
	    graphMin=val;
	}
    }
  //std::cout<< graphMax<< " " <<graphMin<<std::endl;
  //lowerLim=0;
  //upperLim=graphMax*1.1;

  TH2F*dummyTest=new TH2F("dummyTest",newaxisTitle,10000,EMin,EMax,1000,lowerLim,upperLim);
  dummyTest->GetXaxis()->SetTitleOffset(1.2);
  dummyTest->GetYaxis()->SetTitleOffset(1.4);
  dummyTest->Draw();

  TLegend* legend = new TLegend(0.15,0.55,0.45,0.95,"","brNDC");
  //legend->SetNDC();
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetTextSize(0.035);
  legend->SetFillStyle(1000);
  legend->SetHeader(*((listGases[gasChoice]).second));
  for(int i=0;i<AttachmentVecPressures.size();i++)
    {
      testGraph[i]->SetLineWidth(2);
      testGraph[i]->Draw("L");
      legend->AddEntry(testGraph[i], *listGasesLabelsPressures[i],"l");
    }
  legend->Draw();  
  c1->Update();
  TString plotName("plot_Attachment_pressures");
  c1->Print(plotName+".png");
  c1->Print(plotName+".eps");
  c1->Print(plotName+".pdf");
  c1->Print(plotName+".C");
    }

        {
  TCanvas* c1 = new TCanvas("cc5", "", 600, 600);
  c1->SetLogx();c1->SetLogy();

  int graphType = 0;
  double upperLim = 1;
  double lowerLim = 0;
  std::string axisTitle;
  std::string otherTitles("Test Curve; E-Field Strength [V/cm];");

  giveAxis(graphType,&lowerLim,&upperLim, &axisTitle);
  
  TString newaxisTitle=otherTitles + axisTitle;
    

  std::vector<TGraph*> testGraph;
  double graphMax = 0;
  double graphMin = 1e10;
  for(int i=0;i<DriftVelocityVecPressures.size();i++)
    {
      TGraph*testGraph1 = new TGraph(EFields.size(), &(EFields[0]), &(TownsendVecPressures[i][0]));
      testGraph1->SetLineColor(palette[int(i*1./TownsendVecPressures.size()*nColor)]);
      testGraph1->SetLineWidth(2);
      testGraph1->SetMarkerStyle(24);
      testGraph1->SetMarkerSize(0.7);
      testGraph1->SetMarkerColor(kBlack);
      testGraph.push_back(testGraph1);
      for(int j=0;j<TownsendVecPressures[i].size();j++)
	{
	  double val = TownsendVecPressures[i][j];
	  if(graphMax<val)
	    graphMax=val;
	  if(graphMin>val)
	    graphMin=val;
	}
    }
  //std::cout<< graphMax<< " " <<graphMin<<std::endl;
  //lowerLim=0;
  //upperLim=graphMax*1.1;

  TH2F*dummyTest=new TH2F("dummyTest",newaxisTitle,10000,EMin,EMax,1000,lowerLim,upperLim);
  dummyTest->GetXaxis()->SetTitleOffset(1.2);
  dummyTest->GetYaxis()->SetTitleOffset(1.4);
  dummyTest->Draw();

  TLegend* legend = new TLegend(0.20,0.50,0.50,0.90,"","brNDC");
  //legend->SetNDC();
  legend->SetTextFont(42);
  legend->SetBorderSize(0);
  legend->SetFillColor(0);
  legend->SetTextSize(0.035);
  legend->SetFillStyle(1000);
  legend->SetHeader(*((listGases[gasChoice]).second));
  for(int i=0;i<TownsendVecPressures.size();i++)
    {
      testGraph[i]->SetLineWidth(2);
      testGraph[i]->Draw("L");
      legend->AddEntry(testGraph[i], *listGasesLabelsPressures[i],"l");
    }
  legend->Draw();  
  c1->Update();
  TString plotName("plot_Townsend_pressures");
  c1->Print(plotName+".png");
  c1->Print(plotName+".eps");
  c1->Print(plotName+".pdf");
  c1->Print(plotName+".C");
    }

	
  std::cout<< "WARNING :: In Transport Parameters i multiply Drift Velocity by 1e3, so that I get it in cm/mus, instead of the original cm/ns"<<std::endl;
  app.Run(); // Keeps up canvas
  return 0;
} 




//=================================FUNCTIONS=================================//


std::vector<double> GenerateEFieldPoints(double npoints, double EMax, double EMin)
{
  double EField=0;
  
  double LogMin = log(EMin);
  double LogMax = log(EMax);
  double step = (LogMax - LogMin)/(npoints-1);
  
  std::vector<double> EFieldPoints;
  
  for (double logEField = LogMin; logEField<LogMax+step/2; logEField+=step)
    {
      EField = exp(logEField);
      EFieldPoints.push_back(EField);
    }
  
  return EFieldPoints;
}

std::vector<double> GetTownsendVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType)
{
  std::vector<double> alphaVec;
  double alpha;
  
  for(unsigned int iPoint = 0; iPoint < EFields.size(); iPoint++){
    alpha = gas->GetElectronTownsend(EFields[iPoint], pressureBar, impurityType);
    alphaVec.push_back(alpha);
  }
  
  return alphaVec;
}

std::vector<double> GetAttachmentVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType)
{
  std::vector<double> etaVec;
  double eta;
  
  for(unsigned int iPoint = 0; iPoint < EFields.size(); iPoint++){
    eta = gas->GetElectronAttachment(EFields[iPoint], pressureBar, impurityType);
    etaVec.push_back(eta);
  }
  
  return etaVec;  
}

std::vector<double> GetAttachmentVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType, double O2ppm)
{
  std::vector<double> etaVec;
  double eta;
  
  if(impurityType == 1)
    for(unsigned int iPoint = 0; iPoint < EFields.size(); iPoint++)
      {
	eta = gas->GetElectronAttachment(EFields[iPoint], pressureBar, impurityType, O2ppm);
	etaVec.push_back(eta);
      }
  else
    {
      std::cout << "[transportPlots] ERROR: Entered a value for O2 contamination for impurity type != 1 \n"
		<< "Perhaps you meant to use GetAttachmentVector(TransportParameters*, std::vector<double>, double, int, double)?" << std::endl;
    }
  
  
  return etaVec;  
}

std::vector<double> GetDriftVelocityVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType)
{
  std::vector<double> driftvVec;
  double driftv;
  
  for(unsigned int iPoint = 0; iPoint < EFields.size(); iPoint++)
    {
      driftv = gas->GetElectronDriftVelocity(EFields[iPoint], pressureBar, impurityType);
      driftvVec.push_back(driftv);
    }

  return driftvVec;
}

std::vector<double> GetTransverseDiffusionVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType)
{
  std::vector<double> transDiffVec;
  double transDiff;

  for(unsigned int iPoint = 0; iPoint < EFields.size(); iPoint++)
    {
      transDiff = gas->GetElectronTransverseDiffusion(EFields[iPoint], pressureBar, impurityType);
      transDiffVec.push_back(transDiff);
    }
  
  return transDiffVec;
}


std::vector<double> GetLongitudinalDiffusionVector(TransportParameters* gas, std::vector<double> EFields, double pressureBar, int impurityType)
{
  std::vector<double> longDiffVec;
  double longDiff;
  
  for(unsigned int iPoint = 0; iPoint < EFields.size(); iPoint++)
    {
      longDiff = gas->GetElectronLongitudinalDiffusion(EFields[iPoint], pressureBar, impurityType);
      longDiffVec.push_back(longDiff);
    }
  
  return longDiffVec;
}




void rootlogonATLAS(double ytoff, bool atlasmarker, double left_margin) 
{
  TStyle* atlasStyle= new TStyle("ATLAS","Atlas style");

  // use plain black on white colors
  Int_t icol=0;
  atlasStyle->SetFrameBorderMode(icol);
  atlasStyle->SetCanvasBorderMode(icol);
  atlasStyle->SetPadBorderMode(icol);
  atlasStyle->SetPadColor(icol);
  atlasStyle->SetCanvasColor(icol);
  atlasStyle->SetStatColor(icol);
  //atlasStyle->SetFillColor(icol);

  // set the paper & margin sizes
  atlasStyle->SetPaperSize(20,26);
  atlasStyle->SetPadTopMargin(0.05);
  atlasStyle->SetPadRightMargin(0.05);
  atlasStyle->SetPadBottomMargin(0.16);
  atlasStyle->SetPadLeftMargin(0.12);

  // use large fonts
  //Int_t font=72;
  Int_t font=42;
  Double_t tsize=0.05;
  atlasStyle->SetTextFont(font);


  atlasStyle->SetTextSize(tsize);
  atlasStyle->SetLabelFont(font,"x");
  atlasStyle->SetTitleFont(font,"x");
  atlasStyle->SetLabelFont(font,"y");
  atlasStyle->SetTitleFont(font,"y");
  atlasStyle->SetLabelFont(font,"z");
  atlasStyle->SetTitleFont(font,"z");

  atlasStyle->SetLabelSize(tsize,"x");
  atlasStyle->SetTitleSize(tsize,"x");
  atlasStyle->SetLabelSize(tsize,"y");
  atlasStyle->SetTitleSize(tsize,"y");
  atlasStyle->SetLabelSize(tsize,"z");
  atlasStyle->SetTitleSize(tsize,"z");


  //use bold lines and markers
  if ( atlasmarker ) {
    //lasStyle->SetMarkerStyle(20);
    //lasStyle->SetMarkerSize(0.7);
    // atlasStyle->SetMarkerColor(kBlue);
  }
  atlasStyle->SetHistLineWidth(2.);
  atlasStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  //get rid of X error bars and y error bar caps
  //atlasStyle->SetErrorX(0.001);

  //do not display any of the standard histogram decorations
  atlasStyle->SetOptTitle(0);
  //atlasStyle->SetOptStat(1111);
  atlasStyle->SetOptStat(0);
  //atlasStyle->SetOptFit(1111);
  atlasStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  atlasStyle->SetPadTickX(1);
  atlasStyle->SetPadTickY(1);

  //gStyle->SetPadTickX(1);
  //gStyle->SetPadTickY(1);

  // DLA overrides
  atlasStyle->SetPadLeftMargin(left_margin);
  atlasStyle->SetPadBottomMargin(0.13);
  atlasStyle->SetTitleYOffset(ytoff);
  atlasStyle->SetTitleXOffset(1.0);

  const Int_t NRGBs = 5;
  const int NCont = 255;
 
  double stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
  double red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
  double green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
  double blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };

  int FI = TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  int MyPalette[NCont];
  for (int i=0;i<NCont;i++)
    MyPalette[i] = FI+i;
  
  atlasStyle->SetNumberContours(NCont);
  atlasStyle->SetPalette(NCont,MyPalette);

  // Enforce the style.
  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();
}
