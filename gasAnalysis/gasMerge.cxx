#include <iostream>
#include <fstream>
#include <string>

#include <cxxopts.hpp>

#include "Garfield/MediumMagboltz.hh"

using namespace Garfield;
int main(int argc, char **argv)
{

  cxxopts::Options options("simulation", "Simulation for as-yet-untitled Migdal experiment");
  // bools have an implicit value of true anyway but we'll include them here for clarity
  options.add_options()
    ("b,batch", "Run in batch mode", cxxopts::value<bool>()->implicit_value("true")->default_value("false"))
    ("n", "Number of events", cxxopts::value<int>()->default_value("1"))
    ("drift-ions", "Whether to drift ions or not", cxxopts::value<bool>()->implicit_value("true")->default_value("false"))
    ("plane-offset", "Offset of plane", cxxopts::value<float>()->default_value("0"))
    ("debug", "Debug", cxxopts::value<bool>()->default_value("false"))
    ("gas-temperature", "Gas Temperatures", cxxopts::value<double>()->default_value("293.15"))
    ("gas-pressure", "Gas Pressure", cxxopts::value<double>()->default_value("760"))
    ("gas-file", "Gas File", cxxopts::value<std::string>()->default_value("gasFile_100_0_Ar_10_CF4.gas"))
    ("h,help", "Print usage")
  ;
  auto cmdline_args = options.parse(argc, argv);

  if (cmdline_args.count("help")) {
     std::cout << options.help() << std::endl;
     return 1;
  }

  // ./simulation nEvents bBatch bDriftIons planeOffse
  int nEvents = cmdline_args["n"].as<int>();
  bool bBatch = cmdline_args["batch"].as<bool>();
  bool bDriftIons = cmdline_args["drift-ions"].as<bool>();
  float planeOffset  = cmdline_args["plane-offset"].as<float>();

  std::string m_gasFile  =cmdline_args["gas-file"].as<std::string>();
  double m_gasPressure   =cmdline_args["gas-pressure"].as<double>();
  double m_gasTemperature=cmdline_args["gas-temperature"].as<double>();

  const bool debug = cmdline_args["debug"].as<bool>();

  std::vector<std::string> listOfFiles;

  // impurities
  //listOfFiles.push_back("gasFile_760_Ar_98_CH4_1_imp_O2_50_100");
  //listOfFiles.push_back("gasFile_760_Ar_98_CH4_1_imp_O2_50_50");
  //listOfFiles.push_back("gasFile_760_Ar_98_CH4_1_imp_O2_20_50");
  //listOfFiles.push_back("gasFile_760_Ar_98_CH4_1_imp_O2_10_50");
  //listOfFiles.push_back("gasFile_760_Ar_98_CH4_1_imp_O2_5_50");
  /*
  listOfFiles.push_back("gasFile_760_Ar_98_CH4_1_imp_O2_20_500");
  listOfFiles.push_back("gasFile_760_Ar_98_CH4_1_imp_O2_10_500");
  listOfFiles.push_back("gasFile_760_Ar_98_CH4_1_imp_O2_5_500");
  */
  //listOfFiles.push_back("gasFile_760_Ar_98_CH4_1_imp_O2_1_50");
  //listOfFiles.push_back("gasFile_760_Ar_98_CH4_1_imp_O2_0.5_50");
  //listOfFiles.push_back("gasFile_760_Ar_98_CH4_1_imp_O2_0.1_50");
  //listOfFiles.push_back("gasFile_760_Ar_98_CH4_1_imp_O2_0_50");
  //listOfFiles.push_back("gasFile_7600_Ar_98_CH4_1_imp_O2_50_50");
  //listOfFiles.push_back("gasFile_760_Xe_100_CH4_0_imp_O2_50_100");
  //listOfFiles.push_back("gasFile_760_Xe_100_CH4_0_imp_O2_50_50");
  //listOfFiles.push_back("gasFile_760_Xe_100_CH4_0_imp_O2_20_50");
  //listOfFiles.push_back("gasFile_760_Xe_100_CH4_0_imp_O2_10_50");
  //listOfFiles.push_back("gasFile_760_Xe_100_CH4_0_imp_O2_5_50");

  //PURE FILES
  /*
    listOfFiles.push_back("gasFile_760_O2_100_CH4_0_50");
   listOfFiles.push_back("gasFile_760_N2_100_CH4_0_50");
  listOfFiles.push_back("gasFile_760_Ne_100_CH4_0_50");
  listOfFiles.push_back("gasFile_100_Ar_0_CH4_100_50");
  listOfFiles.push_back("gasFile_100_Ar_80_CF4_20_50");
  */

  //listOfFiles.push_back("gasFile_100_CF4_100_CH4_0_50");
  //listOfFiles.push_back("gasFile_760_CF4_100_CH4_0_50");
  /*
  listOfFiles.push_back("gasFile_760_Xe_100_CF4_0_100");
  listOfFiles.push_back("gasFile_760_Ar_0_CF4_100_100");
  listOfFiles.push_back("gasFile_760_Ar_100_CH4_0_50");
  listOfFiles.push_back("gasFile_760_Ar_100_CH4_0_100");
  listOfFiles.push_back("gasFile_760_Ar_0_CH4_100_50");
  // NOT USED listOfFiles.push_back("gasFile_760_Ar_100_CF4_0_50");
  // Ar:CF4 mixes
  listOfFiles.push_back("gasFile_760_Ar_90_CF4_10_50");
  listOfFiles.push_back("gasFile_760_Ar_80_CF4_20_50");
  listOfFiles.push_back("gasFile_760_Ar_70_CF4_30_50");
  listOfFiles.push_back("gasFile_760_Ar_60_CF4_40_50");
  listOfFiles.push_back("gasFile_760_Ar_50_CF4_50_50");
  listOfFiles.push_back("gasFile_760_Ar_40_CF4_60_50");
  listOfFiles.push_back("gasFile_760_Ar_30_CF4_70_50");
  listOfFiles.push_back("gasFile_760_Ar_30_CF4_70_100");
  listOfFiles.push_back("gasFile_760_Ar_20_CF4_80_100");
  listOfFiles.push_back("gasFile_760_Ar_10_CF4_90_100");
  */
  //Ar:CH4 mixes
  listOfFiles.push_back("gasFile_150_Ar_98_CH4_2_50");
  listOfFiles.push_back("gasFile_760_Ar_98_CH4_2_50");
  //listOfFiles.push_back("gasFile_760_Ar_98_CH4_2_50");
  /*  listOfFiles.push_back("gasFile_760_Ar_98_CH4_2_100");
  listOfFiles.push_back("gasFile_760_Ar_95_CH4_5_50");
  listOfFiles.push_back("gasFile_760_Ar_95_CH4_5_100");
  listOfFiles.push_back("gasFile_760_Ar_90_CH4_10_50");
  listOfFiles.push_back("gasFile_760_Ar_90_CH4_10_100");
  listOfFiles.push_back("gasFile_760_Ar_80_CH4_20_50");
  listOfFiles.push_back("gasFile_760_Ar_80_CH4_20_100");
  listOfFiles.push_back("gasFile_760_Ar_70_CH4_30_50");
  listOfFiles.push_back("gasFile_760_Ar_70_CH4_30_100");
  listOfFiles.push_back("gasFile_760_Ar_60_CH4_40_50");
  listOfFiles.push_back("gasFile_760_Ar_60_CH4_40_100");
  listOfFiles.push_back("gasFile_760_Ar_50_CH4_50_50");
  listOfFiles.push_back("gasFile_760_Ar_50_CH4_50_100");
  listOfFiles.push_back("gasFile_760_Ar_40_CH4_60_50");
  listOfFiles.push_back("gasFile_760_Ar_40_CH4_60_100");
  listOfFiles.push_back("gasFile_760_Ar_30_CH4_70_50");
  listOfFiles.push_back("gasFile_760_Ar_30_CH4_70_100");
  listOfFiles.push_back("gasFile_760_Ar_20_CH4_80_50");
  //Not ready: listOfFiles.push_back("gasFile_760_Ar_20_CH4_80_100");
  listOfFiles.push_back("gasFile_760_Ar_10_CH4_90_50");
  //Not ready:listOfFiles.push_back("gasFile_760_Ar_10_CH4_90_100");
  */
  for(unsigned int iFile=0;iFile<listOfFiles.size();iFile++)
    {
  // Setup the gas.
  MediumMagboltz* gas = new MediumMagboltz();
  const std::string pathGas = "gas/";
  //gas->LoadGasFile(pathGas+m_gasFile);
  std::string basename(listOfFiles[iFile]);
  std::vector<std::string> fnames;
  std::fstream fields;
  fields.open("efields.txt");
  while(!fields.eof())
    {
      std::string a,b;
      fields>> a >> b;
      //std::cout << a << "\t"<< b<<std::endl;
      fnames.push_back(std::string("output/"+basename+"_1_"+a+"_"+b+".gas"));
    }
  //const std::string fname2("output/"+basename+"_1_0.1_0.1.gas");
  //const std::string fname1("output/"+basename+"_1_100_100.gas");
  std::cout<< "<---Merging -->"<<std::endl;
  //gas->EnableDebugging();
  int igas=0;
  while(!gas->LoadGasFile(fnames[igas]))
    igas++;
  //gas->SetTemperature(m_gasTemperature);
  //gas->SetPressure(m_gasPressure);
  std::cout<< "<---Merging -->"<<std::endl;
  for(unsigned int j=igas+1;j<fnames.size();j++)
    {
      std::cout<< "Merging -->"<<j<<std::endl;
      gas->MergeGasFile(fnames[j],false);
    }
  gas->Initialise();
  gas->DisableDebugging();
  std::cout << " Temperature = "<< gas->GetTemperature() << ", Pressure = "<<gas->GetPressure()<<std::endl;
  const std::string fname3(basename+".gas");
  gas->WriteGasFile(fname3);
  delete gas;
    }
  return 0;
}
