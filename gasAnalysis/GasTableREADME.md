#gasTable		

__HOW TO USE:__

**Compiling the file**
--> make gasTable

**Executing gasTable in the terminal**

eg.(for 2 gases + impurity)
--> ./gasTable impurityType impurityConc Gas1Name Gas1Composition Gas2Name

eg. (for 2 gases + no impurity)
--> ./gasTable 0 Gas1Name Gas1Composition Gas2Name

##Note:	       1.The composition of the last gas entered need not be specified as the remaining composition will be calculated by default ##NOTE: GAS COMPOSITIONS ARE IN PERCENTAGES##
	       2.impurityType(0: no contamination, 1: 02 contamination, 2: H20 contamination)
	       3.impurityConc(eg.10, 100, 1000 etc.) ##NOTE:IMPURITY GAS COMPOSITION IS IN PPM##
	       4.Up to 5 gas names can be entered with impurity (6th gas is the impurity)
	       5.Up to 6 gas names can be entered without impurity
	       6.Enter the gas names as standard chemical formulas, eg.(He==Helium, CH4==Methane, Ne==Neon, O2==Oxygen)
	       

***Submitting jobs to Condor***
For pure gas files:
--> condor_submit condorSubmitGas.con

For gas files with impurity:
--> condor_submit condorSubmitGasImp.con

##Note:		  1.Gas compositions can be changed in the condor files	

