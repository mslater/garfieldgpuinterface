#include <fstream>
#include <sstream>
#include <iostream>
#include <numeric>
#include <iterator>     // std::advance

#include <cxxopts.hpp>

//#include <stdlib.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TApplication.h>
#include <TStopwatch.h>

#include "Garfield/MediumMagboltz.hh"
#include "Garfield/FundamentalConstants.hh"

using namespace Garfield;

int main(int argc, char * argv[])
{
  TStopwatch timer;
  timer.Start();

  cxxopts::Options options("simulation", "Simulation for as-yet-untitled Migdal experiment");
  // bools have an implicit value of true anyway but we'll include them here for clarity
  options.add_options()
    ("composition", "Composition in the Form El1,Fraction,El2,Fraction,El3", cxxopts::value<std::vector<std::string>>(), "Ar,98,CH4")
    ("impurity", "Impurity in  El1,ppm", cxxopts::value<std::vector<std::string>>(), "")
    ("ncoll", "Number of collisions (in units of 10^7)", cxxopts::value<int>()->default_value("20"))
    ("nfields", "Number of field points", cxxopts::value<int>()->default_value("20"))
    ("emin", "Min Efield [V/cm]", cxxopts::value<float>()->default_value("0.01"))
    ("emax", "Max Efield [V/cm]", cxxopts::value<float>()->default_value("50000"))
    ("log", "Use log spacing", cxxopts::value<bool>()->implicit_value("true")->default_value("false"))
    ("debug", "Debug", cxxopts::value<bool>()->default_value("false"))
    ("gas-temperature", "Gas Temperatures [K]", cxxopts::value<double>()->default_value("293.15"))
    ("gas-pressure", "Gas Pressure [Torr]", cxxopts::value<double>()->default_value("100"))
    ("gas-penning", "Enable Penning Transfers", cxxopts::value<bool>()->implicit_value("true")->default_value("false"))
    ("gas-penningProb", "rPenning", cxxopts::value<float>()->default_value("0.15"))
    ("h,help", "Print usage")
    ;
  auto cmdline_args = options.parse(argc, argv);

  if (cmdline_args.count("help")) {
    std::cout << options.help() << std::endl;
    return 1;
  }

  //The GasFile is a table of transport parameters (e.g drift velocity) as a function of E (and B if required) for use in simulations
  const int ncoll   = cmdline_args["ncoll"].as<int>();                  //Number of electron collisions MagBolts keeps track of (in units of 10^7)
  const int nFields = cmdline_args["nfields"].as<int>();                //Number of different fields to generate the coefficients for
  const bool useLog = cmdline_args["log"].as<bool>();          // Flag to request logarithmic spacing.
  const double emin = cmdline_args["emin"].as<float>();
  const double emax = cmdline_args["emax"].as<float>();           //Make sure this covers the correct range (units of [V/cm])

  const double m_gasPressure   =cmdline_args["gas-pressure"].as<double>();
  const double m_gasTemperature=cmdline_args["gas-temperature"].as<double>();
  const bool m_penning    = cmdline_args["gas-penning"].as<bool>();
  const double m_rPenning = cmdline_args["gas-penningProb"].as<float>();

  std::vector<std::string> name;
  std::vector<double> comp;
  if (cmdline_args.count("composition"))
    {
      auto& fcomp = cmdline_args["composition"].as<std::vector<std::string>>();
      std::cout << "Composition" << std::endl;
      auto begin =std::begin(fcomp);
      auto end   =std::end(fcomp);
      for(unsigned int icomp=0;icomp<fcomp.size();icomp++)
	{
	  name.push_back(fcomp.at(icomp));
	  //std::cout << fcomp.at(icomp)<<std::endl;
	  icomp++;
	  if(icomp<fcomp.size())
	    {
	      //std::cout << fcomp.at(icomp)<<std::endl;
	      comp.push_back(atof(fcomp.at(icomp).c_str()));
	    }
	  else
	    comp.push_back(0);
	}
    }
  else
    {
      std::cerr << "did not see composition"<<std::endl;
      return -1;
    }

  bool m_impurity=false;
  std::vector<std::string> imp_name;
  std::vector<double> imp_comp;
  if (cmdline_args.count("impurity"))
    {
      m_impurity=true;
      auto& fcomp = cmdline_args["impurity"].as<std::vector<std::string>>();
      //std::cout << "Impurity" << std::endl;
      auto begin =std::begin(fcomp);
      auto end   =std::end(fcomp);
      for(unsigned int icomp=0;icomp<fcomp.size();icomp++)
	{
	  imp_name.push_back(fcomp.at(icomp));
	  //std::cout << fcomp.at(icomp)<<std::endl;
	  icomp++;
	  if(icomp<fcomp.size())
	    {
	      //std::cout << fcomp.at(icomp)<<std::endl;
	      imp_comp.push_back(atof(fcomp.at(icomp).c_str())/10000.);//this is in %, so overall input expresses ppm
	    }
	  else
	    imp_comp.push_back(0);
	}
    }
  else
    std::cout << "No impurities"<<std::endl;
  
  for(unsigned int i=0;i<name.size();i++)
    std::cout << name[i] << " "<<comp[i]<<std::endl;

  // //std::cout <<"First gas name is: " << v[0] << std::endl;  
  // // conc.push_back(atof(argv[2]));
  
  //If there's no impurity, gas concentration is just 100 - (remaining composition)
  //If there's impurity, gas concentration is just 100 - (remaining composition) - (impurity)
  double sum_imp = std::accumulate(imp_comp.begin(),imp_comp.end(),0.);//sum impurities
  double sum_compButLast = std::accumulate(comp.begin(), comp.end()-1, 0.); //do not count the last one
  unsigned int vsize = comp.size();
  std::cout<< sum_imp<<" "<<sum_compButLast<<std::endl;
  comp[vsize-1]= 100.-sum_compButLast-sum_imp;

  std::cout << "COMPOSITION"<<std::endl;
  for(unsigned int i=0;i<name.size();i++)
    std::cout << " --> " <<name[i] << " "<<comp[i]<<std::endl;
  for(unsigned int i=0;i<imp_name.size();i++)
    std::cout << " --> " <<imp_name[i] << " "<<imp_comp[i]<<std::endl;
 
  // Setup the gas.
  MediumMagboltz* gas = new MediumMagboltz();  //Program for generating gasfiles
  gas->SetTemperature(m_gasTemperature);
  gas->SetPressure(m_gasPressure );

  std::ostringstream fileName;
  fileName<< "gasFile_";
  fileName<< m_gasPressure << "_";
  for (unsigned int i=0;i<name.size();i++)
    fileName<< name[i]<< "_"<< (int)(comp[i])<< (i+1==name.size()?"":"_");  
  if(imp_name.size()>0)
    fileName<< "_imp_";
  for (unsigned int i=0;i<imp_name.size();i++)
    fileName<< imp_name[i]<< "_"<< (float)(imp_comp[i]*1e4)<< (i+1==imp_name.size()?"":"_");  
  fileName << "_"<<ncoll << "_"<<nFields<< "_"<<emin<< "_"<<emax;
  fileName << ".gas";
  std::cout << fileName.str() << std::endl;  

  name.insert(name.end(),imp_name.begin(),imp_name.end());
  comp.insert(comp.end(),imp_comp.begin(),imp_comp.end());
  gas->SetComposition(name, comp);
  
  // Set the field range to be covered by the gas table. 
  gas->SetFieldGrid(emin, emax, nFields, useLog); 
  
  // Switch on debugging to print the Magboltz output.
  gas->EnableDebugging();
  // Run Magboltz to generate the gas table.
  gas->GenerateGasTable(ncoll);
  gas->DisableDebugging();

  if(m_penning)
    {
      std::cout << "Initialising penning with transfer Prob "<< m_rPenning<<std::endl;
      //const double rPenning = 0.51;
      const double lambdaPenning = 0.;
      gas->EnablePenningTransfer(m_rPenning, lambdaPenning, "ar");
    }

  // Save the table
  std::string filename = fileName.str();
  gas->WriteGasFile(filename);

  timer.Stop();
  std::cout << "Timer = "<< timer.RealTime()<< " "<< timer.CpuTime()<<std::endl;

  return 0;
}
