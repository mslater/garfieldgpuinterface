#!/bin/bash
INPUT_initdir=$1
#INPUT_impurityType=$3
INPUT_emin=$2
INPUT_emax=$3
INPUT_pressure=$4
INPUT_name0=$5
INPUT_comp0=$6
INPUT_name1=$7
#INPUT_comp_1=$7
#INPUT_name_2=$8
#INPUT_comp_2=$9
#INPUT_name_3=$10
#INPUT_comp_3=$11
#INPUT_name_4=$12
#INPUT_comp_4=$13
	   

# export GARFIELD_HOME=/home/$(whoami)/garfield
export HEED_DATABASE=${INPUT_initdir}/../../Heed/heed++/database
pwd
cp ${INPUT_initdir}/../gasAnalysis .
source ${INPUT_initdir}/../../setup.sh

#ls -hlrt
#root-config --cxx
#env

#FOR 2 GAS INPUTS, USE THIS
echo "./gasAnalysis --nfields 1 --ncoll 1 --gas-pressure ${INPUT_pressure} --emin ${INPUT_emin} --emax ${INPUT_emax} --composition ${INPUT_name0},${INPUT_comp0},${INPUT_name1}"
./gasAnalysis --nfields 1 --ncoll 50 --gas-pressure ${INPUT_pressure} --emin ${INPUT_emin} --emax ${INPUT_emax} --composition ${INPUT_name0},${INPUT_comp0},${INPUT_name1}

#--impurities O2,20


#FOR 3 GAS INPUTS, USE THIS
# echo "./gasAnalysis ${INPUT_pressure} ${INPUT_impurityType} ${INPUT_name0} ${INPUT_comp0} ${INPUT_name1} ${INPUT_comp1}
# ${INPUT_name2}"
# ./gasAnalysis ${INPUT_pressure} ${INPUT_impurityType} ${INPUT_name0} ${INPUT_comp0} ${INPUT_name1}

rm gasAnalysis
